#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <numeric>
#include <stdlib.h>
#include "H5Cpp.h"

#include <boost/python.hpp>
#include <boost/python/numpy.hpp>
#include <boost/python/class.hpp>
#include <boost/python/module.hpp>
#include <boost/python/def.hpp>

#include "hist.h"

#define nTher 6
#define nRefl 9

#define iNew

#ifdef iNew
#define nDisk 9
#else
#define nDisk 8
#endif

#define newNorm
#define chandra
#define xillverRatio

#define nThet 9
//#define Ndisk  10000000
#define Nemis  100000000
#define numEnergy 60

#define xl1 -1.1440558
#define xl2 3.0000434
#define xlDel 0.00138182702085184
#define PI 3.14159265358979323846  /* pi */
#define cluster

using std::cout;
using std::endl;
using std::string;
using std::vector;
using std::array;

namespace bp = boost::python;
namespace np = boost::python::numpy;

namespace{

void linspace(vector<double> &v, double x1, double x2, int n)
// most useful function
{
    v.resize(n);
    double delta =(x2-x1)/(double)(n-1);
    for (int i=0; i<n; ++i)
    v[i]=x1+(i*delta);
}

vector<double> linspace(double x1, double x2, int n)
{
    vector<double> v;
    linspace(v,x1,x2,n);
    return v;
}

void numpy2Vector(std::vector<double> &v, np::ndarray& x)
// most useful function, extract info from numpy array
{
    int n = x.shape(0);
    v.resize(n);
    double* input_ptr = reinterpret_cast<double*>(x.get_data());
    for (int i = 0; i < n; ++i)
    v[i]=*(input_ptr + i);
    //    std::cout<<"converted numpy to vector "<<n<<" "<<v[0]<<" "<<v[1]<<" "<<v[2]<<std::endl;
}

double interPol(vector<double> &x,double X,vector<double> &y)
{
    if (X<=x.front()) return y.front();
    if (X>=x.back()) return y.back();
    for (int i=0;i<(int)x.size()-1;i++)
    if (X<x[i+1])
    {
        double xc=(X-x[i])/(x[i+1]-x[i]);
        return (1.-xc)*y[i]+xc*y[i+1];
    }
    std::cout<<"something is wrong"<<std::endl;
    return 0.;
}

template <class T>
void pv(vector<T> v,string text="",bool flag=false)
{
    cout<<"pv "<<text<<endl;
    int end=std::min(10,(int)v.size());
    if (flag) end=(int)v.size();
    
    for (int i=0;i<end;i++)
        cout<<"i "<<i<<" "<<v[i]<<endl;
}

template <class T>
vector<T> multiply (vector<T> &a, vector<T> &b)
{
  vector<T> result;
  for (int i=0;i<(int)a.size();i++)
        result.push_back(a[i]*b[i]);
  return result;
}

double linRegression(vector<double> &x,vector<double> &y,vector<double> &w) // ,double &alpha,double &beta)
{
    vector<double> xW=multiply(x,w);
    vector<double> yW=multiply(y,w);
    double sumW=std::accumulate(w.begin(),w.end(),0.0);
    double meanX=std::accumulate(xW.begin(),xW.end(),0.0)/sumW;
    double meanY=std::accumulate(yW.begin(),yW.end(),0.0)/sumW;

    double num=0.;
    double denom=0.;
    for (int i=0;i<(int)x.size();i++)
    {
        double t1=x[i]-meanX;
        double t2=w[i]*t1;
        denom+=t2*t1;
        num+=t2*(y[i]-meanY);
    }
    double beta=num/denom;
    //    double alpha=meanY-beta*meanX;
    return beta;
}

struct EDGE
{
    int dimension;
    int listIndex;
    double x;
} ;

EDGE edge(int dim,vector<double> x,double X)
{
    EDGE res;
    res.dimension=dim;
    
    if (X<=x.front())
    {
        res.listIndex=0;
        res.x=0.;
    }
    else if (X>=x.back())
    {
      res.listIndex=(int)x.size()-2;
        res.x=1.;
    }
    else
        for (int i=0;i<(int)x.size()-1;i++)
    if (X<x[i+1])
    {
        res.listIndex=i;
        res.x=(X-x[i])/(x[i+1]-x[i]);
        break;
    }
    
//    std::cout<<"dim "<<res.dimension<<" "<<" X "<<X<<" listIndex "<<res.listIndex<<" x "<<res.x<<std::endl;
    
    return res;
}

bool edgeCompare (EDGE i,EDGE j) { return (i.x<j.x); }

#define nStokes 3

void rotate(Hist &q,Hist &u,double chi)
{
  //  cout<<"Rotation angle"<<chi<<endl;
    double rotAngle=2.*(chi*PI/180.);
    double c=cos(rotAngle);
    double s=sin(rotAngle);
    for (int i=0;i<q.bins();i++)
    {
        double qV=q.get(i);
        double uV=u.get(i);
        q.set(i,c*qV-s*uV);
        u.set(i,c*uV+s*qV);
	//	if (i<5)
	//	  cout <<"qV "<<qV<<" uV "<<uV<<" qN "<<q.get(i)<<" "<<u.get(i)<<endl;
    }
}

 typedef vector<Hist> sHist;

#define RADIALBINS 12
#define numIncl 10
class DISK{
    static const int radialBins=RADIALBINS;
    static const int energyBins=40;
    double r1,r2,lr1,lr2,deltaR;

public:
    static constexpr double l1=xl1;
    static constexpr double l2=xl2;
    vector<Hist> histo;
    vector<Hist> histoEflux;
    vector<vector<vector<float>>> rebin;
    vector<float> flux;
    vector<float> flux2;
    vector<float> gamma;
    vector<bool> flag;
    vector<float> xi;
//    double massReference;
    DISK(int spinId,int num2);
  //  string fileName;
  void reset();
    int numBins() {return radialBins;};
    int radiusBin(double r);
    double binRadius(int i);
    double binRadius(double i);
  double getR1() {return r1;};
};

DISK::DISK(int spinId,int num2)
{
    double rISCO[9]={9.,6.,4.233002529530826,3.158039166325728,2.320883041761887,1.9372378781396629,
        1.6140296676354695,1.4544979380596716,1.2369706551751847};
    
    for (int i=0;i<RADIALBINS;i++)
    {
        histo.push_back(Hist(energyBins,l1,l2,"disk "+to_string(i),"E"));
        histoEflux.push_back(Hist(energyBins,l1,l2,"disk "+to_string(i),"E"));
	// make rebin vector 
	vector<vector<float>> temp;
	for (int j=0;j<numIncl;j++)
	  {
	    vector<float>res(num2,0.);
	    temp.push_back(res);
	  }
	rebin.push_back(temp);
    }
    //    cout<<"Defined "<<radialBins<<" Histograms"<<endl;
    flux.assign(radialBins,0.);
    flux2.assign(radialBins,0.);
    xi.assign(radialBins,0.);
    gamma.assign(radialBins,0.);
    flag.assign(radialBins,false);
    
    r1=rISCO[spinId]; // rg
    r2=100.; // rg
    lr1=log10(r1);
    lr2=log10(r2);
    deltaR = (lr2-lr1)/(double)radialBins;
}

void DISK::reset()
{
  // empty histograms
  for (int i=0;i<RADIALBINS;i++)
    {
      histo[i].reset();
      histoEflux[i].reset();
    }
  // empty rebin vector
  for (auto &c1 : rebin) // loop over radial bins
    for (auto &c2 : c1) // loop over inclination bins
      for (auto &c3 : c2) // loop over energy bins
	c3=0.;

  for (auto &c : flux) c=0.;
  for (auto &c : flux2) c=0.;
  for (auto &c : xi) c=0.;
  for (auto &c : gamma) c=0.;
  for (int i=0;i<radialBins;i++)
    flag[i]=false;
}

int DISK::radiusBin(double r)
{
    if (r<=r1) return 0;
    if (r>=r2) return radialBins-1;
    int bin=(int)((log10(r)-lr1)/deltaR);
    if (bin<0) return 0;
    if (bin>=radialBins) return radialBins-1;
    return bin;
}
double DISK::binRadius(int i)
// left side of bin
{
    return pow(10.,lr1+(double)i*deltaR);
}
double DISK::binRadius(double i)
// left side of bin
{
    return pow(10.,lr1+i*deltaR);
}

class XILLVER{
    // add private stuff.

    vector<double>delta;

    vector<int>mem;
    vector<vector<float>>data;

    H5::H5File *dFile;
    int access;

    vector<vector<double>> inter;
    vector<double> Vgamma,VAFe,Vxi,VkTe,Vdens,Vincl,Venergy;
    vector<double>energyDelta;
    vector<double>multiplier;
    double dens1,dens2;
    int densOffset;

    // store the interpolated energy spectra here
  vector<vector<vector<float>>>iSpectrum;
  vector<vector<vector<float>>>sSpectrum;

  // store the loaded XILLVER data sets
  vector<string> lXill;
    
    static const int num=2999;

    static constexpr double lx1=xl1;
    static constexpr double lx2=xl2;
    static constexpr double lDel=xlDel;
    static const int rebinFactor=1; // 16 corresponds to bins 1.0522; 30 is 1.1
    int num2;
  bool on,xiFlag;

public:
    XILLVER(string fname,double dens1, double dens2);
    int getRecord(string id,vector<float> &res);
  vector<float> spectrum2(vector<int> &node,int h);
  void initSpectrum(int index, vector<float> xillverPar);
    int xBin(double energy);
  int inclBin(float theta);
    double xVal(int k);
    double xDelta(int k);
    int numBin() {return num2;};
  double weight2(int iBin,int inclBin,double eEmitted);

  vector<float> getXillver(int bin) {return sSpectrum[bin][2];};

    void listXill(){for (auto &s : lXill) cout<<"\""<<s<<"\","; cout<<endl;};
  double getXiMin() {return Vxi.front();};
  double getXiMax() {return Vxi.back();};

  void switchOn() {on = true;};
  void switchOff() {on = false;};
  bool isOn() {return on;};
  bool isOff() {return !on;};

  void xiSwitchOn() {xiFlag = true;};
  void xiSwitchOff() {xiFlag = false;};
  bool xiOn() {return xiFlag;};
  bool xiOff() {return !xiFlag;};

  void saveSpectrum()
  {
    sSpectrum = iSpectrum;
  }

};

XILLVER::XILLVER(string fname,double d1, double d2)
{
    cout<<"Initializing XILLVER"<<endl;
    // define energy grid
    dens1=d1;
    dens2=d2;
    cout<<"density 1 "<<d1<<endl;
    cout<<"density 2 "<<d2<<endl;
    on=true;
    xiFlag=false;

    Vgamma=linspace(1.,3.4,13);
    VAFe={0.5,1.,5.,10.};
    Vxi={0.,0.30103,0.69897,1.,1.30103,1.69897,2.,2.30103,2.69897,3.,3.30103,3.69897,4.,4.30103,4.69897};
    VkTe={1.,10.,20.,50.,80.,100.,150.,200.,250.,300.,350.,400.};
    vector<double> temp;
    temp=linspace(15.,22.,8);

    bool first=true;
    int i=0;
    for (auto x :temp)
    {
        if ((x>=dens1)&&(x<=dens2))
        {
            if (first)
            {
                densOffset=i;
                first=false;
            }
            Vdens.push_back(x);
        }
        i++;
    }
    cout<<"First density index "<<densOffset<<endl;
    cout<<"Density length "<<Vdens.size()<<endl;

    Vincl={18.194874,31.788328,41.409622,49.458397,56.632984,63.256313,69.51268,75.522484,81.37307,87.13402};

    double x=lx1;
    for (int i=0;i<num;i++)
    {
        Venergy.push_back(pow(10.,x));
        x+=lDel;
    }
    for (int i=0;i<num-1;i++)
        energyDelta.push_back(Venergy[i+1]-Venergy[i]);
    energyDelta.push_back(0.);

    inter.push_back(Vgamma);
    inter.push_back(VAFe);
    inter.push_back(Vxi);
    inter.push_back(VkTe);
    inter.push_back(Vdens);
    inter.push_back(Vincl);
    //    inter.push_back(Venergy);

    cout<<"Dimensions of data structure"<<endl;
    int m=1;
    for (auto v : inter)
    {
        multiplier.push_back(m);
        m*=(int)v.size();
        std::cout<<v.size()<<" "<<m<<" "<<endl;
    }
    cout<<" ==> "<<m<<endl;
    mem.assign(m,-1);

    // open file
    H5std_string dName =fname;
    cout<<"Opening XILLVER "<<dName<<endl;
    
    dFile = new H5::H5File(dName,H5F_ACC_RDONLY );
    cout<<"Opened file."<<endl;

//    string id="0620103702";
//    H5std_string dataSet = id;
//    cout<<"XILLVER getRecord is opening dataset >>"<<dataSet<<"<<"<<endl;
//    H5::DataSet dataset = dFile->openDataSet(dataSet);
//    cout<<"done"<<endl;

    access=0;

    num2=num;

    cout<<"Rebin XILLVER from "<<num<<" to "<<num2<<" bins."<<endl;
    for (int i=0;i<RADIALBINS;i++)
      {
	vector<vector<float>> res2;
	for (int h=0;h<numIncl;h++)
	  {
	    vector<float>res(num2,0.);
	    res2.push_back(res);
	  }
	iSpectrum.push_back(res2);
      }
    cout<<"XILLVER initialization is complete"<<endl<<endl;
}

int XILLVER::getRecord(string id,vector<float> &res2)
{
    H5std_string dataSet = id;
    hsize_t dims[2]; // dataset dimensions
    vector<float> res(num,0.);

    try
    {
      //        cout<<"XILLVER getRecord is opening dataset >>"<<dataSet<<"<<"<<endl;
        H5::DataSet dataset = dFile->openDataSet(dataSet);
	//        cout<<"XILLVER getRecord is defining dataspace"<<endl;
        H5::DataSpace filespace = dataset.getSpace();
        int rank = filespace.getSimpleExtentDims( dims );
        H5::DataSpace mspace1(rank,dims);
	//        cout<<"XILLVER getRecord is reading data rank dims "<<rank<<" "<<dims<<" available storage "<<num<<endl;
        dataset.read(res.data(),H5::PredType::NATIVE_FLOAT, mspace1, filespace );
	lXill.push_back(dataSet);
    }
    // catch failure caused by the H5File operations
    catch( H5::FileIException error )
    {
        cout<<"failure caused by the H5File operations"<<endl;
        return -1;
    }
    // catch failure caused by the DataSet operations
    catch(  H5::DataSetIException error )
    {
        cout<<"failure caused by the DataSet operations"<<endl;
        return -1;
    }
    // catch failure caused by the DataSpace operations
    catch(  H5::DataSpaceIException error )
    {
        cout<<"failure caused by the DataSpace operations"<<endl;
        return -1;
    }
    // catch failure caused by the DataType operations
    catch(  H5::DataTypeIException error )
    {
        cout<<"failure caused by the DataType operations"<<endl;
        return -1;
    }
    access++;
    
    //    if ((int)(num/rebinFactor)>=num2) cout<<"??? "<<num<<" "<<rebinFactor<<" "<<num2<<endl;
    for (int i=0;i<num;i++)
        res2[i]=res[i];
    return (int)dims[0];
}

 vector<float> XILLVER::spectrum2(vector<int> &node,int h)
{
    char cname[31];
    int id=0;

    for (int i=0;i<=(int)node.size();i++)
      if (i<(int)node.size())
        id+=node[i]*multiplier[i];
      else
	id+=h*multiplier[i];

    node[4]+=densOffset;

    //    sprintf(cname, "%02d%01d%02d%02d%01d%02d",node[0],node[1],node[2],node[3],node[4],node[5]);
    //    cout<<"Xillver ID "<<id<<" mem "<<mem[id]<<" cname "<<cname<<endl;
    if (mem[id]<0)
      {
	//	cout <<"XILLVER "<<node[0]<<" "<<node[1]<<" "<<node[2]<<" "<<node[3]<<" "<<node[4]<<" "<<node[5]<<" "<<endl;
        sprintf(cname, "%02d%01d%02d%02d%01d%02d",node[0],node[1],node[2],node[3],node[4],h);
        string name(cname);
        vector<float> res2(num2,0.);

        getRecord(name,res2);

        mem[id]=(int)data.size();
        data.push_back(res2);
	if((int)data.size()%1000==0)
	  cout<<"# of XILLVER data sets loaded "<<data.size()<<endl;
    }
    return data[mem[id]];
}
 void XILLVER::initSpectrum(int index, vector<float> xillverPar)
{

    if (index>RADIALBINS)
    {
      cout<<"Error in XILLVER initSpectrum "<<index<<" "<<iSpectrum.size()<<" "<<RADIALBINS<<endl;
        exit (EXIT_FAILURE);
    }
    // for each dimension, save the dimension, the index offset,
    // and the normalized interpolation value
    // for densities with log10(density)>19:
    // we use the larger density to get a lower xi
    // we use the XLLVER table for lo10(density)=19.
    // we scale up the flux to approximate xi=4piF/n 
    float densFF=0.;
    if (xillverPar[4]>19.)
      {
	densFF=xillverPar[4]-19.;
	xillverPar[4]=19.;
      }

    vector<EDGE>edgeList;
    for (int i=0;i<(int)xillverPar.size();i++)
      edgeList.push_back(edge(i,inter[i],xillverPar[i]));

    // get indices of distant corner of the N-dim cube
    vector<int> coord;
    coord.assign(edgeList.size(),1);
    for (int i=0;i<(int)edgeList.size();i++)
      coord[i]+=edgeList[i].listIndex;
    
    // sort according to x, smallest x first
    std::sort(edgeList.begin(), edgeList.end(), edgeCompare);
    
    vector<vector<int>> points;
    points.push_back(coord);
    
    // go down the monkey bars to the near edge of the N-dim cube
    for (auto v :edgeList)
    {
      //        std::cout<<"dim "<<v.dimension<<" listIndex "<<v.listIndex<<" x "<<v.x<<std::endl;
        coord[v.dimension]-=1;
        points.push_back(coord);
    }

    vector<float> res,resPrev,resNew;

    for (int h=0;h<numIncl;h++)
      { 
	for (int i=0;i<(int)points.size();i++){
	  if (i==0)
	    {
	      res=spectrum2(points[i],h); // deep copy
	      resPrev=res; // deep copy
	    }
	  else
	    {
	      double fac=1.-edgeList[i-1].x;
	  
	      resNew=spectrum2(points[i],h);
	      for (int j=0;j<(int)res.size();j++)
		res[j]+=fac*(resNew[j]-resPrev[j]);
	      // fast resPrev=resNew;
	      if (i<(int)points.size()-1)
		resPrev=resNew;
	      //            pv(res,"next iteration ");
	    }
	}
	for (int k=0;k<(int)res.size();k++)
	  iSpectrum[index][h][k]=res[k];
      }
//    pv(res,"XILLVER result, index "+to_string(index),true);
}

int XILLVER::xBin(double energy)
{
    if (energy<0.) return 0;
    double l=log10(energy);
    if (l<=lx1) return 0;
    if (l>=lx2) return num-1;
    int result=(int)((int)((l-lx1)/lDel)/rebinFactor);
    if (result>=num2) cout <<"??? energy "<<energy<<" result "<<result;
    return result;
}

int XILLVER::inclBin(float theta)
{
  float x[numIncl]={0.45102681, 0.64350111, 0.79539883, 0.92729522, 1.04719755,
    1.15927948, 1.26610367, 1.36943841, 1.47062891, 1.57079633};
  // {25.84193276,36.86989765,45.572996,53.13010235,60.,66.42182152, 72.54239688,78.46304097,84.26082952,90.};
  int i;
  for (i=0;i<numIncl-1;i++)
    if (theta<x[i]) break;
  return i;
}

double XILLVER::xVal(int k)
{
    double delta=lDel*(double)rebinFactor;
    double res=lx1+((double)k+0.5)*delta;
    return res;
}

double XILLVER::xDelta(int k)
{
    double delta=lDel*(double)rebinFactor;
    double x1=pow(10.,lx1+((double)k)*delta);
    double x2=pow(10.,lx1+((double)k+1.)*delta);
    return (x2-x1);
}

 double XILLVER::weight2(int iBin,int inclBin,double eEmitted)
{
  return iSpectrum[iBin][inclBin][xBin(eEmitted)];
}

class dataReader {
    // variables needed to load data
    vector<double> energyCenter,energyDelta,logE,incl1,incl2,inclFac,theta;
    vector<double> xCenterLog,xDelta,xRes;
    vector<float> resT;
    vector<vector<double>> inter1,inter2;
    vector<vector<vector<double>>> inter;

    vector<sHist>lTherm,lRefl;

    vector<string>lRefl1; // holds idName
    vector<int>lRefl2; // holds tNum;
    vector<vector<float>> lRefl3; // holds xillverPar;
  vector<double> lTherm1;
  vector<double> lTherm2;
  vector<double> lRefl4;
  vector<double> lRefl5;
  vector<double> lRefl6;

    vector<DISK> lDisk;
    XILLVER *xillver;

  vector<int> lastDataSet;
  vector<string> lastId;
    
    int nData;
    bool first;
    
    double massReference,distanceReference_kpc;
    double rg;
  int numCall;
    // variables needed to do interpolation
    std::vector<double> Va,Vgeometry,VLoM,VtauC,VtempC,Vincl,VthetaC,VrC, WthetaC,WrC,WrC2,mDot,multiplier;
    
    // index lTherm
    std::vector<int>mem;

    // keep track for which xillverPars we folded reflected spectrum?
    std::vector<vector<float>>memRefl;
    
    int inputSize;
  void logIt(Hist &h);
  
  double eFit,alpha,fl1,fl2;
  vector<string> dimName;

  sHist hTh;
  void sInit(sHist &h);  
  void sReset(sHist &h);  
  string path(string inp);
  double ampl,reflSc;

public:
    dataReader();


    void setBins(np::ndarray& x1, np::ndarray& x2);
    int readData(string pathName,string dType,string coronaId,int dataSetNum,vector<float> &res);
//    bool thermalAnalysis(string pathName,string coronaId,int thetaId,vector<float> &res,int len,sHist &h);
    bool thermalAnalysis(int thetaId,vector<float> &res,int len,sHist &h);

    void triangleSmooth(vector<float> &spec,int num);
    bool diskAnalysis(vector<float> &res,int len,DISK &myDisk);
  bool reflAnalysis(int thetaId,double rSc,vector<float> xillverPar,DISK &disk,vector<float> res,int len,sHist &h,bool flag);
    np::ndarray getNode(int id,int result);

    
    np::ndarray getNumpy(Hist &h,double scale);
    np::ndarray getNumpyInterpol(Hist &h,double scale);
    np::ndarray getNumpy(vector<float> &v,double scale);
  np::ndarray getNumpy(int i){vector <float>n; n.push_back(0.);return getNumpy(n,(double)1.);};

    np::ndarray getI(np::ndarray& pars,np::ndarray& x1, np::ndarray& x2);
    np::ndarray getQ();
    np::ndarray getU();
    np::ndarray getEnergies();

  //    np::ndarray getDisk(string config,int result);
};

dataReader::dataReader()
{


    cout <<"Initializing dataReader"<<endl;

    numCall=0;
    float dens1=15.;
    float dens2=19.;


    cout<<endl<<"xillverRatio enabled"<<endl<<endl;

#ifdef cluster
    string xName="/krawcz/x4sm.hdf5";
    ifstream f(xName.c_str());
    if (!f.good())
      xName="/data/krawcz/x4sm.hdf5";
#else
    string xName="/User/krawcz/data/javier/x4sm.hdf5";
#endif

    cout<<"Opening "<<xName<<endl;
    
    xillver = new XILLVER(xName,dens1,dens2);

    // variables for acquiring histograms
    inputSize=Nemis*nDisk;
    cout<<"Size of buffer "<<inputSize<<" Bytes "<<endl;
    resT.resize(inputSize);
    
    energyCenter.resize(numEnergy);
    energyDelta.resize(numEnergy);
    
    double l1=xl1;
    double l2=xl2;
    linspace(logE,l1,l2,numEnergy+1);
    
    for (int i=0;i<numEnergy;i++){
        energyCenter[i]=pow(10.,(logE[i]+logE[i+1])/2.);
        energyDelta[i] =pow(10.,logE[i+1])-pow(10.,logE[i]);
    }
    
    float x1=1./(float)nThet;
    float x2=1.-x1;
    
    linspace(incl1,0.,x2,nThet);
    linspace(incl2,x1,1.,nThet);
    
    vector<double> t1;
    linspace(t1,0.,1.,nThet+1);
    
    theta.resize(nThet);
    inclFac.resize(nThet);
    
    for (int i=0;i<nThet;i++){
        theta[i]=acos((t1[i]+t1[i+1])/2.)*180./PI;
        inclFac[i]=1./(2.*PI*(incl2[i]-incl1[i]))/20.;
        cout<<"bin "<<i<<" theta "<<theta[i]<<" 90-theta "<<90.-theta[i]<<" inclFac "<<inclFac[i]<<endl;
        theta[i]=90.-theta[i];
    }
    
    first=true;
    
    // variables needed for interpolation
    Vgeometry={0,1}; // 1 digit
    
    Va={-1.,0., 0.5, 0.75, 0.9, 0.95, 0.98, 0.99, 0.998}; // 1 digit
    // VLoM={0.25,0.5,1.,2.}; // 1 digit
#ifdef iNew
    VLoM={0.05,0.1,0.25,0.5,0.75,1.,1.25,1.5,1.75,2.}; // changed on 8/25/
#else
    VLoM={0.25,0.5,1.,2.}; // 1 digit
#endif

    VtempC={5.,10.,25.,100.,250.,500.};
    VtauC={0.,0.125,0.25,0.5,0.75,1.,2.};
    
    VthetaC={ 5., 45., 85.};
    VrC ={25,50.,100.};
    
    WthetaC={ 5., 25., 45.};
    WrC ={2.5,10.,50.};
    
    
    //    WrC2={20,50.,100.};
    inter1.push_back(Va);
    inter1.push_back(VLoM);
    inter1.push_back(VtempC);
    inter1.push_back(VtauC);
    inter1.push_back(VthetaC);
    inter1.push_back(VrC);
    inter1.push_back(theta);


    dimName.push_back("spin   ");
    dimName.push_back("LoM    ");
    dimName.push_back("tempC  ");
    dimName.push_back("tauC   ");
    dimName.push_back("thetaC ");
    dimName.push_back("rC     ");
    dimName.push_back("incl   ");

    std::cout<<"Multiplier "<<std::endl;
    int m=1;
    for (auto v : inter1)
    {
        multiplier.push_back(m);
        m*=(int)v.size();
        std::cout<<m<<" ";
    }
    std::cout<<m<<std::endl;
    mem.assign(m,-1);

    inter2.push_back(Va);
    inter2.push_back(VLoM);
    inter2.push_back(VtempC);
    inter2.push_back(VtauC);
    inter2.push_back(WthetaC);
    inter2.push_back(WrC);
    inter2.push_back(theta);


// and store everything
    inter.push_back(inter1);
    inter.push_back(inter2);
    
    mDot = {3.7051669608864184e18,2.445638162425853e18,1.7032611826597898e18,1.2510545631668196e18,
        8.980141483098305e17,7.35593547554075e17,5.980603496793061e17, 5.297441102019783e17,4.3573499282791e17};
    for (auto &v : mDot)
        v/=1e+18;
    
    
    massReference=10.;
    distanceReference_kpc=2.;

    double G = 6.67259e-8; // cm3 g-1 s-2
    double sol = 2.99792458e+10; // cm/s
    double msun = 1.99e+33; // g

    rg = massReference* G *msun / (sol*sol);
    cout <<"rg for "<<massReference<<" solar mass BH "<<rg<<" cm "<<endl;
    
    
    vector<double> x;
    vector<double> y;
    vector<double> w;
    for (int i=0;i<11;i++)
    {
        x.push_back((double)i);
        y.push_back((double)(i*i));
        w.push_back(1.);
    }
    cout <<"Test of linRegression "<<linRegression(x,y,w)<<endl;

    sInit(hTh);
    cout <<"Initialized"<<endl;
}

void dataReader::setBins(np::ndarray& x1, np::ndarray& x2)
{
    // make arrays for the output of getI, getQ and getU
    vector<double>X1,X2;
    numpy2Vector(X1,x1);
    numpy2Vector(X2,x2);
    
    nData=(int)X1.size();
    xCenterLog.resize(nData);
    xDelta.resize(nData);
    
    // get log center of interval
    for (int i=0;i<nData;i++)
    {
        xCenterLog[i]=(log10(X2[i])+log10(X1[i]))/2.;
        xDelta[i]=X2[i]-X1[i];
    }
    
    xRes.resize(nData);
}

void dataReader::sInit(sHist &h)
 {
   for (int s=0;s<nStokes;s++)
     h.push_back(Hist(numEnergy,xl1,xl2,"name"+to_string(s)));
 }

void dataReader::sReset(sHist &h)
 {
   for (auto &c : h)
     c.reset();
 }

int dataReader::readData(string pathName,string dType,string coronaId,int dataSetNum,vector<float> &res)
{
    string prefix;

    if (dType[0]=='d')
        prefix="d";
    else
        prefix="e";
        
    H5std_string dName = pathName+prefix+coronaId+".h5";
    H5std_string dataSet;
    
    if (dType[0]=='d')
        dataSet= coronaId+dType;
    else
        dataSet= coronaId+dType+std::to_string(dataSetNum);
    
    hsize_t dims[2];    // dataset dimensions
    /*
     * Try block to detect exceptions raised by any of the calls inside it
     */
    try
    {
      //      cout<<"opening "<<dName<<" dataset "<<dataSet<<endl;
        H5::H5File dFile(dName,H5F_ACC_RDONLY );
        //  H5::Group  group(dFile.openGroup(coronaId));
        H5::DataSet dataset = dFile.openDataSet(dataSet);
        H5::DataSpace filespace = dataset.getSpace();        
        int rank = filespace.getSimpleExtentDims( dims );
        if ((int)(dims[0]*dims[1])>inputSize)
        {
            cout<<"input buffer is too small "<<dims[0]*dims[1]<<" > "<<inputSize<<endl;
            exit(EXIT_FAILURE);
        }

        H5::DataSpace mspace1(rank,dims);
        dataset.read(res.data(),H5::PredType::NATIVE_FLOAT, mspace1, filespace );
        
	/*        for (int i=0;i<3;i++){
            for (int j=0;j<(int)dims[1];j++)
            cout <<res[i*dims[1]+j]<<" ";
            cout <<endl;
        }
	*/
        dFile.close();
    }
    // catch failure caused by the H5File operations
    catch( H5::FileIException error )
    {
        cout<<"failure caused by the H5File operations"<<endl;
        exit (EXIT_FAILURE);
    }
    // catch failure caused by the DataSet operations
    catch(  H5::DataSetIException error )
    {
        cout<<"failure caused by the DataSet operations"<<endl;
        exit (EXIT_FAILURE);
    }
    // catch failure caused by the DataSpace operations
    catch(  H5::DataSpaceIException error )
    {
        cout<<"failure caused by the DataSpace operations"<<endl;
        exit (EXIT_FAILURE);
    }
    // catch failure caused by the DataType operations
    catch(  H5::DataTypeIException error )
    {
        cout<<"failure caused by the DataType operations"<<endl;
        exit (EXIT_FAILURE);
    }
    return (int)dims[0];
}

void dataReader::logIt(Hist &h)
{
/*    for (int k=0; k<h.bins(); k++)
        if (h.get(k)>0.)
            h.set(k,log10(h.get(k)/energyDelta[k]));
        else
            h.set(k,-100.);
*/
    for (int k=0; k<h.bins(); k++)
        h.set(k,h.get(k)/energyDelta[k]);
 }
 

bool dataReader::thermalAnalysis(int thetaId,vector<float> &res,int len,sHist &h)
{
    float fac=inclFac[thetaId];
    int ioff=0;

    for (int s=0;s<nStokes;s++)
        h[s].reset();

    for (int i=0;i<len;i++){
      double rLaunch=res[ioff];
      if ((rLaunch>=fl1)&&(rLaunch<=fl2))
	{
	  float eObs=res[ioff+1];
	  if (eObs>0.)
	    for (int s=0;s<nStokes;s++){
	      float stokes=res[ioff+(2+s)];
	      // this would be faster if we stored log10(energy)
	      h[s].fill(log10(eObs),fac*stokes);
	    }
	}
      ioff+=nTher;
    }
    // calculate differential fluxes
    
    for (int s=0;s<nStokes;s++)
        logIt(h[s]);
    return true;
}


void dataReader::triangleSmooth(vector<float> &spec,int num)
{
  auto spec2=spec;
  float maxV=0.;
  for (int i=num;i<(int)spec.size()-1-num;i++)
    {
      if (spec2[i]>maxV) maxV=spec2[i];
      float vSum=0.;
      float wSum=0.;
      for (int j=-num;j<=num;j++)
	{
	  int index=i+j;
	  float val=spec2[index];
	  float w=num+1-fabs(j);
	  wSum+=w;
	  //	  if (val>maxV/5e+2)
	  vSum+=w*log(val);
	  //	  else
	  //	    vSum+=w*(log(maxV/5e+2));
	}
      float res=vSum/wSum;
      spec[i]= exp(res);
    }
  
  //  for (int i=0;i<(int)spec.size();i++)  //  spec[i]=pow((float)(i+1),-0.5);
}

bool dataReader::diskAnalysis(vector<float> &res,int len,DISK &disk)
{
    // res, len: input data

    // output:
    // hList: list of numRbin histograms filled with weighed events
    // flux: list of numRbin fluxes
    // gamma: list of numRbin gamma-values

    // hCenter: center energy of bins of hList histograms
    // hCenterLog: same in log10

    int ioff=0;
    double v1=pow(10.,disk.l1);
    double v2=pow(10.,disk.l2);
    
    //    cout<<"Disk analysis, looping over all events"<<endl;
    // loop over all events

    float sumN=0.;
    float sumI=0.;
    float sumE=0.;

    float eFit1=eFit;
    float eFit2=10.*eFit;

    for (int i=0;i<len;i++){

      double rLaunch=res[ioff];

      if ((rLaunch>=fl1)&&(rLaunch<=fl2))
	{
	  float rDisk=res[ioff+1];
	  if (rDisk>disk.getR1())
	    {
	      float ePf=res[ioff+2];
	      float iDisk=res[ioff+3];
	      double product=ePf*iDisk;
	      if ((iDisk>0.)&&(iDisk<1.))
		{ 
		  int iBin=disk.radiusBin(rDisk);
		  
		  // flux F_x (integrated from 0.1-1000 keV)
		  if ((ePf>0.1)&&(ePf<1000.))
		    {
		      disk.flux2[iBin]+=product;
#ifdef newNorm
		      if ((ePf>eFit1)&&(ePf<eFit2))
#endif
			disk.flux[iBin]+=product;
		      if (iBin==0)
			{
			  sumN+=1;
			  sumI+=iDisk;
			  sumE+=ePf;
			}
		    }

		  if ((ePf>v1)&&(ePf<v2))
		    {
		      disk.histo[iBin].fill(log10(ePf),iDisk);
		      disk.histoEflux[iBin].fill(log10(ePf),product);
		      
		      int eBin=xillver->xBin(ePf);
		      float thetaEmitted=res[ioff+4];
		      int inclBin=xillver->inclBin(thetaEmitted);
#ifdef iNew
		      float reflWeight=res[ioff+8];
		      
		      /*		  cout<<"r>> rL "<<res[ioff+0]
					  <<" rDisk "<<res[ioff+1]
					  << " ePf "<<res[ioff+2] 
					  << " iDisk "<<res[ioff+3] 
					  << " t "<<res[ioff+4] 
					  << " eEm "<<res[ioff+5] 
					  << " eObs "<<res[ioff+6] 
					  << " iObs "<<res[ioff+7] 
					  << " iDD "<<res[ioff+8]<<endl; 
		      */
		      
		      disk.rebin[iBin][inclBin][eBin]+=iDisk*reflWeight;
#else
		      disk.rebin[iBin][inclBin][eBin]+=iDisk;
#endif
		    }
		}
	      else
		cout<<"iDisk "<<iDisk<<endl;
	    }
	}
      ioff+=nDisk;
    }

    for (int i=0;i<(int)disk.flux.size();i++)
    {
        float rd1    = disk.binRadius(i);
        float rd2    = disk.binRadius(i+1);
        float aeff  = PI*(rd2*rd2-rd1*rd1)*(rg*rg);
        // calculate dSq for 2 kpc;
        // factor 20: we weighted for 1e+6 simulated events but we simulated 20e+6 events
        float dSq1   = pow(distanceReference_kpc*3.086e+21,2)/(20.*aeff);
        float dSq1b  = dSq1/1e+20;
        float dSq2   = dSq1*1.60218e-9; // energy flux in <ergs>, not keV
	for (int j=0;j<numIncl;j++)
	  {	  
	    for (int k=0;k<(int)disk.rebin[i][j].size();k++)
	      disk.rebin[i][j][k]*= dSq1b; //this should be the flux in photons cm^-2 s^-1
	    triangleSmooth(disk.rebin[i][j],4);
	  }
        disk.flux[i] *= dSq2; // this should be the flux in ergs cm^-2 s^-1
        disk.flux2[i] *= dSq2; // this should be the flux in ergs cm^-2 s^-1
	disk.histoEflux[i].scale(dSq2);
    }

    // get spectral indices
    for (int i=0;i<(int)disk.histo.size();i++)
    {
        auto & h=disk.histo[i];

        vector<double>energy;
        vector<double>logFlux;
        vector<double>weight;

        double sum=0.;
        double e1=log10(eFit1);
        double e2=log10(eFit2);
        for (int k=0; k<h.bins(); k++)
        {
            double bc=h.getX(k);
            if ((bc>e1)&&(bc<e2)&&(h.get(k)>0.)&&(h.getSq(k)>0.))
                sum+=h.get(k);
        }
            
        sum*=0.95;
        double sum2=0.;
        for (int k=0;k<h.bins(); k++)
       {
            double bc=h.getX(k);
            if ((bc>e1)&&(bc<e2)&&(h.get(k)>0.)&&(h.getSq(k)>0.))
                {
                    energy.push_back(h.getX(k));
                    logFlux.push_back(log10(h.get(k)));
                    weight.push_back(1./h.getSq(k));
                    sum2+=h.get(k);
                }
            if (sum2>sum) break;
        }
       
	if ((int)energy.size()>3)
	  {
	    double gamma=linRegression(energy,logFlux,weight);
	    disk.gamma[i]=-(gamma-1); // -1 because we histogrammed with log E axis
	    disk.flag[i]=true;

#ifdef newNorm
	    if (disk.gamma[i]<1.25)
	      disk.gamma[i]=1.25;

	    double kappa=disk.gamma[i]-2.;
	    double f=(pow(1000.,-kappa)-pow(0.1,-kappa))/
			(pow(eFit2,-kappa)-pow(eFit1,-kappa));
	    disk.flux[i]*=f;
#endif
	  }
	else
	  {
	    disk.gamma[i]=0.;
	    disk.flag[i]=false;
#ifdef newNorm
	    disk.flux[i]=0.;
#endif
	  }
    }
    return true;
}

 bool dataReader::reflAnalysis(int thetaId,double rSc,vector<float> xillverPar,DISK &disk,vector<float> res,int len,sHist &h,bool flag)
{
    float fac=inclFac[thetaId];

    // Get XILLVER ready for analysis
//    pv(xillverPar,"xillverPar coming in");

    xillverPar.insert(xillverPar.begin(), 0.);
    xillverPar.insert(xillverPar.begin()+2, 0.);
    
    double dens=pow(10.,xillverPar[4]);
    double fac3=4.*PI*rSc/dens;
    vector<double> xiScale(disk.numBins(),1.);

    double rad0=disk.binRadius((double)0.5);
    for (int i=0;i<disk.numBins();i++)
      {
	xillverPar[0]=disk.gamma[i];
	
	double rFrac=disk.binRadius((double)((double)i+0.5))/rad0;
	double densScale=pow(rFrac,alpha);
	
	double xi=log10(fac3*disk.flux[i]/densScale);
	
	xillverPar[2]=xi;
	disk.xi[i]=xi;
	
	if(disk.flag[i])
	  xillver->initSpectrum(i,xillverPar);
      }
    if (flag)
      xillver->saveSpectrum();

    int ioff=0;
	
    for (int s=0;s<nStokes;s++)
        h[s].reset();

    for (int i=0;i<len;i++){

      float eObs=res[ioff+1];
      if (eObs>0.)
	{
	  double eEmitted=res[ioff+8];

	  double rLaunch=res[ioff];


	  if ((rLaunch>=fl1)&&(rLaunch<=fl2))
	    {

	      double rDisk=res[ioff+6];   
	      int iBin=disk.radiusBin(rDisk);
	      
	      float thetaEmitted=res[ioff+7];
	      int inclBin=xillver->inclBin(thetaEmitted);
	      
	      if ((eEmitted>0.)&&(disk.flag[iBin]))
		{
		  // dividing rebin by (M_BH/M_ref)**2 makes sense:
		  // the LoM gives us the proper weights to compute fluxes
		  // the impinging flux needs to be divided by the area of the receiving ring element
		  // that area scales with the BH mass
		  double fac2=xillver->weight2(iBin,inclBin,eEmitted);
		  if (fac2>0.)
		    {
		      //		  if (fabs(log10(fac2))>8.)
		      //		    cout<<"xillverRatio iBin "<<iBin<<" inclBin "<<inclBin<<" eEmitted "<<eEmitted<<" fac2 "<<fac2<<endl;
		      
		      float stokes=res[ioff+2];
		      double xx = fac*fac2*stokes;
		      if ((xx>0.)&&(eEmitted<1000.))
			for (int s=0;s<nStokes;s++)
			  {
			    if (s==0)
			      h[s].fill(log10(eObs),xx);
			    else
			      {
				float stokes2=res[ioff+(2+s)];
				double xx2 = fac*fac2*stokes2;
				h[s].fill(log10(eObs),xx2);
			      }
			  }
		    }
		}
	    }
	}
      ioff+=nRefl;
    }
    
    for (int s=0;s<nStokes;s++)
      logIt(h[s]);

    return true;
}

 np::ndarray dataReader::getNumpy(Hist &h,double scale=1.)
{
    // convert histogram with a few bins into numpy output with more bins.
    cout<<"getNumpy histogram "<<h.getName()<<" x1 "<<h.getX1()<<" x2 "<<h.getX2()<<endl;
    
    vector <double>res;
    int n=h.bins();
    for (int i=0;i<n;i++)
        res.push_back(h.get(i)*scale);
    
    //    bp::tuple shape = bp::make_tuple(numEnergy);
    bp::tuple shape = bp::make_tuple(n);
    bp::tuple stride = bp::make_tuple(sizeof(double));
    np::dtype dt = np::dtype::get_builtin<double>();
    
    // cout<<"Making array of "<<n<<" entries from res "<<res.size()<<endl;
    
    np::ndarray output = np::from_data(res.data(), dt, shape, stride, bp::object());
    //    std::cout<<"Now copying "<<n<<" entries"<<std::endl;
    np::ndarray output_array = output.copy();
    //    std::cout<<"Done "<<std::endl;
    return output_array;
}

 np::ndarray dataReader::getNumpyInterpol(Hist &h,double scale=1.)
{
    // convert histogram with a few bins into numpy output with more bins.
    
  //    std::cout<<"Starting to interpolate "<<std::endl;
    // interpolate data and multiply by bin width
    for (int i=0;i<nData;i++)
      {
	//	cout<<i<<" ";
	//	if ((i>1000)&&(i<1100))
	//  xRes[i]=h.interPol(xCenterLog[i],xDelta[i],true);
	//else
	xRes[i]=h.interPol(xCenterLog[i],xDelta[i],false);
      }

    //    cout<<" Done "<<endl;
    //    bp::tuple shape = bp::make_tuple(numEnergy);
    bp::tuple shape = bp::make_tuple(nData);
    bp::tuple stride = bp::make_tuple(sizeof(double));
    np::dtype dt = np::dtype::get_builtin<double>();
    
    //    cout<<"Making array of "<<nData<<" entries from xRes "<<xRes.size()<<endl;
    
    //        np::ndarray output = np::from_data(hTh[Stokes]->getData(), dt, shape, stride, bp::object());
    np::ndarray output = np::from_data(xRes.data(), dt, shape, stride, bp::object());
    //    std::cout<<"Now copying "<<nData<<" entries"<<std::endl;
    np::ndarray output_array = output.copy();
    //    std::cout<<"Done "<<std::endl;
    return output_array;
}

 np::ndarray dataReader::getNumpy(vector<float> &vec,double scale=1.)
{
    std::vector<double> v(vec.begin(), vec.end());
    for (auto &x : v) x*=scale;

    //    bp::tuple shape = bp::make_tuple(numEnergy);
    bp::tuple shape = bp::make_tuple(v.size());
    bp::tuple stride = bp::make_tuple(sizeof(double));
    np::dtype dt = np::dtype::get_builtin<double>();
    
    //    std::cout<<"Making array of "<<v.size()<<" entries"<<std::endl;
    
    np::ndarray output = np::from_data(v.data(), dt, shape, stride, bp::object());
    //    std::cout<<"Now copying "<<v.size()<<" entries"<<std::endl;
    np::ndarray output_array = output.copy();
    //    std::cout<<"Done "<<std::endl;
    return output_array;
}

np::ndarray dataReader::getEnergies()
{
    bp::tuple shape = bp::make_tuple(numEnergy);
    bp::tuple stride = bp::make_tuple(sizeof(double));
    np::dtype dt = np::dtype::get_builtin<double>();
    
    np::ndarray output = np::from_data(energyCenter.data(), dt, shape, stride, bp::object());
    np::ndarray output_array = output.copy();
    return output_array;
}

/*
 bool reflectedAnalysis(string pathName,string coronaId,vector<float> &res)
 {
 }
 */

 string dataReader::path(string inp)
 {
   string pathName;
#ifdef cluster

   int dnum=std::stoi(inp.substr(0,1));
   int ddnum=std::stoi(inp.substr(0,4));
   int dddnum=std::stoi(inp.substr(3,1));
     pathName="/xray02/kerrc";

     if (((dnum>=7)&&(ddnum<=8125))||((dnum>=7)&&(dddnum<4)))
       pathName="/krawcz/";
     else
       pathName="/xray/corona/done";

#else
   string pathName="/Users/henric/data/outX";
#endif

   pathName+="/"+inp.substr(0,4)+"/";
   return pathName;
 }

np::ndarray dataReader::getI(np::ndarray& pars,np::ndarray& x1, np::ndarray& x2)
{
    vector<double> x;
    numpy2Vector(x,pars);

    vector<double> X1,X2;
    numpy2Vector(X1,x1);
    numpy2Vector(X2,x2);
    
    nData=(int)X1.size();
    xCenterLog.resize(nData);
    xDelta.resize(nData);

    // get log center of interval
    for (int i=0;i<nData;i++)
    {
        xCenterLog[i]=(log10(X2[i])+log10(X1[i]))/2.;
        xDelta[i]=X2[i]-X1[i];
    }
    xRes.resize(nData);

    numCall++;
    bool printFlag=(numCall<200)||(numCall%200==0);

    if (printFlag)
      {
	std::cout<<"getI>> "<<numCall<<" ";
	for (auto v :x)
	  std::cout<<v<<" ";
	std::cout<<std::endl;
      }

    int geom=x[1];
/*
 pNames = [
 0 “a”,
 1 ”geom",
 2 ”M”,
 3 ”tempC",
 4 ”tauC",
 5 “thetaC",
 6 “rC",
 7 “incl",
 8 “Mdot",
 9 “dist",
 10 “chi",
 11” refl",
 12 ”AFe",
 13 “kTe",
 14 ”dens"]
 */

    double mass=x[2];
        
    double mDotSim = interPol(inter[0][0],x[0],mDot);
//    double myLoM  = (x[8]/mDotSim)/pow(pow(mass/massReference,2),0.25);
//    double myLoM  = (x[8]/mDotSim)/pow(mass/massReference,0.5);
    // LoM is a misnomer, it gives the temperature scaling 
    double myLoM  = pow(x[8]/mDotSim,0.25)/pow(mass/massReference,0.5);
    // T propto mDot^0.25, weight propto mDot^0.75 and propto mass^0.5
    double flSc = pow(x[8]/mDotSim,0.75)*pow(mass/massReference,0.5);
    // reflection scales with 1/area of receiving ring
    ampl = pow(distanceReference_kpc/x[9],2)*flSc;

    reflSc=flSc/pow(mass/massReference,2);

    double mDot = x[8]/mDotSim;
    double chi  = x[10];
    
    if (printFlag)
      {
	cout<<"Parameters:"<<endl;
	cout<<"a "<<x[0]<<endl;

	cout<<"Simulated mDot: "<<mDotSim<<endl;
	cout<<"myLoM "<<myLoM<<endl;
	cout<<"Ampl "<<ampl<<endl;
	cout<<"mDot/mDotSim "<<mDot<<endl;
      }

    // define vector that tells us where we want to evaluate our function
    vector<double>parList;
    parList.push_back(x[0]); // a
    parList.push_back(myLoM); // LoM
    parList.push_back(x[3]); // tempC
    parList.push_back(x[4]); // tauC
    parList.push_back(x[5]); // thetaC
    parList.push_back(x[6]); // rC
    parList.push_back(90.-x[7]); // incl
    
    vector<float> xillverPar;
    if (x[12]>=0.5)
      xillverPar.push_back(x[12]); // AFe
    else 
	xillverPar.push_back(1.); // AFe

    xillverPar.push_back(x[13]); // kTe
    xillverPar.push_back(x[14]); // dens

    if (fabs(x[12]+1)<0.01)
      xillver->switchOff();
    else
      xillver->switchOn();

    if (printFlag)
	cout<<"Xillver on? "<<xillver->isOn()<<endl;
    /*
    if (fabs(x[12]+0.5)<0.01)
      xillver->xiSwitchOn();
    else
      xillver->xiSwitchOff();
    */

    eFit=x[15];
    alpha=x[16];
    fl1=x[17];
    fl2=x[18];

    //    xillverPar.push_back(90.-x[7]); // inclination
//    pv(xillverPar,"xillverPar handed down from Python");
    
    // for each dimension, save the dimension, the index offset, and the normalized interpolation value
    vector<EDGE>edgeList;
    for (int i=0;i<(int)parList.size();i++)
        edgeList.push_back(edge(i,inter[geom][i],parList[i]));
    
    // get indices of distant corner of the N-dim cube
    vector<int> coord;
    coord.assign((int)edgeList.size(),1);
    for (int i=0;i<(int)edgeList.size();i++)
        coord[i]+=edgeList[i].listIndex;
    
    // sort according to x, smallest x first
    std::sort(edgeList.begin(), edgeList.end(), edgeCompare);
    
    vector<vector<int>> points;
    points.push_back(coord);
    
    // go down the monkey bars to the near edge of the N-dim cube
    //    std::cout<<"After sorting "<<std::endl;
    //    cout<<"Node information:"<<endl;

    for (auto v :edgeList)
    {
      //      cout<<"dim "<<v.dimension<<" "<<dimName[v.dimension]<<" listIndex "<<v.listIndex<<" x "<<v.x<<std::endl;
      coord[v.dimension]-=1;
      points.push_back(coord);
    }
    
    // convert indices into strings that identify the data set
    vector<string>idName;
    vector<int>idNum,tNum,aNum;
    // loop over all the nodes that need evaluation
    lastDataSet.clear();
    lastId.clear();


    for (auto p :points)
    {
        string s;
        int a,st;
        int i=0;
        int id=0;
        // for each node, get the parameters, and thus the id string
	//        cout <<">> "<<i<<endl;
        for (auto x :p)
        {
            if (i<6)
                s+=to_string(x);

            if (i==0)
                a=x;

            if (i==6)
                st=x;
            
            id+=x*multiplier[i];
	    //            cout <<" x "<<x<<" m "<<multiplier[i]<<" id "<<id<<endl;

            if (i==0) s+=to_string(geom);
            i++;
        }
        idName.push_back(s);
        tNum.push_back(st);
        aNum.push_back(a);
        idNum.push_back(id);
	lastDataSet.push_back(id);
	lastId.push_back(s);

	//	cout<<"Data set "<<s<<" id "<<id<<" st "<<st<<endl;
    }
    
    // let's get all the datas sets we need.

    sReset(hTh);
    for (int i=0;i<(int)idName.size();i++){
        bool newConfig=false;
        int myId=idNum[i];
        if(mem[myId]<0)
            {
                // do thermal analysis

	      string pathName=path(idName[i]);
	      int it=readData(pathName,"t",idName[i],tNum[i],resT);
	      // fill histogram
	      //	      cout<<"Using "<<pathName<<endl;
	      sHist hTemp; sInit(hTemp);
	      thermalAnalysis(tNum[i],resT,it,hTemp);
	      // remember where you stored the histogram
	      mem[myId]=(int)lTherm.size();
	      // store it
	      lTherm.push_back(hTemp);
	      lTherm1.push_back(fl1);
	      lTherm2.push_back(fl2);
	      if ((int)lTherm.size()%100==0)
		cout<<"Thermal size "<<lTherm.size()<<endl;
	      // do disk analysis
	      // cout<<"Reading disk data"<<endl;
	      it=readData(pathName,"d",idName[i],-1,resT);
	      // cout<<"Analyzing disk data"<<endl;
	      
	      DISK myDisk(aNum[i],xillver->numBin());
	      // fill vectors with histograms, flux, and gamma-values
	      diskAnalysis(resT,it,myDisk);
	      // cout<<"disk analysis idName[i] "<<idName[i]<<" tNum[i] "<<tNum[i]<<endl;
	      
	      // store it
	      lDisk.push_back(myDisk);
	      if((int)lDisk.size()%100==0)
		cout<<"Disk size "<<lDisk.size()<<endl;
	      
	      lRefl1.push_back(idName[i]);
	      lRefl2.push_back(tNum[i]);
	      lRefl3.push_back(xillverPar);
	      lRefl4.push_back(eFit);
	      lRefl5.push_back(alpha);
	      lRefl6.push_back(aNum[i]);
	      newConfig=true;
            }
	
        int memory = mem[myId];

	bool redoTher=((fl1!=lTherm1[memory])||(fl2!=lTherm2[memory]));
	bool redoDisk=(redoTher)||(eFit!=lRefl4[memory]);
	bool redoRefl=(redoDisk)||(!(xillverPar==lRefl3[memory]))||(alpha!=lRefl5[memory]);

	//	cout<<"redoTher "<<redoTher<<endl;
	//	cout<<"redoDisk "<<redoDisk<<endl;
	//	cout<<"redoRefl "<<redoRefl<<endl;

	if (redoTher)
	  {
	      string pathName=path(lRefl1[memory]);
	      int it=readData(pathName,"t",lRefl1[memory],lRefl2[memory],resT);
	      sHist hTemp; sInit(hTemp);
	      thermalAnalysis(lRefl2[memory],resT,it,hTemp);
	      lTherm[memory].swap(hTemp);
	      lTherm1[memory]=fl1;
	      lTherm2[memory]=fl2;
	  }

	if (redoDisk)
	  {
	      string pathName=path(lRefl1[memory]);
	      int it=readData(pathName,"d",lRefl1[memory],-1,resT);
	      lDisk[memory].reset();
	      diskAnalysis(resT,it,lDisk[memory]);
	      lRefl4[memory]=eFit;
	  }

	if ((newConfig)||(redoRefl))
	  {
	    string pathName=path(lRefl1[memory]);
            int it=readData(pathName,"r",lRefl1[memory],lRefl2[memory],resT);
	    
            // fill histogram
	    sHist hTemp; sInit(hTemp);
            reflAnalysis(lRefl2[memory],reflSc,xillverPar,lDisk[memory],resT,it,hTemp,i==0);
            // deep copies	    
            lRefl3[memory]=xillverPar;
            if (memory<(int)lRefl.size())
	      lRefl[memory].swap(hTemp);
            else
	      lRefl.push_back(hTemp);
	    //	    if ((int)lRefl.size()%20==0)
	    //	      cout<<"#1 "<<memory<<" lRefl3.size() (xillverpar) "<<lRefl3.size()<<" lRefl.size() (reflected spectrum) "<<lRefl.size()<<endl;
	      lRefl5[memory]=alpha;
	  }
        
        // add contents of all histograms
        float reflAmpl=x[11];
	//        for (int s=0;s<nStokes;s++)

        for (int s=0;s<nStokes;s++)
	  {
	    //            if (s==0)
	    //	    cout<<"Adding up results i "<<i<<" s "<<s<<" memory "<<memory<<" refl "<<reflAmpl<<endl;
            if (i==0)
	      {
		//	cout<<"thermal"<<endl;
                hTh[s].copy(lTherm[memory][s]);
		//		hTh[s].pprint();
		//cout<<"adding reflected"<<endl;
                hTh[s].add(lRefl[memory][s],reflAmpl);
		//hTh[s].pprint();
		//		lRefl[memory][s].print();
		//hTh[s].print();
            }
            else
            {
                double fac=1.-edgeList[i-1].x;
                int previous=mem[idNum[i-1]];
		//                if (s==0)
		//cout<<"i "<<i<<" fac "<<fac<<" previous memory "<<previous<<endl;
		//cout<<"?? "<<i<<" adding thermal"<<endl;
                hTh[s].add(lTherm[memory][s],fac);
		//hTh[s].pprint();

		//cout<<"?? "<<i<<" adding reflectedl"<<endl;
                hTh[s].add(lRefl[memory][s],fac*reflAmpl);
		//hTh[s].pprint();

		//cout<<"?? "<<i<<" adding previous thermal"<<endl;
                hTh[s].add(lTherm[previous][s],-fac);
		//	hTh[s].pprint();

		//cout<<"?? "<<i<<" adding previous reflected"<<endl;
                hTh[s].add(lRefl[previous][s],-fac*reflAmpl);
		// hTh[s].pprint();
		//		if (s==0)
		//  hTh[s].print();
            }
        }
	//        hTh[0].print();
    }

//    raise to the power of 10
//    cout<<"raising to the power of 10"<<endl;
//    for (auto &h : hTh)
//        for (int i=0;i<h.bins();i++)
//            h.set(i,pow(10.,h.get(i)));

    // scale and rotate

    //    cout<<"Before scaling "<<endl;
    //    hTh[0].pprint();
    for (auto &h : hTh)
        h.scale(ampl);

    //    cout<<"Stokes Q"<<endl;
    //hTh[1].print();
    //cout<<"Stokes U"<<endl;
    //hTh[2].print();

    if (nStokes>1)
      rotate(hTh[1],hTh[2],chi);

    //cout<<"Stokes QQ"<<endl;
    //hTh[1].print();
    //cout<<"Stokes UU"<<endl;
    //hTh[2].print();

    //    cout <<"Done in getI: "<<endl;
    
    //    for (int i=0;i<hTh[0].bins();i++)
    //        cout <<i<<" "<<hTh[0].get(i)<<" | ";
//    hTh[0].print();
//    cout<<endl;

//    cout<<"Before returning "<<endl;
//    hTh[0].pprint();
    return getNumpyInterpol(hTh[0]);
}

np::ndarray dataReader::getQ()
{
//    hTh[1].print();
  if (nStokes<2)
    {
      cout<<"increase nStokes"<<endl;
        exit (EXIT_FAILURE);
    }
  //  cout<<"get Q"<<endl;
  //hTh[1].print();
  return getNumpyInterpol(hTh[1]);
}

np::ndarray dataReader::getU()
{
//    hTh[2].print();
  if (nStokes<3)
    {
      cout<<"increase nStokes"<<endl;
        exit (EXIT_FAILURE);
    }
  //cout<<"get U"<<endl;
  //hTh[1].print();
  return getNumpyInterpol(hTh[2]);
}

np::ndarray dataReader::getNode(int id,int result)
{
// result == 0 get spectral indices as a function of radius
// result == 1 get xi as a function of radius
// result == 2 get radius for all bins
// result == 3,4,5 thermal emission,I,Q,U
// 7 energies of disk.histo
// 8 energies of disk rebin
// 9 xi
// 11,11,12 i,q,u therm
// 13 energies of lTherm
// 14 get Xillver data sets
// 15 get node list

// result == 100+radBin get histogram of incident energies
// result == 2000+radBin*10+inclBin get histogram of emitted energies
// result == 3000+radBin*10+inclBin get histogram of XILLVER energies



/*    int id=0;
    for (int i=0;i<7;i++)
    {
        int j=stoi(str.substr(i,1));
        id +=j*multiplier[i];
    }
*/
    if (result==15)
      {
	cout<<"I used the following nodes:"<<endl;
	for (auto x : lastId)
	  {
	    cout <<x<<endl;
	  }
	cout<<endl;

	cout<<"I used the following data sets:"<<endl;
	vector <float>out;
	for (auto x : lastDataSet)
	  {
	    cout <<x<<",";
	    out.push_back((float)x);
	  }
	cout<<endl;

	cout <<"The last used amplitude is: "<<ampl<<endl;
	cout <<"The last used reflSc is: "<<reflSc<<endl;
	return getNumpy(out);
      }


    if(mem[id]<0)
    {
        cout<<" myId "<<id<<" has not been loaded "<<endl;
        return getNumpy(0);
    }
    
    if (result==0)
      return getNumpy(lDisk[mem[id]].flux2,reflSc);

    if (result==1)
        return getNumpy(lDisk[mem[id]].gamma);

    if (result==2)
    {
        auto & r = lDisk[mem[id]];
        vector<float> rad;
        for (int i=0;i<r.numBins();i++)
            rad.push_back(r.binRadius(i));
        return getNumpy(rad);
    }
    
    if (result==3)
      return getNumpy(lTherm[mem[id]][0],ampl);
    if (result==4)
      return getNumpy(lTherm[mem[id]][1],ampl);
    if (result==5)
      return getNumpy(lTherm[mem[id]][2],ampl);
    
    if (result==7) // energies of disk.histo
    {
        auto & h = lDisk[mem[id]].histo[0];
        vector<float> energy;
        for (int i=0;i<h.bins();i++)
            energy.push_back(pow(10.,h.getX(i)));
        return getNumpy(energy);
    }

    if (result==8) // energies of disk.rebin
    {
        vector<float> energy;
        for (int i=0;i<xillver->numBin();i++)
            energy.push_back(pow(10.,xillver->xVal(i)));
        return getNumpy(energy);
    }
    
    if (result==9)
        return getNumpy(lDisk[mem[id]].xi);

    if (result==10) // I
      return getNumpy(lRefl[mem[id]][0],ampl);
    if (result==11) // Q
      return getNumpy(lRefl[mem[id]][1],ampl);
    if (result==12) // U
      return getNumpy(lRefl[mem[id]][2],ampl);
    
    if (result==13) // energies of hTh
        {
            auto & h = lTherm[mem[id]][0];
            vector<float> energy;
            for (int i=0;i<h.bins();i++)
                energy.push_back(pow(10.,h.getX(i)));
            return getNumpy(energy);
        }

    if (result==14)
      {
	xillver->listXill();
	return getNumpy(0.);
      }

    if ((result>=100)&&(result<200))
    {
        int bin=result-100;
	//        lDisk[mem[id]].histo[bin].print();

	//        auto & h = lDisk[mem[id]].histo[bin];
        auto & h = lDisk[mem[id]].histoEflux[bin];
        vector<float> v;
        for (int i=0;i<h.bins();i++)
	  v.push_back(reflSc*h.get(i)/h.getXDeltaPow10(i));

        return getNumpy(v);
    }

    if ((result>=2000)&&(result<3000))
    {
      // format 2xxy xx: radialbin, y:inclbin
        int bin=result-2000;
	int rBin =bin/10;
	int inclBin=bin-rBin*10;
        vector<float> r=lDisk[mem[id]].rebin[rBin][inclBin];
	//        cout<<"Length of vector "<<r.size()<<endl;
	int i=0;
	for (auto &x : r) 
	    x*=reflSc/xillver->xDelta(i++);
        return getNumpy(r);
    }
    if ((result>=3000)&&(result<4000))
    {
        int bin=result-3000;
	int rBin =bin/10;
	int inclBin=bin-rBin*10;
        vector<float> r=xillver->getXillver(rBin);
        return getNumpy(r);
    }
    return getNumpy(0);
}
}

BOOST_PYTHON_MODULE(dataReader)
{
    Py_Initialize();
    np::initialize();
    
    using namespace boost::python;
    class_<dataReader>("dataReader")
    .def("getI", &dataReader::getI)
    .def("getQ", &dataReader::getQ)
    .def("getU", &dataReader::getU)
    .def("setBins", &dataReader::setBins)
    .def("getNode", &dataReader::getNode)
    .def("getEnergies", &dataReader::getEnergies)
    ;
}
/*
 int main(int argc, char** argv)
 {
 dataReader in;
 
 array_type resT(boost::extents[Nemis][nTher]);
 
 string pathName="/Users/henric/data/outX/0020/";
 string coronaId="0020000";
 
 int it=in.readData(pathName,"e",coronaId,"t0",resT);
 int ir=in.readData(pathName,"e",coronaId,"r0",resR);
 int id=in.readData(pathName,"d",coronaId,"d",resD);
 
 in.thermalAnalysis(pathName,coronaId,0,resT,it);
 
 cout<<"Done"<<endl;
 }
 */

// === END ===

