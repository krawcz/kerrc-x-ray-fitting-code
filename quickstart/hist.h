// histogram class
// Henric Krawczynski 6/22/2020

// C++
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
// C
#include <stdio.h>

using namespace std;

class Hist {    
  int n;
  double x1,x2,delta;
  vector<double>cont;
  vector<double>xS;
  vector<double>contSq; // add for error calculation
  int Num; // entries (# of times fill was called)
  bool error;
  string name;

 public:
    Hist(int In,double Ix1, double Ix2,string myName,string opt);

  int bins() {return n; };
  double getX1() {return x1; };
  double getX2() {return x2; };
  string getName() {return name; };

  int index(double x);
  
  // modify contents
  void reset(){
    for (auto &c : cont) c=0.;
    if (error)
      for (auto &c : contSq) c=0.;    
    Num=0;}

  void fill(double x,double w);

  double set(int k,double val) {cont[1+k]=val; return val;};
  double get(int k) {return cont[1+k]; };
  double get(double x) {return cont[index(x)]; };
  double getSq(int k) {return contSq[1+k]; };

  double interPol(double x,double dx,bool flag);
  double *getData(){return &cont[1];};

  double setRaw(int k,double val) {cont[k]=val; return val;};
  double getRaw(int k) {return cont[k]; };
  double getRawSq(int k) {return contSq[k]; };
  
  double getX(int k);
  double getXDeltaPow10(int k);
  int entries() {return Num;};

  // math
  void copy(Hist &h);
  void add(Hist &h, double fac);
  void scale(double fac);

  // output
  bool print(int kMax);
  bool pprint();
  bool write(string fName,string id);
};

// creator
Hist::Hist(int In,double Ix1, double Ix2,string myName="std",string opt="")
{
    name=myName;
    Num=0;
  n=In;
  x1=Ix1;
  x2=Ix2;
  delta=(x2-x1)/(double)n;

    int nD=n+2;
    xS.resize(nD);
    double x=x1;

    xS[0]=0.;
    xS[n+1]=0.;
    for (int i=1;i<=In;i++)
    {
        xS[i]=x;
        x+=delta;
    }
    
  cont.assign(nD,0.);
  error=(opt.compare("E")==0);
  if (error)
    contSq.assign(nD,0.);
}

// get index for value x
int Hist::index(double x)
{
  if (x<x1) return 0;
  if (x>=x2) return n+1;
  return 1+(int)((x-x1)/delta);
}

double Hist::getX(int k)
{
  return x1+((double)k+0.5)*delta;
}

double Hist::getXDeltaPow10(int k)
{
  double res1=x1+((double)k)*delta;
  double res2=x1+((double)k+1.)*delta;
  return pow(10.,res2)-pow(10.,res1);
}

double Hist::interPol(double x,double dx,bool flag)
{
//   cout<<"Interpol "<<x<<" "<<x1<<" "<<x2<<endl;
    
    if (x<=x1) return cont[1];
    if (x>=x2) return cont[n];

    int i=index(x);
//    cout <<"index i"<<i<<endl;
    double y1=cont[i];
    double y2=cont[i+1];
    double rest=x-xS[i];
    double xc=rest/delta;
    double interP=(1.-xc)*y1+xc*y2;
//    double res=exp(interP);
    if (flag)
      cout<<" x "<<x<<" bin "<<i<<" y1 "<<y1<<" y2 "<<y2<<" ipolVal "<<interP<<" dx "<<dx<<endl;
    return interP*dx;
}

//  fill entry
void Hist::fill(double x,double w=1.)
{
  int k=index(x);
  cont[k]+=w;
  Num++;
  if (error)
    contSq[k]+=w*w;
}

void Hist::copy(Hist &h)
{
    if (h.bins()!=n) {std::cout<<"histogram copy n mismatch "<<n<<" "<<h.bins(); return;}
    for (int i=0;i<(int)cont.size();i++)
    cont[i]=h.getRaw(i);
}

// add second histogram
void Hist::add(Hist &h,double fac=1.)
{
  for (int k=0;k<n+2;k++)
    {
      cont[k]+=fac*h.getRaw(k);
      if (error)
	contSq[k]+=fac*h.getRawSq(k);
    }
  Num+=h.entries();
}

// scale histogram contents
void Hist::scale(double fac)
{
  for (int k=0;k<n+2;k++)
    cont[k]*=fac;
}

// print histogram contents to screen
bool Hist::print(int kMax=5)
{
    cout<<"Hist "<<name<<" x-axis: "<<x1<<" - "<<x2<<" delta "
        <<delta<<" U "<<cont[0]<<" O "<<cont[n+1]<<endl;

  for (int k=1;k<std::min(kMax,n+1);k++)
    if (!error)
      cout <<k<<" "<<x1+((double)k-0.5)*delta<<" "<<cont[k]<<endl;
    else
      cout <<k<<" "<<x1+((double)k-0.5)*delta<<" "<<cont[k]<<" "<<sqrt(contSq[k])<<endl;

  return true;
}

bool Hist::pprint()
{
  cout<<"[";
  for (int k=1;k<n+1;k++)
    if (k<n)
      cout <<cont[k]<<",";
    else
      cout <<cont[k]<<"]"<<endl;

  return true;
}

// write histogram contents to file
bool Hist::write(string fName,string id)
{
  ofstream out;
  out.open(fName, fstream::out | fstream::app);

  if ( !out.is_open()) {cout << "couldn't open output file "<<fName << endl; return false;}
  out <<id<<" ";
  for (int k=0; k<n; k++) 
    if (!error)
      out <<cont[1+k]<<" ";
    else
      out <<cont[1+k]<<" "<<sqrt(contSq[1+k])<<" ";

  out<<endl;
  out.close();

  cout <<"Wrote to file "<<fName<<endl;
  return true;
}

