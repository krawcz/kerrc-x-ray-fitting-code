scl enable devtoolset-9 bash
conda activate ciao-4.13

export CPATH=/data/krawcz/boost/include/boost
export LIBRARY_PATH=/data/krawcz/boost/lib
export LD_LIBRARY_PATH=/data/krawcz/boost/lib:$LD_LIBRARY_PATH

h5c++ -fPIC -O0 -fno-inline -Wall -g    -I"/data/krawcz/boost_1_76_0" -I"/faculty/krawcz/.conda/envs/ciao-4.13/include/python3.8"  -c -o "dataReader.o" "dataReader.cpp"

g++ -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_BSD_SOURCE -O2 -g -pipe -Wall -Wp,-D_FORTIFY_SOURCE=2 -fexceptions -fstack-protector-strong --param=ssp-buffer-size=4 -grecord-gcc-switches -m64 -mtune=generic dataReader.o -o dataReader.so -Wl,-h -Wl,dataReader.so -shared -Wl,--start-group -Wl,-Bstatic -Wl,-Bdynamic -L/data/krawcz/boost/lib -Wl,--end-group -fPIC -g -L/usr/lib64 -lboost_python -ldl -lpthread -lutil /usr/lib64/libhdf5_hl_cpp.so /usr/lib64/libhdf5_cpp.so /usr/lib64/libhdf5_hl.so /usr/lib64/libhdf5.so -Wl,-z,relro -lsz -lz -ldl -lm -Wl,-rpath -Wl,/usr/lib64


cd existing_folder
git init 
git remote add origin git@gitlab.com:krawcz/quickstart.git
git add .
git commit -m "Initial commit"
git push -u origin main

