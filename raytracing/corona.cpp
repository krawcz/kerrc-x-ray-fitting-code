//
//  corona.cpp
//  
//
//  Created by Henric Krawczynski on 7/16/20.
//
#include <iostream>
#include <string>

#include "xTrack.h"
#include "xTrackMath.h"
#include "metric.h"
#include "macro.h"
#include "kerr.h"

#include "corona.h"
#include "par.h"

void Corona::init(std::string id)
{
    initFlag=true;

    Par par;

    std::cout <<"++ Initializing Corona ++"<<std::endl;

    config=par.config(id);
    std::cout <<"Configuration "<<config<<std::endl;

    a=par.a(id);
    std::cout <<"Spin parameter "<<a<<std::endl;

    geometry=par.geometry(id);
    std::cout<<"Geometry "<<geometry<<std::endl;

    LoM = par.LoM(id);
    std::cout <<"L over M "<<LoM<<std::endl;
    
    tempC=par.tempC(id);
    std::cout <<"tempC "<<tempC<<std::endl;

    thetaC = par.thetaC(id);
    std::cout <<"thetaC "<<thetaC<<" degree"<<std::endl;

    dtheta = thetaC/RtD;
    std::cout <<"dtheta "<<dtheta<<" radian"<<std::endl;

    rC = par.rC(id);
    std::cout <<"rC "<<rC<<std::endl;

    rC2 = par.rC2(id);
    std::cout <<"rC2 "<<rC2<<std::endl;

    tauC = par.tauC(id);
    std::cout <<"tauC "<<tauC<<std::endl;

    hard=1.8;
    minHeight=tan(dtheta);
    zCorona = rC *sin(dtheta);
    xCorona = rC2*sin(dtheta);
        
    fname=id+".dat";
    if (tauC>0.)
        activeFlag=true;
            
    seed=-100;
}

bool Corona::inCorona(double *y)
{
  if (geometry==0)
    return (y[1]*sin(y[2])>=r1)&&(y[1]<=rC)&&(fabs(PI/2.-y[2])<dtheta);
  else
    return (y[1]>=rC)&&(y[1]<=rC2)&&(y[2]<dtheta);
}

bool Corona::inRegion(double *y)
{
  if (geometry==0)
    return ((y[1]<=rC)&&(y[1]*sin(y[2])>=r1)&&(y[1]*cos(y[2])<zCorona));
  else
    return ((y[1]<rC2)&&(y[1]*cos(y[2])>=rC)&&(y[1]*sin(y[2])<xCorona));
}

double Corona::height(double r, double theta)
{
  if (geometry==0)
    return yheight(r,theta);
  else
    return xheight(r,theta);
}


double Corona::yheight(double r, double theta)
{
  //  std::cout<<"c1 "<<r<<" "<<theta<<" "<<minHeight<<std::endl;

  if (r<r1) return minHeight;
  if (r<=rC)
    return r*sin(dtheta);

  if (r>rC) r=rC;

  double x=r*sin(theta);
  if (x<rC)
    {
      double y=sqrt(rC*rC-x*x);
      if (y<minHeight) y=minHeight;
      return y;
    }
  return minHeight;
}
double Corona::xheight(double r, double thet)
{
  return rC2-rC;
}

double Corona::maxDtau()
{
    double res=0.02*tauC;
    if (res>0.05) res=0.02;
    return res;
}

void Corona::loadEmunu(double *y)
{
    BZ_emunu(y,emunu,enumu);
}

void Corona::loadEmunu(double *y, double *yn)
{
    double yAv[4];
    for (int i=0;i<4;i++)
        yAv[i]      =(yn[i]+y[i])/2.;
    BZ_emunu(yAv,emunu,enumu);
}


// attention: always call loadEmunu first
double Corona::dtau(double y[4], double yn[4])
{
    double dispBL[4];
    for (int i=0;i<4;i++)
        dispBL[i]   = yn[i]-y[i];
        
    double dispZAMO[4];
    for (int i=0;i<4;i++)
    {
        dispZAMO[i]=0;
        for (int mu=0; mu<4; mu++)
            dispZAMO[i] += enumu[i][mu]*dispBL[mu];  //BB
    }
    
    double dl = sqrt(sqr(dispZAMO[1])+sqr(dispZAMO[2])+sqr(dispZAMO[3]));
    double res = tauC*dl/height((y[1]+yn[1])/2.,(y[2]+yn[2])/2.);
    
    
    if (debugFlag)
    {
        std::cout<<"AA "<<tauC<<" "<<dl<<" "<<height((y[1]+yn[1])/2.,(y[2]+yn[2])/2.)
        <<" "<<(y[1]+yn[1])/2.<<" "<<(y[2]+yn[2])/2.<<std::endl;
    }
    return res;
}

double Corona::correct(double r, double t,double rn, double tn)
{
  if (geometry==0)
    {
      double y1 =  r*cos( t);
      double y2 = rn*cos(tn);

      double h1=yheight(r,t);
      double h2=yheight(rn,tn);

      double delta1=h1-y1;
      double delta2=y2-h2;

      if (std::signbit(delta1)==std::signbit(delta2))
	{
	  double res=fabs(delta1)/(fabs(delta1)+fabs(delta2));
	  return res;
	}

      double s1=r1-r*sin(t);
      double s2=rn*sin(tn)-r1;
      if (std::signbit(s1)==std::signbit(s2))
	{
	  double res=fabs(s1)/(fabs(s1)+fabs(s2));
	  return res;
	}
      
      s1=rC-r*sin(t);
      s2=rn*sin(tn)-rC;
      if (std::signbit(s1)==std::signbit(s2))
	{
	  double res=fabs(s1)/(fabs(s1)+fabs(s2));
	  return res;
	}
    }
  else
    {
      bool flagR1=((r<rC )&&(rn>=rC ))||((rn<rC )&&(r>=rC ));
      if (flagR1)
	return fabs((r-rC)/(rn-r));

      bool flagR2=((r<rC2 )&&(rn>=rC2))||((rn<rC2)&&(r>=rC2));
      if (flagR2)
	return fabs((r-rC2)/(rn-r));
      
      bool flagT=((t<dtheta)&&(tn>=dtheta))||((tn<dtheta)&&(t>=dtheta));
      if (flagT)
	return fabs((t-dtheta)/(tn-t));
    }
  return 1.;
}

bool Corona::scatter(long int total,Photon *gamma_p, double u[4], double f_corona[4], double fac){
    
    //transform u ZAMO
    double uZAMO[4];
    double fZAMO[4];
    
    for (int i=0;i<4;i++)
    {
        uZAMO[i]=0.;
        fZAMO[i]=0;
        for (int mu=0; mu<4; mu++)
        {
            uZAMO[i] += enumu[i][mu]*u[mu];
            fZAMO[i] += enumu[i][mu]*f_corona[mu];
        }
    }

    if (uZAMO[0]<0)
        if(gamma_p->E<0)
        {
            uZAMO[0]*=-1;
            uZAMO[1]*=-1;
            uZAMO[2]*=-1;
            uZAMO[3]*=-1;
        }
    
    double mag1= -1*uZAMO[0]*fZAMO[0]+uZAMO[1]*fZAMO[1]+uZAMO[2]*fZAMO[2]+uZAMO[3]*fZAMO[3];
    double mag2= -1*uZAMO[0]*uZAMO[0]+uZAMO[1]*uZAMO[1]+uZAMO[2]*uZAMO[2]+uZAMO[3]*uZAMO[3];
    if(fabs(mag1)>0.1 || fabs(mag2)>0.5)
        gamma_p->timeout=1000001;
    
    //give electron random direction
    double cos_theta_e=1-(2*ran1(&seed));
   
    double phi_e = 2.*PI*ran1(&seed);
    
    double n[4]={0.,0.,0.,0.};
    n[1] = sqrt(1-sqr(cos_theta_e))*cos(phi_e);
    n[2] = -cos_theta_e;
    n[3] = sqrt(1-sqr(cos_theta_e))*sin(phi_e);

    
    
    //Find Gamma (Lorentz Factor of Electron) Using distribution (Maxwell-Boltzman) (using Schnittman&Krolik 2013, Appendix B)
    
    double gamma_e= Gamma_MB(tempC);
    double beta_e = sqrt(1 - 1/pow(gamma_e,2));
    
    // find angle between elecron and photon before scattering in ZAMO frame
    
    double Cos_angle_oframe = 0;
    double ph_mag_oframe = sqrt(pow(uZAMO[1],2)+pow(uZAMO[2],2)+pow(uZAMO[3],2));
    double n_mag_oframe = sqrt(pow(n[1],2)+pow(n[2],2)+pow(n[3],2));
    
    for (int i=1; i<4; i++)
        Cos_angle_oframe += uZAMO[i]*n[i];
    
    Cos_angle_oframe/=(ph_mag_oframe*n_mag_oframe);
    
    double  weight_e_p=1-beta_e*Cos_angle_oframe;
    
    //construct Lorentz transformation and inverse (Misner,Thorne & Wheeler 1973, pg.69)
    double LAMBDA[4][4];
    double LAMBDAi[4][4];  //inverse of LAMBDA
    
    Lorentz(gamma_e, beta_e, n, LAMBDA, LAMBDAi);   // Lorentz Transformation Metrix
    
    //transform u and f into electron Rest Frame
    double uERF[4];
    double fERF[4];
    
    for (int i=0; i<4; i++)
    {
        uERF[i]=0.;
        fERF[i]=0.;

        for (int j=0; j<4; j++)
        {
            uERF[i] += LAMBDA[i][j]*uZAMO[j];
            fERF[i] += LAMBDA[i][j]*fZAMO[j];

        }
    }

    for (int i=1; i<4; i++) {
        fERF[i]=fERF[i]-(fERF[0]/uERF[0])*uERF[i];
    }
    fERF[0]=0;
    
    //give photon a random scattering direction in ERF frame
    double cos_theta_ERF=1-(2*ran1(&seed));
    //double theta_ERF = PI*ran1(&seed);
    double phi_ERF = 2.*PI*ran1(&seed);
    
    double m[4]={1.,0.,0.,0.};
    m[1] = sqrt(1-sqr(cos_theta_ERF))*cos(phi_ERF);
    m[2] = -cos_theta_ERF;
    m[3] = sqrt(1-sqr(cos_theta_ERF))*sin(phi_ERF);
    
    
    //FIND ANGLE BETWEEN uERF AND M[]
    double um_Cos_angle = 0;
    double uERF_mag = sqrt(pow(uERF[1],2)+pow(uERF[2],2)+pow(uERF[3],2));
    double m_mag = 1;
    
    for (int i=1; i<4; i++)
        um_Cos_angle += (uERF[i]*m[i]);
    
    um_Cos_angle /= (uERF_mag*m_mag);
        
    // define basis and finding the f vecotr in these basis
    
    double k1n[3];
    normalize(&uERF[1],k1n);
    
    double k2n[3];
    normalize(&m[1],k2n);
    
    double e_perp[3];
    cross(k2n,k1n,e_perp);
    normalize(e_perp,e_perp);
    
    
    double e_para[3];
    cross(k1n,e_perp,e_para);
    normalize(e_para,e_para);
    
    
    double f1n[3];
    normalize(&fERF[1],f1n);
    
    double Cos_polAngl=dot(f1n,e_para);
    double Sin_polAngl=dot(f1n,e_perp);
    
    double jam=sqr(Cos_polAngl)+sqr(Sin_polAngl);
    if((jam<0.95)||(jam>1.05)){
        //cout<<" jam "<<sqr(Cos_polAngl)+sqr(Sin_polAngl)<<" num "<<gamma_p->totalN<<endl;
        gamma_p->timeout=1000001;
    }
    
    
    /*    double fERF_test[3];
    for (int i=0; i<3; i++) {
        fERF_test[i]=Cos_polAngl*e_para[i]+Sin_polAngl*e_perp[i];
    }
    */

    double PolAngl=atan2(Sin_polAngl,Cos_polAngl);
    if(PolAngl<0)
        PolAngl+=PI;
    
    double stokes_noscat[3]={1, (gamma_p->pol *cos(2*PolAngl)) , (gamma_p->pol *sin(2*PolAngl))};
        
    // finding new energy
    double Escale0=(uERF[0]*fabs(gamma_p->E))/511.;
    // cout <<"x "<<x<<endl;
    
    double f=1/(1.+Escale0*(1-um_Cos_angle));
    double Escale1=f*Escale0;

    double uERF0save=uERF[0]*fabs(gamma_p->E);
    
    for (int i=0; i<4; i++)
        uERF[i]=f*uERF0save*m[i];

    //defin FANO matrix
    
    double FANO[3][3];
    
    for (int i=0; i<3; i++) {
        for (int j=0; j<3; j++) {
            FANO[i][j]=0;
        }
    }
    double norm=0.5; // define norm such that the expression is 1 in the case of Thomson scattering
    // 3./4.;
    FANO[0][0]=norm*pow(f,2)*(1+pow(um_Cos_angle,2)+(Escale0-Escale1)*(1-um_Cos_angle));
    FANO[0][1]=norm*pow(f,2)*(pow(um_Cos_angle,2)-1);
    FANO[0][2]=0;
    FANO[1][0]=norm*pow(f,2)*(pow(um_Cos_angle,2)-1);
    FANO[1][1]=norm*pow(f,2)*(1+pow(um_Cos_angle,2));
    FANO[1][2]=0;
    FANO[2][0]=0;
    FANO[2][1]=0;
    FANO[2][2]=norm*pow(f,2)*(2*um_Cos_angle);
    
    // New stokes after scattering
    
    double stokes_scat[3];
    
    for (int i=0; i<3; i++) {
        stokes_scat[i]=0;
        for (int j=0; j<3; j++) {
            stokes_scat[i] += FANO[i][j]*stokes_noscat[j];
        }
    }
    
    // Use rejection method to deal with Klein Nishina regime:
    double normFac=stokes_scat[0];
    if (fac>normFac) return false;
    // In the Thomson regime, this will reject 1/3 of all events.
    // This is good because it gives the right cos(theta) distribution,
    // however, we have to make up for the lost events by
    // weighing the events that make it with the weight 1.5

    gamma_p->weight *= weight_e_p;

    // define the new basis with scatt
    
    double e_perp_prime[3];
    for (int i=0; i<3; i++) {
        e_perp_prime[i]=e_perp[i];
    }
    
    double e_para_prime[3];
    cross(k2n,e_perp_prime,e_para_prime);
    normalize(e_para_prime,e_para_prime);
    
    PolAngl= getChi(stokes_scat[1],stokes_scat[2]);
    
    double fERF_scat[4];
    fERF_scat[0]=0;
    for (int i=1; i<4; i++) {
      //        fERF_test[i-1]=cos(PolAngl)*e_para_prime[i-1]+sin(PolAngl)*e_perp_prime[i-1];
        fERF_scat[i]=cos(PolAngl)*e_para_prime[i-1]+sin(PolAngl)*e_perp_prime[i-1];
    }
    
    gamma_p->pol = sqrt(pow((stokes_scat[1]),2)+pow(stokes_scat[2],2))/stokes_scat[0];
    
    if (stokes_scat[0]<0) {
        
        //cout<<" stokes_scat[0] <0 "<<stokes_scat[0]<<" ph number "<<gamma_p->totalN<<endl;
        //exit(1);
        gamma_p->timeout=100000;

    }
    
    //transform u and f back into ZAMO
    for (int i=0; i<4; i++)
    {
        uZAMO[i]=0.;
        fZAMO[i]=0;
        for (int j=0; j<4; j++)
        {
            uZAMO[i] += LAMBDAi[i][j]*uERF[j];
            fZAMO[i] += LAMBDAi[i][j]*fERF_scat[j];
        }
    }

//    double normERF =sqr(uERF[0])-sqr(uERF[1])-sqr(uERF[2])-sqr(uERF[3]);
//    double normZAMO =sqr(uZAMO[0])-sqr(uZAMO[1])-sqr(uZAMO[2])-sqr(uZAMO[3]);
//    std::cout<<"normERF "<<normERF<<" normZAMO "<<normZAMO<<std::endl;

    //transform u and f back into BL coordinates
    for (int mu=0;mu<4;mu++)
    {
        u[mu]=0.;
        f_corona[mu]=0;
        for (int i=0; i<4; i++)
        {
            u[mu] += emunu[mu][i] *uZAMO[i];
            f_corona[mu] += emunu[mu][i] *fZAMO[i];
        }
    }
        
    return true;
}


void Corona::BZ_emunu(double *y,double emunu[4][4], double enumu[4][4])
//BB // enumu transforms BL to ZAMO, emunu, inverse metrix, transfroms ZAMO to BL
{
    for (int i=0; i<4; i++){
        for (int j=0; j<4; j++){
            emunu[i][j]=0;
            enumu[i][j]=0;
        }
    }
    double Sigma_metric= sqr(y[1])+sqr(a*cos(y[2]));
    double Delta_metric= sqr(y[1])-2*y[1]+sqr(a);
    double A_metric=sqr(sqr(y[1])+sqr(a))-sqr(a)*Delta_metric*sqr(sin(y[2]));
    
    double rho_sq=sqr(y[1])+sqr(a*cos(y[2]));
    double delta=sqr(y[1])-2*y[1]+sqr(a);
    double alpha=sqrt(rho_sq*delta/(rho_sq*delta+2*y[1]*(sqr(a)+sqr(y[1]))));
    double omega=2*y[1]*a/(rho_sq*delta+2*y[1]*(sqr(a)+sqr(y[1])));
    double omegabar_sq=(rho_sq*delta+2*y[1]*(sqr(a)+sqr(y[1])))*sqr(sin(y[2]))/rho_sq;
    
    emunu[0][0]=1/alpha;
    emunu[1][1]=sqrt(delta/rho_sq);
    emunu[2][2]=sqrt(1/rho_sq);
    emunu[3][3]=sqrt(1/omegabar_sq);
    emunu[3][0]=omega/alpha;
    
    enumu[0][0]=sqrt((Sigma_metric*Delta_metric)/A_metric);
    enumu[1][1]=sqrt(Sigma_metric/Delta_metric);
    enumu[2][2]=sqrt(Sigma_metric);
    enumu[3][3]=sqrt(A_metric/Sigma_metric)*sin(y[2]);
    enumu[3][0]=-2*a*y[1]*sin(y[2])/sqrt(Sigma_metric*A_metric);
}

void Corona::Lorentz(double gamma_e, double beta_e, double *n ,double LAMBDA[4][4], double LAMBDAi[4][4]) //BB // Lorentz Transformation Metrix
{
    for (int i=0; i<4; i++){
        for (int j=0; j<4; j++){
            LAMBDAi[i][j]=0;
            LAMBDA[i][j]=0;
        }
    }
    
    for (int j=1; j<4; j++)
    {
        LAMBDA[0][j] = -beta_e*gamma_e*n[j];
        LAMBDA[j][0] = LAMBDA[0][j];
        LAMBDA[j][j] = (gamma_e - 1)*n[j]*n[j] + 1;
        
        LAMBDAi[0][j] = beta_e*gamma_e*n[j]; //beta -> -beta
        LAMBDAi[j][0] = LAMBDAi[0][j];
        LAMBDAi[j][j] = LAMBDA[j][j];
    }
    LAMBDA[0][0] = gamma_e;
    LAMBDA[1][2] = (gamma_e - 1)*n[1]*n[2];
    LAMBDA[1][3] = (gamma_e - 1)*n[1]*n[3];
    LAMBDA[2][1] = LAMBDA[1][2];
    LAMBDA[2][3] = (gamma_e - 1)*n[2]*n[3];
    LAMBDA[3][1] = LAMBDA[1][3];
    LAMBDA[3][2] = LAMBDA[2][3];
    
    LAMBDAi[0][0] = LAMBDA[0][0];
    LAMBDAi[1][2] = LAMBDA[1][2];
    LAMBDAi[1][3] = LAMBDA[1][3];
    LAMBDAi[2][1] = LAMBDAi[1][2];
    LAMBDAi[2][3] = LAMBDA[2][3];
    LAMBDAi[3][1] = LAMBDAi[1][3];
    LAMBDAi[3][2] = LAMBDAi[2][3];
    
}

double Corona::Gamma_MB(double T)  //BB // find gamma using Mawell- Boltzman distribution
{
    int N=1000;
    double THETA = T/511;
    double gamma_e=0;
    for (int i=0; i<N; i++) {
        
        double lambda0 = ran1(&seed);
        double x0= RootFinder(THETA,lambda0);
        double beta_e = sqrt(1 - 1/pow(x0,2));
        
        double lambda1 =ran1(&seed);
        
        if (beta_e>lambda1) {
            gamma_e=x0;
            i=N;
        }
        
    }
    
    if (gamma_e==0) {
        std::cout<<" Cannot find any Gamma for N="<<N<<std::endl;
    }
    
    return gamma_e;
}

double Corona::RootFinder(double THETA,double lambda0) //BB //newton method
{
    double x0= 1.+THETA;     //giving the first estimate for root //I checked before the root is around this
    double xi=x0;
    double xi_1;
    double gamma;
    double E=0.001;   //error for the root, So this means that the answer is 0 for this root by the error of 0.001
    double root=0.;
    int N=2000;  //The number of loop for newton method
    
    for (int i=0; i<N; i++) {
        
        gamma=xi;
        
        double f_xi=(1-(exp(-gamma/THETA)*(2*pow((THETA),2)+2*THETA*gamma+pow((gamma),2))/(exp(-1/THETA)*(2*pow((THETA),2)+2*THETA+1))))-lambda0;
        
        double df_xi=(1/(exp(-1/THETA)*(2*pow((THETA),2)+2*THETA+1)))*exp(-gamma/THETA)*(pow((gamma),2)/THETA);
        
        xi_1= xi-(f_xi/df_xi);
        
        gamma=xi_1;
        
        double f_xi_1=(1-(exp(-gamma/THETA)*(2*pow((THETA),2)+2*THETA*gamma+pow((gamma),2))/(exp(-1/THETA)*(2*pow((THETA),2)+2*THETA+1))))-lambda0;
        
        if (fabs(f_xi_1)<E) {
            root=gamma;
            i=N;
        }
        
        else{
            xi=xi_1;
        }
        
    }
    
    if (root==0) {
        std::cout<<" root not found "<<"lambda0 "<<lambda0<<" Theta "<<THETA<<std::endl;
    }
    return root;
}

double Corona::planck_sample ( double a, double b, int &seed )
{
    double a2;
    double b2;
    double c2;
    double g;
    double x;
    int z;
    //
    a2 = 0.0;
    b2 = 1.0;
    c2 = b + 1.0;
    
    g = gamma_sample ( a2, b2, c2, seed );
    
    z = zipf_sample ( c2, seed );
    
    x = g / ( a * ( double ) ( z ) );
    
    return x;
}

int Corona::zipf_sample ( double a, int &seed )
{
    double b;
    double t;
    double u;
    double v;
    double w;
    int x;
    
    b = pow ( 2.0, ( a - 1.0 ) );
    
    for ( ; ; )
    {
        u = r8_uniform_01 ( seed );
        v = r8_uniform_01 ( seed );
        w = ( int ) ( 1.0 / pow ( u, 1.0 / ( a - 1.0 ) ) );
        
        t = pow ( ( w + 1.0 ) / w, a - 1.0 );
        
        if ( v * w * ( t - 1.0 ) * b <= t * ( b - 1.0 ) )
        {
            break;
        }
        
    }
    
    x = ( int ) w;
    
    return x;
}

double Corona::gamma_sample ( double a, double b, double c, int &seed )
{
    double a1 =   0.3333333;
    double a2 = - 0.2500030;
    double a3 =   0.2000062;
    double a4 = - 0.1662921;
    double a5 =   0.1423657;
    double a6 = - 0.1367177;
    double a7 =   0.1233795;
    double bcoef;
    double co;
    double d;
    double e;
    double e1 = 1.0;
    double e2 = 0.4999897;
    double e3 = 0.1668290;
    double e4 = 0.0407753;
    double e5 = 0.0102930;
    double euler = 2.71828182845904;
    double p;
    double q;
    double q0;
    double q1 =  0.04166669;
    double q2 =  0.02083148;
    double q3 =  0.00801191;
    double q4 =  0.00144121;
    double q5 = -0.00007388;
    double q6 =  0.00024511;
    double q7 =  0.00024240;
    double r;
    double s;
    double si;
    double s2;
    double t;
    double u;
    double v;
    double w;
    double x;
    //
    //  Allow C = 0.
    //
    if ( c == 0.0 )
    {
        x = a;
        return x;
    }
    //
    //  C < 1.
    //
    if ( c < 1.0 )
    {
        for ( ; ; )
        {
            u = r8_uniform_01 ( seed );
            t = 1.0 + c / euler;
            p = u * t;
            
            s = exponential_01_sample ( seed );
            
            if ( p < 1.0 )
            {
                x = exp ( log ( p ) / c );
                if ( x <= s )
                {
                    break;
                }
            }
            else
            {
                x = - log ( ( t - p ) / c );
                if ( ( 1.0 - c ) * log ( x ) <= s )
                {
                    break;
                }
            }
        }
        
        x = a + b * x;
        return x;
    }
    //
    //  1 <= C.
    //
    else
    {
        s2 = c - 0.5;
        s = sqrt ( c - 0.5 );
        d = sqrt ( 32.0 ) - 12.0 * sqrt ( c - 0.5 );
        
        t = normal_01_sample ( seed );
        x = pow ( ( sqrt ( c - 0.5 ) + 0.5 * t ), 2 );
        
        if ( 0.0 <= t )
        {
            x = a + b * x;
            return x;
        }
        
        u = r8_uniform_01 ( seed );
        
        if ( d * u <= t * t * t )
        {
            x = a + b * x;
            return x;
        }
        
        r = 1.0 / c;
        
        q0 = ( ( ( ( ( (q7*r+ q6 )*r+ q5 )*r+ q4 )*r+ q3 )*r+ q2 )*r+ q1) * r;
        
        if ( c <= 3.686 )
        {
            bcoef = 0.463 + s - 0.178 * s2;
            si = 1.235;
            co = 0.195 / s - 0.079 + 0.016 * s;
        }
        else if ( c <= 13.022 )
        {
            bcoef = 1.654 + 0.0076 * s2;
            si = 1.68 / s + 0.275;
            co = 0.062 / s + 0.024;
        }
        else
        {
            bcoef = 1.77;
            si = 0.75;
            co = 0.1515 / s;
        }
        
        if ( 0.0 < sqrt ( c - 0.5 ) + 0.5 * t )
        {
            v = 0.5 * t / s;
            
            if ( 0.25 < r8_abs ( v ) )
            {
                q = q0 - s * t + 0.25 * t * t + 2.0 * s2 * log ( 1.0 + v );
            }
            else
            {
                q = q0 + 0.5 * t * t * ((((((a7*v+a6)*v+a5)*v+a4)*v+a3)*v+a2)*v+a1)*v;
            }
            
            if ( log ( 1.0 - u ) <= q )
            {
                x = a + b * x;
                return x;
            }
        }
        
        for ( ; ; )
        {
            e = exponential_01_sample ( seed );
            
            u = r8_uniform_01 ( seed );
            
            u = 2.0 * u - 1.0;
            t = bcoef + r8_abs ( si * e ) * r8_sign ( u );
            
            if ( -0.7187449 <= t )
            {
                v = 0.5 * t / s;
                
                if ( 0.25 < r8_abs ( v ) )
                {
                    q = q0 - s * t + 0.25 * t * t + 2.0 * s2 * log ( 1.0 + v );
                }
                else
                {
                    q = q0 + 0.5 * t * t * ( ( ( ( ( (a7*v+a6)*v+a5)*v+a4)*v+a3)*v+a2)*v+a1) * v;
                }
                
                if ( 0.0 < q )
                {
                    if ( 0.5 < q )
                    {
                        w = exp ( q ) - 1.0;
                    }
                    else
                    {
                        w = ( ( ( (e5   * q+ e4 ) * q+ e3 ) * q+ e2 ) * q+ e1 ) * q;
                    }
                    
                    if ( co * r8_abs ( u ) <= w * exp ( e - 0.5 * t * t ) )
                    {
                        x = a + b * pow ( s + 0.5 * t, 2 );
                        return x;
                    }
                }
            }
        }
    }
}

double Corona::r8_uniform_01 ( int &seed )
{
    int k;
    double r;
    
    k = seed / 127773;
    
    seed = 16807 * ( seed - k * 127773 ) - k * 2836;
    
    if ( seed < 0 )
    {
        seed = seed + 2147483647;
    }
    
    r = ( double ) ( seed ) * 4.656612875E-10;
    
    return r;
}

double Corona::exponential_01_sample ( int &seed )
{
    double cdf;
    double x;
    
    cdf = r8_uniform_01 ( seed );
    
    x = - log ( 1.0 - cdf );
    
    return x;
}

double Corona::normal_01_sample ( int &seed )
{
    
    const double pi = 3.14159265358979323;
    double r1;
    double r2;
    double x;
    
    r1 = r8_uniform_01 ( seed );
    r2 = r8_uniform_01 ( seed );
    x = sqrt ( -2.0 * log ( r1 ) ) * cos ( 2.0 * pi * r2 );
    
    return x;
}

double Corona::r8_abs ( double x )
{
    double value;
    
    if ( 0.0 <= x )
    {
        value = x;
    }
    else
    {
        value = -x;
    }
    return value;
}

double Corona::r8_sign ( double x )

{
    if ( x < 0.0 )
    {
        return ( -1.0 );
    }
    else
    {
        return ( 1.0 );
    }
}

