#ifndef par_h
#define par_h

#include <iostream>
#include <string>
#include <vector>

class Par {
  std::vector<double> Va,Vgeometry,VLoM,VtauC,VtempC,Vincl,VthetaC,VrC, WthetaC,WrC,WrC2;
  void linspace(std::vector<double> &res, double x1, double x2, int n);
    
public:
    Par();
    int    config    (std::string str) {return stoi(str.substr(0,1));};
    double a         (std::string str) {return Va[stoi(str.substr(0,1))];};

    int    geometry  (std::string str) {return Vgeometry[stoi(str.substr(1,1))];};
    double LoM       (std::string str) {return VLoM[stoi(str.substr(2,1))];};
    double tempC     (std::string str) {return VtempC[stoi(str.substr(3,1))];};
    double tauC      (std::string str) {return VtauC[stoi(str.substr(4,1))];};

    double thetaC    (std::string str) {if (geometry(str)==0) return VthetaC[stoi(str.substr(5,1))];
      else return WthetaC[stoi(str.substr(5,1))];};

    double rC        (std::string str) {if (geometry(str)==0) return VrC[stoi(str.substr(6,1))];
      else return WrC[stoi(str.substr(6,1))];};

    double rC2       (std::string str) {return WrC2[stoi(str.substr(6,1))];};

    double incl      (std::string str) {return Vincl[stoi(str.substr(7,1))];};
    void check(std::string str);
};

// creator
Par::Par()
{
  Va={-1.,0., 0.5, 0.75, 0.9, 0.95, 0.98, 0.99, 0.998}; // 1 digit
  Vgeometry={0,1}; // 1 digit
  //  VLoM={0.25,0.5,1.,2.}; // original simulation
  //  VLoM={0.03,0.06,0.125,0.25,0.5,1.,2.}; // changed on 7/3/
  VLoM={0.05,0.1,0.25,0.5,0.75,1.,1.25,1.5,1.75,2.}; // changed on 8/25/
  VtempC={5.,10.,25.,100.,250.,500.};
  VtauC={0.,0.125,0.25,0.5,0.75,1.,2.};    
  
  VthetaC={ 5., 45., 85.};
  VrC ={25,50.,100.};

  WthetaC={ 5., 25., 45.};
  WrC ={2.5,10.,50.};
  WrC2={20,50.,100.};

  linspace(Vincl,5,85,9); // 1 digit
    
  std::cout <<"Initializing corona parameters."<<std::endl;
}
void Par::check(std::string str)
{
    bool flag=true;
    flag&=config(str)<(int)Va.size();
    flag&=stoi(str.substr(1,1))<(int)Vgeometry.size();
    flag&=stoi(str.substr(2,1))<(int)VLoM.size();
    flag&=stoi(str.substr(3,1))<(int)VtempC.size();
    flag&=stoi(str.substr(4,1))<(int)VtauC.size();
    flag&=stoi(str.substr(5,1))<(int)VthetaC.size();
    flag&=stoi(str.substr(6,1))<(int)VrC.size();
    if (!flag)
    {
        std::cout<<"Problem with ID string"<<std::endl;
        exit(-1);
    }
}


void Par::linspace(std::vector<double> &res, double x1, double x2, int n)
{
    res.resize(n);
    double delta =(x2-x1)/(double)(n-1);
    for (int i=0; i<n; ++i)
        res[i]=x1+(i*delta);
}

#endif
