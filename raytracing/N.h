#define NO_debugN

typedef std::vector<double> Metric;

#define D 4
typedef double Matrix[D][D];
typedef double Vector[D];

class N
{
  // define n in k vector frame and in disk coordinates
  Matrix Nk;
  Matrix ND;

  Disk *d_p;

public:
  void init(Disk *disk_p);
  void kFill(double pol); // fills Nk[i][j]
  void k2D(double amu,double phi); // transforms n from k frame to disk frame
  //  double myDisk(int i,int j) {return ND[i][j];}; // gives back n in disk coordinates
};

