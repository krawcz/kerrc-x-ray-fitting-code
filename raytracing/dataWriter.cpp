#include <iostream>
#include <string>

#include "xTrack.h"
#include "xTrackMath.h"
#include "metric.h"
#include "macro.h"
#include "kerr.h"

#include "dataWriter.h"

dataWriter::dataWriter(string name1,string name2)
{
  pathName=name1;
  coronaId=name2;
  std::cout <<"dataWriter opened:\n path: "<<name1<<endl;
  std::cout <<"id: "<<name2<<endl;
}

void dataWriter::init()
{
  //  rLaunch=eObs=iObs=qObs=uObs=thetaObs=tObs=0.;
  // rDisk=thetaEmitted=eEmitted=0.;
  // rDisk=ePf=iDisk=0.;
}

int dataWriter::thetaBin(float thetaObs)
{
  double x=fabs(cos(thetaObs));
  int i = (int)(x*9.);
  if (i>8) i=8;
  return i;
}

void dataWriter::printInfo()
{
  cout<<"th: ";
  long int sum=0;

  for (auto v : rTher)
    {
      cout <<v.size()<<" ";
      sum+=v.size();
    }

  cout<<"re: ";
  for (auto v : rRefl)
    {
      cout <<v.size()<<" ";
      sum+=v.size();
    }

  cout<<"di: ";
  cout <<vDisk.size()<<" sum "<<sum;
  cout <<endl;
}

int dataWriter::addTher(float rLaunch,float eObs,float iObs,float qObs, float uObs,float thetaObs,float tObs)
{
  array<float,nTher> x;
  x[0]=rLaunch;
  x[1]=eObs;
  x[2]=iObs;
  x[3]=qObs;
  x[4]=uObs;
  x[5]=tObs;

  int tBin=thetaBin(thetaObs);
  rTher[tBin].push_back(x);
  return (int)rTher[tBin].size();
}

int dataWriter::addRefl(float rLaunch,float eObs,float iObs,float qObs, float uObs,float thetaObs,float tObs,float rDisk,float thetaEmitted,float eEmitted)
{
  array<float,nRefl> x;
  x[0]=rLaunch;
  x[1]=eObs;
  x[2]=iObs;
  x[3]=qObs;
  x[4]=uObs;
  x[5]=tObs;
  x[6]=rDisk;
  x[7]=thetaEmitted;
  x[8]=eEmitted;

  int tBin=thetaBin(thetaObs);
  rRefl[tBin].push_back(x);
  return (int)rRefl[tBin].size();
}

int dataWriter::addDisk(float rLaunch,float rDisk,float ePf, float iDisk, float iDD,float thetaEmitted,float eEmitted,float eObs,float iObs)
{
  array<float,nDisk> x;
  x[0]=rLaunch;
  x[1]=rDisk;
  x[2]=ePf;
  x[3]=iDisk;
  x[4]=thetaEmitted;
  x[5]=eEmitted;
  x[6]=eObs;
  x[7]=iObs;
  x[8]=iDD;

  vDisk.push_back(x);
  return (int)vDisk.size();
}

int dataWriter::save()
{
  printInfo();
  int size=0;
  H5std_string eName = pathName+"e"+coronaId+".h5";
  H5std_string dName = pathName+"d"+coronaId+".h5";

  const int rank=2;
  hsize_t dims[rank];

  // open file for thermal and reflected emission
  H5::H5File eFile(eName,H5F_ACC_TRUNC);

  // make group!

  H5::Group eGroup( eFile.createGroup(coronaId ));
  
  // loop over all theta bins
  cout<<"Save thermal and reflected emission"<<endl;
  for (int j=0;j<nThet;j++)
    {
      // save thermal emission

      dims[0]=(hsize_t) rTher[j].size();
      dims[1]=(hsize_t) rTher[j][0].size();
      H5::DataSpace therDataspace(2,dims);

      H5::FloatType therDatatype(H5::PredType::NATIVE_FLOAT);
      therDatatype.setOrder(H5T_ORDER_LE);

      H5::DataSet therDataset;
      therDataset = eFile.createDataSet(coronaId+"t"+std::to_string(j), therDatatype, therDataspace);
      therDataset.write(rTher[j][0].data(),H5::PredType::NATIVE_FLOAT);
      
      // save reflected emission

      dims[0]=(hsize_t) rRefl[j].size();
      dims[1]=(hsize_t) rRefl[j][0].size();

      H5::DataSpace reflDataspace(2,dims);

      H5::FloatType reflDatatype(H5::PredType::NATIVE_FLOAT);
      reflDatatype.setOrder(H5T_ORDER_LE);

      H5::DataSet reflDataset;
      reflDataset = eFile.createDataSet(coronaId+"r"+std::to_string(j), reflDatatype, reflDataspace);
      reflDataset.write(rRefl[j][0].data(),H5::PredType::NATIVE_FLOAT);

      size+=rTher[j].size()+rRefl[j].size();
    }

  eFile.close();

  // save disk data

  cout <<"Saving disk data ..."<<endl;
  H5::H5File dFile(dName,H5F_ACC_TRUNC);

  // make group!

  H5::Group* dGroup = new H5::Group( dFile.createGroup(coronaId ));
  
  dims[0]=(hsize_t) vDisk.size();
  dims[1]=(hsize_t) vDisk[0].size();
  H5::DataSpace diskDataspace(rank,dims);

  H5::FloatType diskDatatype(H5::PredType::NATIVE_FLOAT);
  diskDatatype.setOrder(H5T_ORDER_LE);

  H5::DataSet diskDataset;
  diskDataset = dFile.createDataSet(coronaId+"d", diskDatatype, diskDataspace);
  diskDataset.write(vDisk[0].data(),H5::PredType::NATIVE_FLOAT);
  dFile.close();

  delete dGroup;

  cout <<"Done saving."<<endl;

  size+=vDisk.size();

  return size;
}
