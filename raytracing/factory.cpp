// C++
#include <cstdlib> 
#include <iostream>
#include <fstream>
#include <iomanip>   // format manipulation
#include <string>
#include <cmath>
#include <vector>
// C
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>    
using namespace std;

#include "xTrack.h"
#include "xTrackMath.h"
#include "macro.h"
#include "hist.h"

double getChi(double q, double u)
{
    double res = atan(u/q)/2.;
    if(q < 0.) res += PI/2.;
    else if (u < 0.) res += PI;
    return res;
}

int main(int argc, char** argv)
{
    
    /* histogram results */
    if (argc<=2)
    {
        /* Help */
        cout << "Compile with g++ --std=c++11 -lm -O3 factory.cpp -o factory"<<endl;
        cout << "Run     with ./factory parameterString outDir"<<endl;
        cout << "./factory 050020001 ~/data/simulations/"<<endl;
    }
    cout<<"Running Factory on "<<endl;
    system("echo $HOST");
    cout<<"Number of arguments "<<argc<<endl;
    
    //void plot(string fName, long int nBin=20000000)
    // arginput parameters
    // fName: string with parameter values, used for input and output file name
    // nBin: maximum events to be processed
    
    // now we start reading in the information
    long int nBin=20000000;
    cout <<endl<<"Reading up to "<<nBin<<" events."<<endl;
    
    PhotonRed *gammaRed_p = new PhotonRed;
    int size = sizeof(PhotonRed);
    
    cout <<"Reading data in chunks of "<<size<<" units."<<endl;
    int total=0;
    
    // we want to do the analysis for thetaBins different inclinations:
#define dTheta 5.
#define thetaBins 9
    vector<double> thetaCd;
    for (int i=0;i<thetaBins;i++)
        thetaCd.push_back(5.+i*10.);
    
    // define dtheta window for the arrival position
    
#define logE 40
#define twelve 12
    Hist *h[thetaBins][3];
    Hist *d[thetaBins][3];
    Hist *h12[twelve][thetaBins][3];
    Hist *d12[twelve][thetaBins][3];
    
    double l1=-1.;
    double l2=2.;
    
    for (int t=0;t<thetaBins;t++)
        for (int l=0;l<3;l++)
        {
            h[t][l] = new Hist(logE,l1,l2);
            d[t][l] = new Hist(logE,l1,l2);
	    for (int m=0;m<twelve;m++)
	      {
		h12[m][t][l] = new Hist(logE,l1,l2);
		d12[m][t][l] = new Hist(logE,l1,l2);
	      }
        }
    
    double x,y,z,kr,kt,kp,kx,ky,pol;
    
    string id=argv[1];
    string dir=argv[2];
    
    string inp=dir+id+".dat";
    string outFile=dir+id+".par";
    
    ifstream i_file(inp,ios::in | ios::binary);   //open file for writing
    if ( !i_file.is_open()) {cout << "couldn't open output file "<<inp << endl; exit (1);}
    cout <<endl<<endl<<">>> Opened "<<inp<<endl<<endl;
    cout <<endl<<endl<<">>> Output file "<<outFile<<endl<<endl;
    
    long int c12=0;
    while ((total++<nBin)&&(i_file.read((char*)gammaRed_p,size)))
    {
      if (gammaRed_p->uCS[0]<0.) continue;
        //print the detailed event information for the first 5 events
      if ((total<50)||(total%100000==0)) cout <<total<<" "<<" r0 "<<gammaRed_p->r0<<" T "<<gammaRed_p->T<<" E "<<gammaRed_p->E <<" nS "<<gammaRed_p->nScatter<<" nSC "<<gammaRed_p->nScatter_Corona <<" xBL "<<gammaRed_p->xBL[0]<<" "<<gammaRed_p->xBL[1]<<" "<<gammaRed_p->xBL[2]<<" "<<gammaRed_p->xBL[3]<<" "<<" uCS "<<gammaRed_p->uCS[0]<<" "<<gammaRed_p->uCS[1]<<" "<<gammaRed_p->uCS[2]<<" "<<gammaRed_p->uCS[3]<<" "<<endl;
        
        // we only simulated the emission into the upper hemisphere;
        // we should have simulated the emission into the lower hemisphere also
        // however, because of the symmetry of the accretion disk, we can
        // simply use the photons arriving below the accretion disk and
        // "flip them up". In this way, we get all the events that we would have
        // gotten, had we simulated the disk emission into the lower hemisphere.
        if (gammaRed_p->xBL[2]>PI/2.) {
            gammaRed_p->xBL[2] = PI/2. - (gammaRed_p->xBL[2]-PI/2.);
            gammaRed_p->uCS[2] = -gammaRed_p->uCS[2];
            gammaRed_p->stokes[2]=-gammaRed_p->stokes[2];
        }
        
        // transform from spherical coordinates of the final photon position into x-y-z coordinates
        x=gammaRed_p->xBL[1]*sin(gammaRed_p->xBL[2])*cos(gammaRed_p->xBL[3]);
        y=gammaRed_p->xBL[1]*sin(gammaRed_p->xBL[2])*sin(gammaRed_p->xBL[3]);
        z=gammaRed_p->xBL[1]*cos(gammaRed_p->xBL[2]);
        
        // get the wave vector of the photon in the coordinate stationary frame
        kr=gammaRed_p->uCS[1];
        kt=gammaRed_p->uCS[2];
        kp=gammaRed_p->uCS[3];
        
        // get the normalized wave vector components in x and y direction.
        kx=-gammaRed_p->uCS[3]/fabs(gammaRed_p->uCS[0]);
        ky= gammaRed_p->uCS[2]/fabs(gammaRed_p->uCS[0]);
        // get the polarization fraction if the photon was successfully tracked
        if (gammaRed_p->stokes[0]>0.)
            pol=sqrt( sqr(gammaRed_p->stokes[1])+sqr(gammaRed_p->stokes[2]) )/ gammaRed_p->stokes[0];
        else
            pol=0.;
        
        if ((gammaRed_p->xBL[1]>=10000.))
	  {
	    int c12c = (c12++)%twelve;
            for (int t=0;t<thetaBins;t++)
	      if ((gammaRed_p->xBL[2]>(thetaCd[t]-dTheta)/RtD)&&
		  (gammaRed_p->xBL[2]<(thetaCd[t]+dTheta)/RtD))
		for (int l=0;l<3;l++)
		  {
		    
		    if (gammaRed_p->nScatter==0)
		      {
			h[t][l]->fill(log10(gammaRed_p->uCS[0]),gammaRed_p->uCS[0]*gammaRed_p->stokes[l]);
			h[c12c][t][l]->fill(log10(gammaRed_p->uCS[0]),gammaRed_p->uCS[0]*gammaRed_p->stokes[l]);
		      }
		    else
		      {
			d[t][l]->fill(log10(gammaRed_p->uCS[0]),gammaRed_p->uCS[0]*gammaRed_p->stokes[l]);
			d12[c12c][t][l]->fill(log10(gammaRed_p->uCS[0]),gammaRed_p->uCS[0]*gammaRed_p->stokes[l]);
		      }		    
		  }
	  }
    }
    i_file.close();
    
    cout<<"Finished reading in events; total= "<<total<<endl;

    Hist *hh = new Hist(logE,l1,l2);
    Hist *dd = new Hist(logE,l1,l2);
    
    
    for (int t=0;t<thetaBins;t++)
        for (int l=0;l<3;l++)
        {
	  for (int k=0;k<logE;k++) // loop over all bins
	    {
	      double a=h[t][l]->get(k)/(double)twelve;
	      double d=d[t][l]->get(k)/(double)twelve;

	      double sumH=0.;
	      double sumD=0.;
	      for (int m=0;m<twelve;m++)
		{
		  sumH+=pow(a-h12[m][t][l]->get(k),2);
		  sumD+=pow(d-h12[m][t][l]->get(k),2);
		}
	      sumH = sqr(sumH/(double)(twelve-1)); // RMS = sum dev^2/(N*(N-1))
	      sumD = sqr(sumD/(double)(twelve-1)); // RMSoriginal = RMS*sqrt(N)
	      hh->set(k,sumH);
	      dd->set(k,sumD);
	    }

            string fullId=id+to_string(t)+to_string(l)+"00";
            h[t][l]->write(outFile,fullId);

            string fullId=id+to_string(t)+to_string(l)+"01";
            hh->write(outFile,fullId);

            fullId=id+to_string(t)+to_string(l)+"10";
            d[t][l]->write(outFile,fullId);

            fullId=id+to_string(t)+to_string(l)+"11";
            dd->write(outFile,fullId);

        }

    for (int t=0;t<thetaBins;t++)
    {
        // calculate polarization fraction and direction
        
        for (int k=0;k<logE;k++)
        {
            // pol fraction
            double i=h[t][0]->get(k);
            double q=h[t][1]->get(k);
            double u=h[t][2]->get(k);
            
            double res=0.;
            if (i>0.)
                res= sqrt(sqr(q)+sqr(u))/ i;
            hPi->set(k,res);
            
            // pol angle
            res=0.;
            if(q>0.)
                res=getChi(q,u)*180./PI;
            hDir->set(k,res);
        }
	/*        if (t==0)
        {
            hPi->print("pol fraction");
            hDir->print("pol angle");
        }
	*/
    }
    cout <<"Done Factory "<<id<<" "<<total<<" events"<<endl;
}





