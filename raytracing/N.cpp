#include <iostream>
#include <string>

#include "xTrack.h"
#include "xTrackMath.h"
#include "metric.h"
#include "macro.h"
#include "kerr.h"
#include "N.h"

/* void N::init(Disk *disk_p)
{
  d_p = disk_p;
}
*/

void N::kFill(double pol) // fills Nk[i][j]
{
  for (int i=0;i<D;i++)
    for (int j=0;j<D;j++)
      Nk[i][j]=0.;
  /*
  Nk[][] =1.;
  Nk[][] =1.;
  Nk[][] =pol;
  Nk[][] =pol;
  */
}

void N::k2D(double amu,double phi) // transforms n from k frame to disk frame
{
  /*
    perform 2 rotations on Nk to fill nD

    (1) Turn k=(1,1,0,0) by by 180-theta degree around 3 - axis (defined in disk frame  ==> k-F� -A
    (2) Turn k-F� by phi around 2 - positive r component becomes positive phi component,-A
    Positive phi component becomes a negative r-component
  */

#define Dspatial 3
//  double rot[Dspatial][Dspatial];

//  double sp = sin(phi);
//  double cp = cos(phi);

  /*
  // define rotation matrix #1

  ... here

  // rotate N by multiplying N by rot (look up tensor transformation law)

 // define rotation matrix #2

	... here; result goes into the variabl NDp

  // rotate NDp by multiplying NDp by rot (look up tensor transformation law)

	... here; result goes into the variabl ND
  */
}

