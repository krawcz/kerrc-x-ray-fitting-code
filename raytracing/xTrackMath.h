#ifndef XTRACK_XTRACKMATH_H_INCLUDED
#define XTRACK_XTRACKMATH_H_INCLUDED

#include <cmath>

template<typename T>
T sqr(T x) { return x * x; }

inline double cube(double x) { return x*x*x; }
inline int cube(int x) { return x*x*x; }

inline double cot(double x) { return (1./tan(x)); }

#endif // XTRACK_XTRACKMATH_H_INCLUDED
