#ifndef dataWriter_h
#define dataWriter_h

#include <iostream>
#include <string>
#include <array>
#include <vector>

#include "H5Cpp.h"

using namespace std;

#define nTher 6
#define nRefl 9
#define nDisk 9
#define nThet 9

class dataWriter {

  string coronaId;
  string pathName;

  // array of 9 vectors of 7 floats
  array<vector<array<float,nTher>>,nThet> rTher;

  // array of 9 vectors of 10 floats
  array<vector<array<float,nRefl>>,nThet> rRefl;

  // vector of 9 floats
  vector<array<float, nDisk>> vDisk;

  int thetaBin(float thetaObs);

public:
  dataWriter(string name1,string name2);
  void init();
  int addTher(float rLaunch,float eObs,float iObs,float qObs, float uObs,float thetaObs,float tObs);
  int addRefl(float rLaunch,float eObs,float iObs,float qObs, float uObs,float thetaObs,float tObs,float rDisk,float thetaEmitted,float eEmitted);
  int addDisk(float rLaunch,float rDisk,float ePf, float iDisk, float iDD,float thetaEmitted,float eEmitted,float eObs,float iObs);
  void printInfo();

  int save();
};
#endif
