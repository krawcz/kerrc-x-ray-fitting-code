double minusOne()
// produce random numbers following a dN/dE/E^-1 distribution
{
 double e1=0.1; // keV
 double e2=1000.; // keV
 double n0=1./log(e2/e1);

 double y=ran1();
 double x=exp(y/n0+log(e1));
 return x;
}
