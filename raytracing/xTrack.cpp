//  xTrackCode.cpp 

//  Authors:

//       Henric Krawczynski: Thermal emission from a Novikov/Thorne accretion 
//       disk with polarization given by Chandrasekhar 1960, ray tracing.
//       Janie K. Hoormann: Lamp-post corona to model Fe-K alpha line and Compton Hump (two prescriptions)
//       Banafsheh Beheshtipour: Extended corona models (wedge and sphere to be added here later) and QPO hot spot model
//       Quin Abarr: Improved methodology to solve the geodesic equation
//       Fabian Kislat: Accelerated computation of Christoffel symbols

//
//  Compile with "make"
//

//  Description:
//
// The real work is done in the function "trackPhoton", where the photon trajectories are
// calculated, and the photon parameters (time, position, energy, momentum, polarization vector)
// are updated, and photons are scattered if they collide with the accretion disk.
//
// Metrics / geometries:
//  0 - Kerr
//  1 - Johannsen and Psaltis (JP)
//  2 - Aliev and G. (AG)
//  3 - Glampedakis and Babak (GB)
//  4 - Pani et al (PMCC)


/* Here is what the most important functions do:
 
 loadConfig: loads the information about the parameters
 (black hole mass, accretion rate, black hole spin)
 and the radial disk emissivity.
 
 generateDiskPhoton(Disk *disk_p, int ir,Photon *res_p):
 emits a photon in a radial bin into a random direction, makign use of Chandrasekhar's
 results concenring the mean polarization and elevation-dependent intensity of the emission
 
 trackPhoton: does the real work of propagating the photon and scattering it.
 
 tab24, tab25: have Chandrasekhar's information about the initial polarization
 of photons emerging from the disk (tab24) and how scattering changes the polarization (tab25).
 
 */


// C++
#include <cstdlib> 
#include <iostream>
#include <fstream>
#include <iomanip>   // format manipulation
#include <string>
#include <cmath>
#include <vector>
#include <functional>
#include <tclap/CmdLine.h>

// C
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>    

using namespace std;

#include "xTrack.h"
#include "xTrackMath.h"
#include "metric.h"
#include "macro.h"

#include "aliev.h"
#include "glampedakis.h"
#include "kerr.h"
#include "pani.h"

#include "config.h"
#include "wind.h"
#include "corona.h"
#include "dataWriter.h"
#include "N.h"
#include "hist.h"

#define NPRINT 1000


static long seed = -90;
static int Pseed = -90;

static long int total=0;

static long int totalEh=0; // events falling into BH
static long int totalTo=0; // events with time out
static long int totalTh=0; // events without scattering
static long int totalRe=0; // events with scattering
static long int totalNaN=0; // events with scattering
static long int totalDi=0; // events with scattering
static double deltaR=20.;

#define MAXT 100000000

Wind wind;
Corona corona;
dataWriter *dw;
Config config;
N radTransport;

#define no_DEBUG
#define LOOK 0
#ifdef DEBUG

#define debugM(a) cout<<a<<endl;
#define debugV(a,b) cout<<a<<" "<<b<<endl;

#define EVENT
#define debugEventM(a) if (total==LOOK) cout<<a<<endl;
#define debugEventV(a,b) if (total==LOOK) cout<<a<<" "<<b<<endl;

#endif

#ifndef DEBUG

#define debugM(a)
#define debugV(a,b)
#define debugEventM(a)
#define debugEventV(a,b)
#define No_oldDataFormat

#endif


int main(int argc, char** argv)
{
    
    // Define arguments
    bool thermalDisk = false;
    bool chandra = false;
    bool lamppost_gf = false;
    bool lamppost_phenom = false;
    bool lamppost_wind=false;
    bool geometry_kerr = false;
    bool geometry_jp = false;
    bool geometry_ag = false;
    bool geometry_gb = false;
    bool geometry_pm = false;
    int geometry = 0;
    int spin = 0;
    int nBin = 0;
    double height = 0;
    double spectral_index = -1.7;
    string coronaID;
    string outFile;
    
    int frequency = 10000;

    cout<<"Running xTrackCode on "<<endl;
    cout<<system("echo $HOST")<<endl;
    
    //Interpret all the command line arguments
    try{
        using namespace TCLAP;
        
        CmdLine cmd("Ray-tracing simulation from black hole accretion disk", ' ', "0.1", true);
        
        //Specify which type of simulation
        SwitchArg diskSwitch("d", "disk", "Simulate thermal disk");
        // Lampposts
        SwitchArg gfSwitch("g", "george-fabian", "Lamppost with George & Fabian methodology");
        SwitchArg phenomSwitch("p", "phenomenolological", "Lamppost with phenomenological methodology");

        vector<Arg*> modeList = {&diskSwitch, &phenomSwitch, &gfSwitch};
        cmd.xorAdd(modeList);
        
        // Specify geometry to use
        SwitchArg kerrSwitch("K", "kerr", "Use Kerr black hole geometry");
        SwitchArg jpSwitch("J", "jp", "Use Johann-Psaltis black hole geometry");
        SwitchArg agSwitch("A", "aliev", "Use Aliev black hole geometry");
        SwitchArg gbSwitch("G", "glampedakis", "Use Glampedakis black hole geometry");
        SwitchArg pmSwitch("P", "pani", "Use Pani black hole geometry");
        vector<Arg*> geometryList = {&kerrSwitch, &jpSwitch, &agSwitch, &gbSwitch, &pmSwitch};
        cmd.xorAdd(geometryList);
                
        ValueArg<int> spinArg("a", "spin", "Black hole spin config", false, 0, "int", cmd);

        // Corona arguments
        ValueArg<string> coronaArg("c", "corona", "corona ID", false, "", "string", cmd);

        // Reflection options
        SwitchArg chandraSwitch("D", "chandra", "Chandrasekhar reflection off <D>isk");
        SwitchArg windSwitch("w", "wind", "Lamppost with Fe K-alpha reflection off wind");
        vector<Arg*> reflList = {&chandraSwitch, &windSwitch};
        cmd.add(chandraSwitch);
        cmd.add(windSwitch);
        
        
        // Formatting options
        ValueArg<int> printArg("F", "frequency", "After how many photons to print", false, 10000, "int", cmd);
        
        // Lamppost arguments
        ValueArg<double> heightArg("l", "height", "Lamppost height, required for lamppost simulation; default: 10.", false, 10., "double", cmd);
        ValueArg<double> spectral_indexArg("s", "spectral_index", "Spectral Index, required for lamppost simulation; default: -1.7.", false, -1.7, "double", cmd);
                
        // Required arguments for all simulations
        ValueArg<string> fileArg("f", "file", "Output file name", true, "out", "string", cmd);
        ValueArg<int> numberArg("n", "number", "Number of photons to simulate; number per bin for thermal disk, or total number for lamppost", true, 10, "int", cmd);
        ValueArg<long> seedArg("r", "random", "Random seed number", true, -90, "long", cmd);
        
        
        // Parse the argv array.
        cmd.parse( argc, argv );
        
        // Get the value parsed by each arg
        thermalDisk = diskSwitch.getValue();
        chandra = chandraSwitch.getValue();
        lamppost_gf = gfSwitch.getValue();
        lamppost_phenom = phenomSwitch.getValue();
        lamppost_wind = windSwitch.getValue();
        
        geometry_kerr = kerrSwitch.getValue();
        geometry_jp = jpSwitch.getValue();
        geometry_ag = agSwitch.getValue();
        geometry_gb = gbSwitch.getValue();
        geometry_pm = pmSwitch.getValue();
        
        spin = spinArg.getValue();
        nBin = numberArg.getValue();
        seed = seedArg.getValue();
        height = heightArg.getValue();
        spectral_index = spectral_indexArg.getValue();
        outFile = fileArg.getValue();
        coronaID=coronaArg.getValue();
        frequency = printArg.getValue();
        
    } catch (TCLAP::ArgException &e) {
        fprintf(stderr, "%s for argument %s", e.error().c_str(), e.argId().c_str());
        return 1;
    }
    
    config.setThermalDisk(thermalDisk);
    config.setChandraReflection(chandra);
    
    if (lamppost_gf)
    {
        config.setLampPost(1);
        cout << "Lamppost, George & Fabian methodology";
    }
    if (lamppost_phenom)
    {
        config.setLampPost(2);
        cout << "Lamppost, phenomenological methodology";
    }
    if (lamppost_wind)
    {
        config.setLampPost(3);
        cout << "Lamppost, scattering off wind";
    }
    
    /* load disk information */
    Disk *disk_p = new Disk;

    cout <<"coronaID "<<coronaID<<" "<<coronaID.length()<<endl;
    if (coronaID.length()>0)
    {
        corona.init(coronaID);
        spin=corona.getConfig();
    }

    loadConfig(spin,disk_p);

    if (corona.active()>0)
        corona.setR1(disk_p->r1);
    
    KerrMetric::Initialize(disk_p->hair);
    
    if( geometry_kerr ) geometry = 0;
    else if( geometry_jp ) geometry = 1;
    else if( geometry_ag ) {
        AlievMetric::Initialize(disk_p->hair);
        geometry = 2;
    }
    else if( geometry_gb ) {
        GlampedakisMetric::Initialize(disk_p->hair);
        geometry = 3;
    }
    else if( geometry_pm ) {
        PaniMetric::Initialize(disk_p->hair);
        geometry = 4;
    }
    
    // Save height if doing lamppost
    if( lamppost_gf || lamppost_phenom ) {
        if( height > 0 ) {
            disk_p->height = height;
            disk_p->spectral_index = spectral_index;
            cout<<endl<<"Lamppost: height "<<height<<endl;
            cout<<"Lamppost: spectral index "<<spectral_index<<endl;
        }
        else {
            cerr << "Lamppost height must be positive" << endl;
            return 1;
        }
    }
    
    char fname[100];
    sprintf(fname,"%s%s.dat",outFile.c_str(),coronaID.c_str());

    ofstream o_file (fname,ios::out | ios::binary);   //open file for writing
    if ( !o_file.is_open()) {
        cerr << "couldn't open output file" << fname<<" "<<endl;
        return 1;
    }
    else
      std::cout<<"Opened "<<fname<<endl;

    dw= new dataWriter(outFile,coronaID);
    
    switch (geometry) {
        case 0: cout << "The Geometry is Kerr" << endl; break;
        case 1: cout << "The Geometry is JP" << endl; break;
        case 2: cout << "The Geometry is AG" << endl; break;
        case 3: cout << "The Geometry is GB" << endl; break;
        case 4: cout << "The Geometry is PM" << endl; break;
    }
    
    cout << "Seed number: " << seed << endl;
    cout << "Spin parameter: " << disk_p->a << endl;
    cout << "Simulating " << nBin << " photons";
    
    if(config.getThermalDisk())
        cout << "for all " << DISK_STRUCTURE << " radial bins" << endl;
    else
        cout << " from the lamppost" << endl;
    printf("Saving results to %s\n\n", fname);
    
    cout << "ISCO: " << disk_p->ISCO << endl;
    cout << "r1: " << disk_p->r1 << endl;
    cout << "r2: " << disk_p->r2 << endl;
    cout << "rmax: " << disk_p->rmax << endl;
    
    cout << "==============================================" << endl << endl;
    
    if (lamppost_wind)
    {
        std::vector<double> parms(Cone::max,0.);
        
        /*
         parms[Cone::a]: black hole spin a
         parms[Cone::x1]: structure parameter start x1 = ISCO[a]
         parms[Cone::x2]: sturcture parameter end   x2 = 100.;
         parms[Cone::n]: power law index of cone n = 2;
         parms[Cone::x0]: scaling parameter for xc-extension x0 = 1.;
         parms[Cone::z0]: scaling parameter for zc-extension z0 = 10.;
         parms[Cone::gInf]: Lorentz factor at infinity, e.g. 20.;
         parms[Cone::sProb]: scattering probability, e.g. 0.01
         
         calculated paramaters:
         parms[Cone::xoff]: xoff
         parms[Cone::g0]: Lorentz factor at zc=0
         */
        
        cout <<"starting parms"<<endl;
        parms[Cone::a]=disk_p->a;
        
        // hhh
        
        parms[Cone::x1]=disk_p->ISCO; // 20.
        parms[Cone::x2]=disk_p->r2;
        parms[Cone::n]=2.;
        parms[Cone::x0]=1.;
        parms[Cone::z0]=10.;
        parms[Cone::gInf]=1.51186;
        parms[Cone::sProb]=1.0;//0.2;
        
        parms[Linear::alpha]=60.;
        
        cout <<"entering wind"<<endl;
        wind.init("linear",parms,disk_p);
        cout <<endl;
        
        std::cout<<"rc "<<wind.rc(15.)<<" tc "<<wind.tc(5.)<<std::endl;
        wind.load_g(wind.rc(15.),wind.tc(15.));
        wind.define_tetrad();
        wind.printTetrad();
        
    }
    
    Photon    *gamma_p    = new Photon;
    PhotonRed *gammaRed_p = new PhotonRed;
    int size = sizeof(PhotonRed);
    
    cout <<"Writing data in chunks of "<<size<<" units."<<endl;
    int flag;
    /*
    char track[100];
    sprintf(track, "track.dat");
    ofstream os(track);
    
    if( !os )
    {
        cout << "error opening" <<track<< endl;
        exit(-1);
    }
    */

    if(thermalDisk)
    {
        double ev=0.;
        double sc=0.;
        /* Loop over radial bins of the accretion disk */
        for (int ir=0; ir<disk_p->n_disk_structure; ir++)
        {
            // Comparison with Wenda's code:
            // corona.setR1(90.);
            // double r=binLog(disk_p->r1,disk_p->r2,disk_p->n_disk_structure,ir);
            // if (r>90.)
            // remember to change the corona definition to make it a complete wedge
            if (true)
            {
                
                /* In each radial bin, generate nBin photons */
                for (int ip=0; ip<nBin; ip++)
                {
                    initializePhoton(gamma_p);
                    generateDiskPhoton(disk_p,ir,gamma_p,-1.);
                    flag=-1;
                    
                    if (total<NPRINT)
                    {
                        flag=total;
                    }
                    
                    debugEventV("Entering Track Photon",total);

                    trackPhoton(disk_p,gamma_p,1E-5,1E-5,flag); /* includes scattering */

                    ev+=gamma_p->stokes[0];
                    if (gamma_p->nScatter_Corona>0) sc+=gamma_p->stokes[0];
                    
                    debugEventV("Exiting Track Photon",total);
                    /* save data to binary file */
                    saveData(o_file,disk_p,gamma_p, gammaRed_p, size, frequency);
                    if (++total>MAXT) break;
                }
                if (total>MAXT) break;
            }
        }
        cout<<"Fraction of events scattering "<<sc/ev<<endl;
    }
    
    if(config.getLampPost()>0)
    {
        double energy[EnergyBins];
        for (int i=0; i<EnergyBins; i++)
        {
            if (config.discreteEnergies())
                energy[i]=1+0.2*i;
            else
                energy[i]=1.;
        }
        
        for (int ip=0; ip<nBin; ip++)
        {
            // generates photon from the source looping through the list of potential energies given in the above vector
            initializePhoton(gamma_p);
            generateLampPostPhoton(gamma_p, disk_p, height, energy[(ip)%EnergyBins]);
            flag=-1;
            
            if (total<NPRINT) {
                flag=total;
                if (total%100==0)
                    cout <<"Writing track!"<<total<<endl;
            }
            
            trackPhoton(disk_p,gamma_p,1E-5,1E-5,flag); /* includes scattering */
            
            /* save data to binary file */
            
            saveData(o_file,disk_p, gamma_p, gammaRed_p, size, frequency);
            if (++total>MAXT) break;
        }
    }
    
    dw->save();
    o_file.close();
    //    os.close();
    cout <<"Done xTrackCode: "<<total<<endl;
}

/* custom functions */
void initializePhoton(Photon *gamma_p)
{
    gamma_p->r0 = 0;
    gamma_p->iron = false;
    gamma_p->T = 0;
    gamma_p->Ei = 0;
    gamma_p->E0 = 0;
    gamma_p->E = 0;
    gamma_p->L = 0;
    gamma_p->b = 0;
    gamma_p->weight = 0;
    gamma_p->timeout = 0;
    gamma_p->NuCS = 0;
    gamma_p->NfCS = 0;
    gamma_p->pol = 0;
    gamma_p->nScatter = 0;
    gamma_p->nScatter_Corona=0;
    gamma_p->tau=0.;

    gamma_p->rDisk=0.;
    gamma_p->thetaEmitted =0.;
    gamma_p->eEmitted=0.;
    gamma_p->ePf  =0.;
    gamma_p->diskI=0.;
    gamma_p->diskDI=0.;

    for(int i=0; i < 4; i++)
    {
        gamma_p->xBL[i] = 0;
        gamma_p->uBL[i] = 0;
        gamma_p->fBL[i] = 0;
        
        gamma_p->xSC[i] = 0;
        gamma_p->uSC[i] = 0;
        
        gamma_p->uCS[i] = 0;
        gamma_p->fCS[i] = 0;
        
        gamma_p->u0PF[i] = 0;
        gamma_p->u0BL[i] = 0;
    }
    
    for(int i=0; i < 3; i++)
        gamma_p->stokes[i]=0.;
    
    for(int i=0; i < IScat; i++)
        gamma_p->scatter[i] = 0;
}

void saveData(std::ofstream& o_file,Disk_p disk_p, Photon *gamma_p, PhotonRed *gammaRed_p, int size, int frequency)
{
    gammaRed_p->r0       = gamma_p->r0;
    gammaRed_p->T        = gamma_p->T;
    gammaRed_p->E0       = gamma_p->E0;
    gammaRed_p->E        = gamma_p->E;
    gammaRed_p->NuCS     = gamma_p->NuCS;
    gammaRed_p->NfCS     = gamma_p->NfCS;
    gammaRed_p->nScatter = gamma_p->nScatter;
    gammaRed_p->timeout  = gamma_p->timeout;
    gammaRed_p->Ei       = gamma_p->Ei;
    gammaRed_p->nScatter_Corona = gamma_p->nScatter_Corona;
    gammaRed_p->tau      =gamma_p->tau;
    
    gammaRed_p->iron     = gamma_p->iron;
    
    for (int i=0;i<3;i++) gammaRed_p->stokes[i]=gamma_p->stokes[i];
    
    for (int i=0;i<4;i++)
    {
        gammaRed_p->u0PF[i] = gamma_p->u0PF[i];
        gammaRed_p->xBL[i]  = gamma_p->xBL[i];
        gammaRed_p->uCS[i]  = gamma_p->uCS[i];
        gammaRed_p->fCS[i]  = gamma_p->fCS[i];
    }
    
    for(int i=0; i < IScat; i++)
        gammaRed_p->scatter[i] = gamma_p->scatter[i];

    if (gamma_p->nScatter==0)
      {
	if (gamma_p->xBL[1]>=disk_p->rmax-deltaR)
	  dw->addTher(gamma_p->r0,gamma_p->E,
		      gamma_p->stokes[0],gamma_p->stokes[1],gamma_p->stokes[2],
		      gamma_p->xBL[2],gamma_p->xBL[0]);
      }
    else
      {
	if (gamma_p->xBL[1]>=disk_p->rmax-deltaR)
	  dw->addRefl(gamma_p->r0,gamma_p->E,
		      gamma_p->stokes[0],gamma_p->stokes[1],gamma_p->stokes[2],
		      gamma_p->xBL[2],gamma_p->xBL[0],
		      gamma_p->rDisk,gamma_p->thetaEmitted,gamma_p->eEmitted);

	float ene=gamma_p->E;
	if (gamma_p->xBL[1]<disk_p->rmax-deltaR)
	  ene=-1.;

	dw->addDisk(gamma_p->r0,
		    gamma_p->rDisk,
		    gamma_p->ePf,
		    gamma_p->diskI,
		    gamma_p->diskDI,
		    gamma_p->thetaEmitted,
		    gamma_p->eEmitted,
		    ene,
		    gamma_p->stokes[0]);
      }

#ifdef oldDataFormat
    o_file.write ((char*)gammaRed_p,size);
#else
    if ((total<50)||(total%frequency==0))
      o_file.write ((char*)gammaRed_p,size);
#endif

    if ((total<50)||(total%frequency==0))
      {
        cout <<"#" <<total<<" r0 "<<gammaRed_p->r0<<" T "<<gammaRed_p->T<<" E "<<gammaRed_p->E
	     <<" sc "<<gammaRed_p->nScatter
	     <<" cSc "<<gamma_p->nScatter_Corona
	     <<" xBL "<<gammaRed_p->xBL[0]<<" "<<gammaRed_p->xBL[1]<<" "<<gammaRed_p->xBL[2]<<" "<<gammaRed_p->xBL[3]<<" "<<" uCS "<<gammaRed_p->NuCS<<" fCS "<<gammaRed_p->NfCS<<
	  " tau "<<gammaRed_p->tau<<" to "<<gammaRed_p->timeout<<" Nan "<<totalNaN<<" di "<<totalDi<<" To "<<totalTo<<" Eh "<<totalEh<<" Th "<<totalTh<<" Re "<<totalRe<<" ";
	dw->printInfo();
      }
    
}
void loadConfig(int nConfig, Disk *disk_p)
{
    char fname[50];
    int dummy;
    
    sprintf(fname,"input/d%d/horizon.dat",nConfig);
    load_table(disk_p->horizon,disk_p->n_horizon_mu,fname,DISK_HORIZON_MU);
    
    double in[10];
    sprintf(fname,"input/d%d/isco.dat",nConfig);
    load_table(in,dummy,fname,10);
    
    disk_p->M       =in[0];
    disk_p->M       =1.; // careful, this always needs to equal 1!
    disk_p->a       =in[1];
    
    disk_p->ISCO=in[3];
    disk_p->r1  =in[4];
    disk_p->r2  =in[5];
    disk_p->rmax=in[6];
    disk_p->epsilon3=0.;
    
    disk_p->n_disk_structure=in[7];
    disk_p->Mdot=in[8];
    cout <<"M "<<disk_p->M<<" a "<<disk_p->a<<" e3 "<<disk_p->epsilon3<<" Mdot "<<disk_p->Mdot<<endl;
    
    sprintf(fname,"input/d%d/Tlist.dat",nConfig);
    load_table(disk_p->r_T,dummy,fname,DISK_STRUCTURE);
    
    sprintf(fname,"input/d%d/Wlist.dat",nConfig);
    load_table(disk_p->r_weight,dummy,fname,DISK_STRUCTURE);
    
}

// Do lamppost model
void generateLampPostPhoton(Photon *gamma_p, Disk *disk_p, double h, double Ei)
{
    double y[4];
    gamma_p->T = gamma_p->Ei=Ei;
    gamma_p->r0=h; // this will become the height
    
    y[0]=0.;
    y[1]=gamma_p->r0;
    y[2]=PI/180.;   // now, instead of the radius being in the
    // accretion disk it is along the rotation axis
    // but slightly offset to avoid the singularities
    y[3]=0.;
    
    //    double mu= cos(y[2])-(cos(y[2])-cos(PI-y[2]))*ran1(&seed); // photon from
    // 0+ y[2]< theta < Pi-y[2]
    // projected onto the e_theta basis
    // mu = cos(theta)
    double mu = 1-2*ran1(&seed);
    
    //hhh
    //    if (total<10000)
    
    /*
     int myEvent=total;
     int it  = myEvent/1000;
     myEvent-= it*1000;
     int ih  = myEvent/100;
     myEvent-= ih*100;
     int iz  = myEvent;
     mu = -0.6-(double)it/100.; // go to 200,000
     */
    
    gamma_p->weight = 1;
    
    std::vector<double> metric(16, 0);
    CalculateMetricElements(disk_p->M, disk_p->a, disk_p->hair, y, metric);
    double g00=metric[make_metric_index(0, 0)];
    double g03=metric[make_metric_index(0, 3)];
    double g11=metric[make_metric_index(1, 1)];
    double g22=metric[make_metric_index(2, 2)];
    double g33=metric[make_metric_index(3, 3)];
    
    double sqrt_1_m_mu2 = sqrt(1.-mu*mu);
    double sqrt_mg00 = sqrt(-g00);
    double sqrt_g11 = sqrt(g11);
    double sqrt_g22 = sqrt(g22);
    
    gamma_p->uBL[0] = Ei/sqrt_mg00; // keeps track of redshift
    gamma_p->uBL[2] = Ei*sqrt_1_m_mu2/sqrt_g22; // e_r
    gamma_p->uBL[1] = Ei*mu/sqrt_g11; //e_theta
    gamma_p->uBL[3] = 0; //e_phi
    
    gamma_p->u0BL[0] = Ei/sqrt_mg00; // keeps track of redshift
    gamma_p->u0BL[2] = Ei*sqrt_1_m_mu2/sqrt_g22; // e_r
    gamma_p->u0BL[1] = Ei*mu/sqrt_g11; //e_theta
    gamma_p->u0BL[3] = 0; //e_phi
    
    gamma_p->xBL[0]=0.;
    gamma_p->xBL[1]=gamma_p->r0; // r
    gamma_p->xBL[2]=PI/180.; // theta
    gamma_p->xBL[3]=0.; // phi
    
    gamma_p->E = gamma_p->E0 = -1.*(g00*gamma_p->uBL[0]+g03*gamma_p->uBL[3]);
    gamma_p->L =     (g33*gamma_p->uBL[3]+g03*gamma_p->uBL[0]);
    gamma_p->b = gamma_p->L/gamma_p->E;
    
    
    // want emitted light to be unpolarized: f.f=1 and f.k=0
    
    gamma_p->fBL[0] = 1/sqrt_mg00;
    gamma_p->fBL[2] = sqrt_1_m_mu2/sqrt_g22;
    gamma_p->fBL[1] = mu/sqrt_g11;
    gamma_p->fBL[3] = 1/sqrt(g33);
    
    gamma_p->pol = 0;
    //gamma_p->nScatter = 0;
}

// generate thermal photon from accretion disk from Maxwell-Juttner Distribution
void generateDiskPhoton(Disk *disk_p, int ir,Photon *gamma_p,double Ei)
{
    double y[4];
    
    gamma_p->timeout=0;
    gamma_p->r0=binLog(disk_p->r1,disk_p->r2,disk_p->n_disk_structure,ir);
    gamma_p->T = disk_p->r_T[ir];
    
    double amu=ran1(&seed);
    // for test only
    // amu=1.;
    double phi=2. * PI * ran1(&seed);
    
    if ((corona.initialized()) && (Ei<0.))
    {
        double A =SI_h*J_eV/(1000*corona.getLoM()*gamma_p->T*corona.getHard());  // Temperature is in unit of keV, So I multiplied by 1000 to make in in unit of eV
        
        double freq=corona.planck_sample(A,2,Pseed);
        Ei = SI_h*J_eV*freq/1000; //keV  h*\nu \nu is from BB radiation

        debugEventV("Launching photon, Ei(keV)= ",Ei)
    }
    
    gamma_p->u0PF[0] =  Ei*1.;
    gamma_p->u0PF[1] =  Ei*sqrt(1.-amu*amu)*cos(phi); // r
    gamma_p->u0PF[2] =  Ei*(-amu); // theta ==> emission into the upper hemisphere!
    gamma_p->u0PF[3] =  Ei*sqrt(1.-amu*amu)*sin(phi); // phi
    
    double eimu[4][4];
    double emui[4][4];
    
    PF_eimu(disk_p->a,gamma_p->r0,eimu,emui); //BB  //transfrom BL to PF and reverse
    
    for (int mu=0;mu<4;mu++){
        gamma_p->u0BL[mu]=0.;
        for (int i=0; i<4; i++) gamma_p->u0BL[mu]+=emui[mu][i]*gamma_p->u0PF[i];
        gamma_p->uBL[mu]=gamma_p->u0BL[mu];
    }
    
    y[0]=0.;
    y[1]=gamma_p->r0;
    y[2]=PI/2.;
    y[3]=0.;
    
    debugEventM("generate Disk Photon gamma_p->u0BL ")
    debugEventV("u0BL ",gamma_p->u0BL[0])
    debugEventV("u0BL ",gamma_p->u0BL[1])
    debugEventV("u0BL ",gamma_p->u0BL[2])
    debugEventV("u0BL ",gamma_p->u0BL[3])
    
    gamma_p->xBL[0]=0.;
    gamma_p->xBL[1]=gamma_p->r0; // r
    gamma_p->xBL[2]=PI/2.; // theta
    gamma_p->xBL[3]=0.; // phi
    
    std::vector<double> metric(16, 0);
    CalculateMetricElements(disk_p->M, disk_p->a, disk_p->hair, y, metric);
    double g00=metric[make_metric_index(0, 0)];
    double g03=metric[make_metric_index(0, 3)];
    double g33=metric[make_metric_index(3, 3)];
    
    gamma_p->E = gamma_p->E0 = -1.*(g00*gamma_p->u0BL[0]+g03*gamma_p->u0BL[3]);
    gamma_p->L =     (g33*gamma_p->u0BL[3]+g03*gamma_p->u0BL[0]);
    gamma_p->b = gamma_p->L/gamma_p->E;
    
    
    double fPF[DIM];// f should point along the y-direction for phi=0.
    fPF[0]=0.;
    fPF[1]=-sin(phi); // e_r     == x
    fPF[2]=0.;        // e_theta == z
    fPF[3]=cos(phi);  // e_phi   == y
    
    for (int mu=0;mu<4;mu++){
        gamma_p->fBL[mu]=0.;
        for (int i=0; i<4; i++) gamma_p->fBL[mu]+=emui[mu][i]*fPF[i];
    }
    
    double weight, pol;
    tab24(amu,weight,pol);
    
    gamma_p->weight  =weight*disk_p->r_weight[ir];
    gamma_p->pol     =pol;
    
    debugEventV("Initialization E   ",gamma_p->E)
    debugEventV("Initialization b   ",gamma_p->b)
    debugEventV("Initialization pol ",gamma_p->pol)
}

// Create Fe K-alpha photon from wind
void generateWindPhoton(Photon *gamma_p, double Ei,double s)
{
    gamma_p->T = Ei;
    
    double amu=s*ran1(&seed);
    double phi=2. * PI * ran1(&seed);
    
    // hhh
    //    phi=0.;
    //amu=1.;
    /*
     int myEvent=total;
     int it  = myEvent/1000;
     myEvent-= it*1000;
     
     int ih  = myEvent/100;
     myEvent-= ih*100;
     int iz  = myEvent;
     
     amu = (double)ih/10.;
     phi = 2*PI/100.*(double)iz;
     */
    
    double sqrt_1_m_amu2 = sqrt(1.-amu*amu);
    
    gamma_p->u0PF[0] =  Ei;
    gamma_p->u0PF[1] =  Ei*sqrt_1_m_amu2*cos(phi); // r
    gamma_p->u0PF[2] = -Ei*amu;
    gamma_p->u0PF[3] =  Ei*sqrt_1_m_amu2*sin(phi); // phi
    
    wind.Tetrad2BL(gamma_p->u0PF,gamma_p->u0BL);
    
    //double norm=wind.square(gamma_p->u0BL);
    
    gamma_p->xBL[0]  = 0.;
    gamma_p->xBL[1]  = wind.BLr();
    gamma_p->xBL[2]  = wind.BLt();
    gamma_p->xBL[3]  = 0.;
    
    gamma_p->E = gamma_p->E0 = -1. * ( wind.g00() * gamma_p->u0BL[0] + wind.g03() * gamma_p->u0BL[3] );
    gamma_p->L = ( wind.g33() * gamma_p->u0BL[3] + wind.g03() * gamma_p->u0BL[0] );
    gamma_p->b = gamma_p->L / gamma_p->E;
    
    double fPF[DIM];// f should point along the y-direction for phi=0.
    fPF[0]=0.;
    fPF[1]=-sin(phi); // e_r     == x
    fPF[2]=0.;        // e_theta == z
    fPF[3]=cos(phi);  // e_phi   == y
    
    wind.Tetrad2BL(fPF,gamma_p->fBL);
    
    double weight, pol;
    tab24(amu,weight,pol);
    
    gamma_p->weight  =weight;
    gamma_p->pol     =pol;
    
    //    cout <<" BLr    "<<wind.BLr();
    // cout <<" BLt    "<<wind.BLt();
    // cout <<" Energy "<<gamma_p->E<<endl;
}

void trackPhoton(Disk *disk_p,Photon *gamma_p, double accuracy1, double accuracy2,int flag)
{
#define NUM_RK 10
    
    //  cout <<total<<endl;
    double y   [6][NUM_RK];
    double ydot[6][NUM_RK];
    
    //    double n[6][DIM][DIM];
    //    double ndot[6][DIM][DIM];
    
    double k   [6][NUM_RK];
    double yStar [NUM_RK]; // For CK error calculation
    double yPrev [NUM_RK]; // Save previous step in case we have to redo it
    double g00,g03,g11,g22,g33,den;
    double dist2=0.;
    double dist5=0.;
    
    double delta[NUM_RK], deltaStar[NUM_RK];
    
    double error = 0.;
    double errorIdeal = .00001;
    double safety = .95;
    
    double minStep = 0.000001;
    double maxStep = 300.;
    
    double u[4],uPF[4],fPF[4],vPF[4];
    
    double eta[4]={-1.,1.,1.,1.};
    bool NaN;
    int timeout=0;
    int fail=100000;
    std::vector<double> christoffel(64, 0);
    std::vector<double> metric(16, 0);

    double rmax1=disk_p->rmax-deltaR;
    double rmax2=disk_p->rmax+deltaR;
    double rh= disk_p->horizon[0];
    double rh2= disk_p->horizon[0]*1.02;
 
    //Cash-Karp Butcher tableau
    double b[5][5] = {
        {1./5., 0., 0., 0., 0.},
        {3./40., 9./40., 0., 0., 0.},
        {3./10., -9./10., 6./5., 0., 0.},
        {-11./54., 5./2., -70./27., 35./27., 0.},
        {1631./55296., 175./512., 575./13824., 44275./110592., 253./4096.}
    };
    
    //c, c* values from Butcher tableau
    double c[6] = {37./378., 0., 250./621., 125./594., 0., 512./1771.};
    double cStar[6] = {2825./27648., 0., 18575./48384., 13525./55296., 277./14336., 1./4.};
    
    double dh=0.001;
    
    bool toggle = false;
    bool sFlag  = false; // scatter flag
    bool wFlag  = false; // wind flag
    //    bool cEdge  = false; // corona edge flag
    
    y[0][0] =gamma_p->xBL[0]; // t
    y[0][1] =gamma_p->xBL[1]; // r
    y[0][2] =gamma_p->xBL[2]; // theta
    y[0][3] =gamma_p->xBL[3]; // phi
    
    y[0][4] =gamma_p->u0BL[1]/gamma_p->E; // d_r/d_lambda' = d_r/d_lambda * d_lambda/d_lambda' (with lambda=lambda'/E)
    y[0][5] =gamma_p->u0BL[2]/gamma_p->E; // d_theta/d_lambda'
    
    y[0][6] =gamma_p->fBL[0]; // fBL[0]
    y[0][7] =gamma_p->fBL[1]; // fBL[1]
    y[0][8] =gamma_p->fBL[2]; // fBL[2]
    y[0][9] =gamma_p->fBL[3]; // fBL[3]
    
    // Lindsey: fill n[0][i][j]
    //    for (int i=0;i<DIM;i++)
    //  for (int j=0;j<DIM;j++)
    //      n[0][i][j] = gamma_p->nBL[i][j];
    
    for (int i=0;i<4;i++)
    {
        gamma_p->xSC[i] = 0.;
        gamma_p->uSC[i] = 0.;
    }
    
    do
    {
        debugEventV("\n Tracking ",total)
        debugEventV("Timeout ",timeout)
        debugEventV("y1 ",y[0][1])
        debugEventV("y2 ",y[0][2])
        debugEventV("z ",y[0][1]*cos(y[0][2]))
	  debugEventV("Corona Height ",corona.height(y[0][1],y[0][2]))
        debugEventV("Integrated optical depth ",gamma_p->tau)
        debugEventV("Scattering ",gamma_p->nScatter)
        debugEventV("Corona Scattering ",gamma_p->nScatter_Corona)
        for(int i=0; i<NUM_RK; i++)
        {
            yStar[i] = y[0][i];
            yPrev[i] = y[0][i];
        }
        
        for (int iter=0; iter<6; iter++)
        {
            CalculateMetricElements3(disk_p->M, disk_p->a, disk_p->hair, y[iter], metric);
            g00 = metric[make_metric_index(0, 0)];
            g03 = metric[make_metric_index(0, 3)];
            g33 = metric[make_metric_index(3, 3)];
            
            // (1) compute change of coordinates and velocities
            
            den = g33*g00-sqr(g03);
            ydot[iter][0]=(-g33 - gamma_p->b*g03)/den; // dt/dlambda'
            ydot[iter][1]=y[iter][4]; // dr/dlambda'
            ydot[iter][2]=y[iter][5]; // dtheta/dlambda'
            ydot[iter][3]=(gamma_p->b*g00+g03)/den; // dphi/dlambda'
            
            CalculateChristoffelSymbols(disk_p->M, disk_p->a, disk_p->hair, y[iter], christoffel);
            
            ydot[iter][4]=
            -christoffel[make_christoffel_index(1, 0, 0)]    * sqr(ydot[iter][0])
            -christoffel[make_christoffel_index(1, 1, 1)]    * sqr(ydot[iter][1])
            -christoffel[make_christoffel_index(1, 2, 2)]    * sqr(ydot[iter][2])
            -christoffel[make_christoffel_index(1, 3, 3)]    * sqr(ydot[iter][3])
            -2.*christoffel[make_christoffel_index(1, 3, 0)]    * ydot[iter][3] * ydot[iter][0]
            -2.*christoffel[make_christoffel_index(1, 2, 1)]    * ydot[iter][2] * ydot[iter][1];
            
            ydot[iter][5]=
            -christoffel[make_christoffel_index(2, 0, 0)]    * sqr(ydot[iter][0])
            -christoffel[make_christoffel_index(2, 1, 1)]    * sqr(ydot[iter][1])
            -christoffel[make_christoffel_index(2, 2, 2)]    * sqr(ydot[iter][2])
            -christoffel[make_christoffel_index(2, 3, 3)]    * sqr(ydot[iter][3])
            -2.*christoffel[make_christoffel_index(2, 3, 0)]    * ydot[iter][3] * ydot[iter][0]
            -2.*christoffel[make_christoffel_index(2, 2, 1)]    * ydot[iter][2] * ydot[iter][1];
            
            // (2) compute change of polarization vector
            for (int i=0;i<4;i++)
            {
                
                ydot[iter][6+i] =0;
                
                for (int gamma=0;gamma<4;gamma++)
                    for (int beta=0;beta<4;beta++)
                    {
                        ydot[iter][6+i]-= christoffel[make_christoffel_index(i, gamma, beta)]*y[iter][6+gamma]*ydot[iter][beta];
                    }
            }
            
            for (int i=0; i<NUM_RK; i++)
            {
                k[iter][i] = dh * ydot[iter][i];
                if (iter <5)
                {
                    y[iter+1][i] = y[0][i];
                    for(int j=0;j<iter+1;j++)
                        y[iter+1][i] += b[iter][j] * k[j][i];
                }
            }
            
        }
        
        for (int j=0; j<NUM_RK; j++)
        {
            delta[j] = 0.;
            deltaStar[j] = 0.;
            for (int iter=0; iter<6; iter++)
            {
                delta[j] += k[iter][j]*c[iter];
                deltaStar[j] += k[iter][j]*cStar[iter];
		//		debugEventV("calc delta j ",j)
		//debugEventV("calc delta iter ",iter)
		//debugEventV("calc delta delta ",delta[j])
		//debugEventV("calc delta k ",k[iter][j])
		//debugEventV("calc delta c ",c[iter])

            }
        }
        
        //      if (isnan(delta[0]+delta[1]+delta[2]+delta[3])) {timeout =fail; break;}
        
        // the switch is toggled in case the beam hit the disk or wind or reaches the end of the corona in the previous step
        if (!toggle)
        {
            sFlag=false;
            wFlag=false;
	    //            cEdge=false;
            
            double correct=1.;
	    //            double correctStar=1.;
            
            double yPrelim[4]; // ,yPrelimStar[4];
            for (int i=0;i<4;i++)
            {
                yPrelim[i]=y[0][i]+delta[i];
		//                yPrelimStar[i]=y[0][i]+deltaStar[i];
            }
            // scatter off the disk?
            if ((y[0][1]>disk_p->ISCO)&&(y[0][1]<disk_p->r2)&&
                (
                 ((y[0][2]<PI/2.)&&(yPrelim[2]>PI/2.))||
                 ((y[0][2]>PI/2.)&&(yPrelim[2]<PI/2.))))
            {
                toggle=true;
                sFlag=true;
                correct = (PI/2.-y[0][2])/delta[2]/1.002;
		//                correctStar = (PI/2.-y[0][2])/deltaStar[2]/1.002;
                debugEventM("Hitting the disk")
            }
            
            // scatter off the wind?
            if (config.getLampPost()==3)
            {
                
                if (!wind.hit(y[0][1],y[0][2])&&(wind.hit(yPrelim[1],yPrelim[2])))
                {
                    toggle=true;
                    sFlag=false;
                    wFlag=(ran1(&seed)<wind.sProb());
                    if (wFlag)
                    {
                        correct=wind.correct(y[0][1],y[0][2],yPrelim[1],yPrelim[2])/1.002;
			//                        correctStar=wind.correct(y[0][1],y[0][2],yPrelimStar[1],yPrelimStar[2])/1.002;
                    }
                    debugEventM("Hitting the wind")
                }
            }
            debugEventM("Checking for corona tau")
            debugEventV("corona.active ",corona.active())
            debugEventV("corona.inCorona(y[0]) ",corona.inCorona(y[0]))
            debugEventV("y1 ",y[0][1])
            debugEventV("y2 ",y[0][2])

            debugEventV("corona.inCorona(yPrelim) ",corona.inCorona(yPrelim))
            debugEventV("yPrelim ",yPrelim[1])
            debugEventV("yPrelim ",yPrelim[2])
            debugEventV("delta ",delta[1])
            debugEventV("delta ",delta[2])

            // advance to the edge of the corona?
            if (corona.active())
            {
                if (corona.inCorona(y[0])||corona.inCorona(yPrelim))
                {
                    double correct2=1.;
		    //                    double correctStar2=1.;
                    
                    // are we crossing the edge of the corona?
                    if ((corona.inCorona(y[0])&&(!corona.inCorona(yPrelim)))||
                        (!corona.inCorona(y[0])&&(corona.inCorona(yPrelim))))
                    {
                        toggle=true;
			//                        cEdge=true;
                        correct=corona.correct(y[0][1],y[0][2],yPrelim[1],yPrelim[2])*1.002;
			//                        correctStar=corona.correct(y[0][1],y[0][2],yPrelimStar[1],yPrelimStar[2])*1.002;
                        debugEventV("Hitting the edge of the corona y1 ",y[0][1])
                        debugEventV("Hitting the edge of the corona y2 ",y[0][2])
                        debugEventV("Hitting the edge of the corona yp1 ",yPrelim[1])
                        debugEventV("Hitting the edge of the corona yp2 ",yPrelim[2])
                    }
                    
                    // how far are we allowed to move in the corona?
                    corona.loadEmunu(y[0],yPrelim);
                    double dtau=corona.dtau(y[0],yPrelim);
		    //                    double dtauStar=corona.dtau(y[0],yPrelimStar);
                    debugEventV("trackphoton dtau ",dtau)
                    debugEventV("trackphoton max.dtau ",corona.maxDtau())
                    
                    debugEventM("Checking for corona tau for real")
                    if (dtau>corona.maxDtau())
                    {
                        toggle=true;
                        correct2 = 0.8*corona.maxDtau()/dtau;
			//                        correctStar2 = 0.8*corona.maxDtau()/dtauStar;
                        debugEventV("Toggling corona step correction? ",dtau)

                        if (correct2<correct)
                        {
			  //                            cEdge=false;
                            correct=correct2;
			    //                            correctStar=correctStar2;
                            debugEventM("Did it.")
                        }
                    }
                    
                }
                else {
                    bool myFlag=(((y[0][2]<PI/2.)&&(yPrelim[2]>PI/2.))||
                                 ((y[0][2]>PI/2.)&&(yPrelim[2]<PI/2.)));
                    
                    if ((corona.inRegion(y[0])) || (corona.inRegion(yPrelim))
                        || (myFlag))
                    {
                        double travel=fabs(yPrelim[0]-y[0][0]);
                        double correct2     = 0.05*corona.height(y[0][1],y[0][2])/travel;
			//                        double correctStar2 = 0.05*corona.height(yPrelim[1],yPrelim[2])/travel;
                        if (correct2<correct)
                        {
                            toggle=true;
			    //                            cEdge=false;
                            correct=correct2;
			    //                            correctStar=correctStar2;
                            debugEventV("Adjusted with travel: ",correct2)
                            debugEventV("Adjusted step size: ",correct2*travel)
                    
                        }
                    }
                }
            }
            
            // corrects step size to hit observer at r=10,000 rg?
            if (y[0][1]+delta[1]>rmax2)
            {
                toggle=true;
                correct = (disk_p->rmax+1.-y[0][1])/delta[1];
		//                correctStar = (disk_p->rmax+1.-y[0][1])/deltaStar[1];
            }
            debugEventV("dh before correction ",dh)
            dh *= correct;
            debugEventV("dh after  correction ",dh)
            if (dh<minStep) dh=minStep;
        }
        else
            toggle=false;
            
        debugEventV("y1 ",y[0][1])
        debugEventV("y2 ",y[0][2])
        debugEventV("dh ",dh)
        debugEventV("d1 ",delta[1])
        debugEventV("d2 ",delta[2])
        debugEventV("toggle ",toggle)

        // if we toggled, we need to retake the step, thus, don't do anything here, and don't scatter neither.
        
        if (!toggle) // if toggle still equals false, do the step
            // otherwise fasst forward to the end of the do-loop and start fresh again
        {
            // let's do the step, maybe we have to undo it later
            for (int j=0; j<NUM_RK; j++)
            {
                y[0][j] += delta[j];
                yStar[j] += deltaStar[j];
            }
            
            //==================================Step size adaptation==============================//
            
            // location and velocity
            CalculateMetricElements(disk_p->M, disk_p->a, disk_p->hair, y[0], metric);
            g00 = metric[make_metric_index(0, 0)];
            g03 = metric[make_metric_index(0, 3)];
            g11 = metric[make_metric_index(1, 1)];
            g22 = metric[make_metric_index(2, 2)];
            g33 = metric[make_metric_index(3, 3)];
            
            den = g33*g00-sqr(g03);
            u[0]  = (-g33 - gamma_p->b*g03)/den; // dt/dlambda'
            u[1]  = y[0][4];
            u[2]  = y[0][5];
            u[3]  = (gamma_p->b*g00+g03)/den; // dphi/dlambda'
            dist2 = g00*sqr(u[0]) + g33*sqr(u[3]) + 2.*g03*u[0]*u[3] + g11*sqr(u[1]) + g22*sqr(u[2]); // velocities
            // f.f
            dist5 = g00*sqr(y[0][6+0])+ 2.*g03*y[0][6+0]*y[0][6+3]+
            g11* sqr(y[0][6+1]) + g22 * sqr(y[0][6+2]) + g33*sqr(y[0][6+3]);
            
            
            debugEventV(">y1 ",y[0][1])
            debugEventV(">y2 ",y[0][2])
            debugEventV("dist2 ",dist2)
            debugEventV("dist5 ",dist5)

            // Error is the difference between the regular and star Cash-Karp vectors
            error =  sqrt(y[0][1]*y[0][1]+yStar[1]*yStar[1]-2*y[0][1]*yStar[1]*(sin(y[0][2])*sin(yStar[2])*cos(y[0][3])*cos(yStar[3])+sin(y[0][2])*sin(yStar[2])*sin(y[0][3])*sin(yStar[3])+cos(y[0][2])*cos(yStar[2])));
            
            // Adjust is to change the ideal error if we're close to the BH or the error is already high
            double adjust = 1.0;
            
            // Set the acceptable error lower if we're close to the black hole
            if( y[0][1]<1.5*disk_p->ISCO) adjust /= 1000;
            
            // Also set the acceptable error if the error is already high
            if( fabs(dist2)>0.1 )
            {
                //printf("dist2=%f, error=%f, errorIdeal*adjust=%f\n",dist2,error,errorIdeal*adjust);
                adjust /= 10;
            }
            
            debugEventV(" adjust ",adjust)
            
            // Increase step size if the error is 0
	      if (error == 0 || std::isnan(error))
                dh = 3 * dh;
            else if (errorIdeal*adjust>=error)
                dh = safety * dh * pow(errorIdeal*adjust/error, 0.2);
            //If step size can get smaller, shrink it and retake step
            else if ((error>errorIdeal*adjust) && (dh>minStep))
            {
                for(int i=0; i<NUM_RK; i++)
                    y[0][i] = yPrev[i];
                
                debugEventM("Retaking the step")
                debugEventV("y[1]",y[0][1])
                debugEventV("y[1]",y[0][2])

                dh = safety * dh * pow(errorIdeal*adjust/error, 0.25);
                // Don't scatter if we're retaking the step
                toggle = false;
                sFlag  = false;
                wFlag  = false;
		//                cEdge  = false;
            }
            
            if (std::isnan(y[0][0]+y[0][1]+y[0][2]+y[0][3])) {NaN=true; break;}
            
            //Don't let the step size get too small or large
            if (dh<minStep) dh=minStep;
            if (dh>maxStep) dh=maxStep;
            
            //cout << "Step size adapted..." << endl;
            
            //Recalculate so that we don't fail for a large dist2 if we're taking the step again
            // location and velocity
            CalculateMetricElements(disk_p->M, disk_p->a, disk_p->hair, y[0], metric);
            g00 = metric[make_metric_index(0, 0)];
            g03 = metric[make_metric_index(0, 3)];
            g11 = metric[make_metric_index(1, 1)];
            g22 = metric[make_metric_index(2, 2)];
            g33 = metric[make_metric_index(3, 3)];
            
            den = g33*g00-sqr(g03);
            u[0]  = (-g33 - gamma_p->b*g03)/den; // dt/dlambda'
            u[1]  = y[0][4];
            u[2]  = y[0][5];
            u[3]  = (gamma_p->b*g00+g03)/den; // dphi/dlambda'
            dist2 = g00*sqr(u[0]) + g33*sqr(u[3]) + 2.*g03*u[0]*u[3] + g11*sqr(u[1]) + g22*sqr(u[2]); // velocities
            
            debugEventV(">> dist2 ",dist2)
            debugEventV(">> u0 ",u[0])
            debugEventV(">> u1 ",u[1])
            debugEventV(">> u2 ",u[2])
            debugEventV(">> u3 ",u[3])
            
	      /*
            // check if we are hitting the horizon.
            double mu    = cos(y[0][2]);
            int    index = DISK_HORIZON_MU-linBin(-1.,1.,DISK_HORIZON_MU,mu)-1.;
            rh           = disk_p->horizon[index];
	      */
            
            NaN=false;
            for (int i=0;i<10;i++)
	      if (std::isnan(y[0][i])) {NaN=true; break;}
            
            if (NaN)
            {
                if (y[0][1]>rh)
                {
                    cout <<"Attention: total "<<total;
                    cout <<" y ";
                    for (int i=0;i<10;i++)
                    {
                        cout <<" "<<y[0][i];
                        y[0][i]=0.;
                    }
                    cout <<" r_H "<<rh;
                    cout <<endl;
                }
            }
            else
            {
                // do we scatter in the corona?
                if (corona.active())
                    if (corona.inCorona(yPrev)||corona.inCorona(y[0]))
                    {
                        corona.loadEmunu(yPrev,y[0]);
                        double dtau=corona.dtau(yPrev,y[0]);
                        if (dtau>0.2) {
                            cout<<"Event "<<total<<" timeout "<<timeout<<" dtau: "<<dtau<<" maxTau "<<corona.maxDtau();
                            cout<<" yPrev "<<yPrev[1]<<" "<<yPrev[2]<<" y "<<y[0][1]<<" "<<y[0][2];
                            cout <<" height1 "<<corona.height(yPrev[1],yPrev[2])<<
                            " dt "<<y[0][0]-yPrev[0]<<endl;
//                            corona.debug(true);
                            dtau=corona.dtau(yPrev,y[0]);
//                            corona.debug(false);
                        }
                        double Pscat = (1.-exp(-dtau))*1.5;
                        double p = ran1(&seed);
                        gamma_p->tau+=dtau;
                        debugEventV("optical depth ",dtau)
			  debugEventV("Integrated optical depth ",gamma_p->tau)
			  debugEventV("Check optical depth ",y[0][1]*cos(y[0][2])/corona.height(y[0][1],y[0][2]));

                        
                        if (p<Pscat)
                        {
                            debugEventV("Corona Scatter, old energy ",gamma_p->E)
                            double f_corona[4];
                            for (int i=0; i<4; i++) {
                                f_corona[i]=y[0][6+i];
                            }
                                                  
                            double oldVal=u[0];
                            corona.loadEmunu(y[0]);
                            if (corona.scatter(total,gamma_p,u,f_corona,p/Pscat))
			      {
				debugEventV("g00 ",g00)
				  debugEventV("g03 ",g03)
				  debugEventV("u0 ",u[0])
				  debugEventV("u3 ",u[3])
				  gamma_p->E = -1.*(g00*u[0] + g03 * u[3]);
				  debugEventV("gamma_p->E ",gamma_p->E)
                                gamma_p->L =     (g33*u[3] + g03 * u[0]);
                                gamma_p->b = gamma_p->L/gamma_p->E;
                                
                                y[0][4]=u[1]/gamma_p->E;
                                y[0][5]=u[2]/gamma_p->E;
                                
                                for (int i=0; i<4; i++)
                                    y[0][6+i] = f_corona[i];
                                
                                double newVal=u[0];
                                double ratio=fabs(oldVal/newVal);
                                
                                if (ratio<0.1) ratio=0.1;
                                dh/=ratio;
                                debugEventV("New energy ",gamma_p->E)
                                gamma_p->nScatter_Corona++;
                                debugEventV("gamma_p->nScatter_Corona ",gamma_p->nScatter_Corona)
                            }
                        }
                    };
                
                if ((y[0][1]<disk_p->ISCO)||(y[0][1]>disk_p->r2)) sFlag=false;
                if ((config.getLampPost()==3)&&(wind.out(y[0][1],y[0][2]))) wFlag=false;
                
                if(((config.getChandraReflection())||(config.getIronReflection()))&&(sFlag)&&(timeout>1))
                {

                    debugEventV("Scattering off disk, r ",y[0][1])
                    debugEventV("Scattering off disk, z ",y[0][1]*cos(y[0][2]))
                    y[0][2]=PI/2;
                    
                    int ir=logBin(disk_p->r1,disk_p->r2,disk_p->n_disk_structure,y[0][1]);
                    
                    double eimu[4][4];
                    double emui[4][4];
                    PF_eimu(disk_p->a,y[0][1],eimu,emui); //BB  //transfrom BL to PF and reverse
                    
                    for (int i=0;i<4;i++)
                    {
                        uPF[i]=0.;
                        fPF[i]=0;
                        for (int mu=0; mu<4; mu++)
                        {
                            uPF[i] += eimu[i][mu]*(u[mu]*gamma_p->E);
                            fPF[i] += eimu[i][mu]*y[0][6+mu];
                        }
                    }
                    
                    gamma_p->scatter[0]=uPF[0];
                    gamma_p->scatter[1]=uPF[1];
                    gamma_p->scatter[2]=uPF[2];
                    gamma_p->scatter[3]=uPF[3];
                    gamma_p->scatter[4]=y[0][1];
                    gamma_p->scatter[5]=PI/2.;
                    
                    
                    double theta0 =180 - acos(-1*uPF[2]/uPF[0])*180/PI;
                    double num = ran1(&seed);
                    
                    
                    double yield = effectiveYield(theta0);
                    double albedo = 1;
                    if(uPF[0]<15) albedo = comptonLow(uPF[0],theta0);
                    if(uPF[0]>=15) albedo = comptonHigh(uPF[0],theta0);
                    double Es = uPF[0];
                    double rat = yield/(albedo);
                    gamma_p->scatter[11] = theta0;
                    
                    // George and Fabian prescription
                    // if(uPF[0] > 7.1 && num < yield) for stellar mass black holes
                    
                    if ((config.getLampPost() == 1 &&  uPF[0] > 7.1 && num < rat)
                        ||(config.getLampPost() == 2 && uPF[0] > 7.1 && num < 0.9 && gamma_p->nScatter == 1))
                    {
                        Es = 6.4;
                        gamma_p->iron = true;
                        generateDiskPhoton(disk_p,ir,gamma_p,Es);
                        gamma_p->scatter[10]=1;
                    }
                    else if (config.getLampPost() == 3)
                    {
                        gamma_p->iron = true;
                        gamma_p->weight *= pow(uPF[0],-(disk_p->spectral_index+1.));
                        generateDiskPhoton(disk_p,ir,gamma_p,1.);
                        gamma_p->scatter[10]=1;
                    }
                    else if (config.getChandraReflection())
                    {
                        // Scatter photon back into the upper hemisphere:
                        // get PF k vector of scattered photon
                        
                        double amu=ran1(&seed);
                        double phi=2. * PI * ran1(&seed);
                        double sqrt_1_m_amu2 = sqrt(1.-amu*amu);


                        // the photon is emitted into the upper hemisphere; phi starts at 0 at e_r, and is positive towards e_phi.
                        
                        vPF[0] =gamma_p->u0PF[0]=  Es ;
                        vPF[1] =gamma_p->u0PF[1]=  Es* sqrt_1_m_amu2*cos(phi); // r ; phi=0 ==> the phi of the direction is 0.
                        vPF[2] =gamma_p->u0PF[2]=  Es* (-amu); // theta ==> emission into the upper hemisphere!
                        vPF[3] =gamma_p->u0PF[3]=  Es* sqrt_1_m_amu2*sin(phi); // phi
                        
                        double nvPF[3];
                        normalize(&vPF[1],nvPF);
                        
                        // get parameters for Chandrasekhar's scattering equation (p. 260)
                        
                        double kV[3];
                        normalize(&uPF[1],kV);
                        
                        double ethetaN[3]={0.,-1.,0.};
                        double etheta_perp[3];
                        perp(ethetaN,kV,etheta_perp); // changed...
                        
                        double f[3];
                        normalize(&fPF[1],f);
                        
                        double f_perp[3];
                        perp(f,kV,f_perp);
                        
                        double help[3];
                        cross(ethetaN,kV,help);
                        int sign=(dot(help,f_perp)>0);
                        
                        // this gives only positive u's!
                        double chi= angle(etheta_perp,f_perp);
                        
                        double mu0=fabs(kV[1]);
                        
                        double kVn[3];
                        kVn[0]=-kV[0];
                        kVn[1]=-kV[1];
                        kVn[2]=-kV[2];
                        
                        double phi0;
                        phi0 = atan2(kVn[2],kVn[0]); // phi=0 for kVN=e_r, and PI/2. for kVN=e_phi
                        if (phi0<0.) phi0+=2.*PI;
                        
                        double stokes_i = 1.;
                        double stokes_q = gamma_p->pol * cos(2.*chi);
                        double stokes_u = -sign*gamma_p->pol * sin(2.*chi); // the minus is required because Chandra defined U seen from the 'far end' of the incoming rays
                        
                        double chandra[3];
                        
                        chandra[0] = (stokes_i+stokes_q)/2.;
                        chandra[1] = (stokes_i-stokes_q)/2.;
                        chandra[2] = stokes_u;
                        
                        double I[3];
                        tab25(chandra,mu0,phi0,I,amu,phi);
                        
                        stokes_i=I[0]+I[1];
                        stokes_q=I[0]-I[1];
                        stokes_u=I[2];
                        
                        gamma_p->pol = sqrt(sqr(stokes_q)+sqr(stokes_u))/stokes_i;

                        // use I to get "f after scattering"
                        chi=getChi(stokes_q,stokes_u);
                        double e_l[3],e_r[3];
                        
                        perp(ethetaN,nvPF,e_l);
                        cross(e_l,nvPF,e_r);
                        
                        gamma_p->fBL[0]=0.;
                        for(int i=0;i<3;i++)
                            gamma_p->fBL[i+1]=cos(chi)*e_l[i]+sin(chi)*e_r[i];
                        
                        for (int mu=0;mu<4;mu++)
                        {
                            u[mu]=gamma_p->u0BL[mu]=0.;
                            for (int i=0; i<4; i++)
                                u[mu]=gamma_p->u0BL[mu] += emui[mu][i]*vPF[i];
                        }
                        gamma_p->E = -1.*(g00*u[0] + g03 * u[3]);
                        gamma_p->L =     (g33*u[3] + g03 * u[0]);
                        gamma_p->b = gamma_p->L/gamma_p->E;
                        gamma_p->scatter[10]=-1;

			if (gamma_p->nScatter==0)
			  {
			    gamma_p->rDisk=y[0][1];
			    gamma_p->ePf  =uPF[0];
			    gamma_p->diskI=gamma_p->weight;
			    gamma_p->diskDI=stokes_i;
			    gamma_p->thetaEmitted =acos(amu);
			    gamma_p->eEmitted=u[0];
			  }
                        gamma_p->weight *= stokes_i;
                    }
                    
                    y[0][4] =gamma_p->u0BL[1]/gamma_p->E; // d_r/d_lambda' = d_r/d_lambda * d_lambda/d_lambda' (with lambda=lambda'/E)
                    y[0][5] =gamma_p->u0BL[2]/gamma_p->E; // d_theta/d_lambda'
                    y[0][6] =gamma_p->fBL[0];
                    y[0][7] =gamma_p->fBL[1];
                    y[0][8] =gamma_p->fBL[2];
                    y[0][9] =gamma_p->fBL[3];
                    
                    gamma_p->nScatter++;
                }
                
                if ((wFlag)&& timeout>1)
                {
                    // Produce Fe K-alpha photon
                    
                    gamma_p->iron = true;
                    
                    double r=y[0][1];
                    double t=y[0][2];
                                        
                    // move photon exactly onto cone.
                    double xc=wind.xc(r,t);
                    r=y[0][1]=wind.rc(xc);
                    
                    double s = (ran1(&seed)<0.5) ? -1. : 1.;
                    s=1.;
                    t=y[0][2]=wind.tc(xc)-s*0.0001;
                                        
                    wind.load_g(r,t);
                    wind.define_tetrad();
                    //		    wind.printTetrad();
                                        
                    // get u and f in wind frame
                    Vector ud,fd,udPF,fdPF;
                    
                    for (int i=0;i<DIM;i++)
                    {
                        ud[i]=u[i]*gamma_p->E;
                        fd[i]=y[0][6+i];
                    }
                    
                    //		    cout <<"Energy before scattering "<<total<<" "<<gamma_p->E<<endl;
                    wind.BL2Tetrad(ud,udPF);
                    wind.BL2Tetrad(fd,fdPF);
                    
                    gamma_p->scatter[0]=udPF[0];
                    gamma_p->scatter[1]=udPF[1];
                    gamma_p->scatter[2]=udPF[2];
                    gamma_p->scatter[3]=udPF[3];
                    gamma_p->scatter[4]=r;
                    gamma_p->scatter[5]=t;
                    gamma_p->weight *= pow(udPF[0],-(disk_p->spectral_index+1.));
                    
                    generateWindPhoton(gamma_p, 1.,s);
                    
                    // cout <<"Energy after scattering "<<total<<" "<<gamma_p->E<<endl;
                    gamma_p->scatter[6]=gamma_p->u0PF[0];
                    gamma_p->scatter[7]=gamma_p->u0PF[1];
                    gamma_p->scatter[8]=gamma_p->u0PF[2];
                    gamma_p->scatter[9]=gamma_p->u0PF[3];
                    gamma_p->scatter[10]=1.;
                    gamma_p->nScatter++;
                    
                    y[0][4] =gamma_p->u0BL[1]/gamma_p->E; // d_r/d_lambda' = d_r/d_lambda * d_lambda/d_lambda' (with lambda=lambda'/E)
                    y[0][5] =gamma_p->u0BL[2]/gamma_p->E; // d_theta/d_lambda'
                    y[0][6] =gamma_p->fBL[0];
                    y[0][7] =gamma_p->fBL[1];
                    y[0][8] =gamma_p->fBL[2];
                    y[0][9] =gamma_p->fBL[3];
                    
                    
                    CalculateMetricElements(disk_p->M, disk_p->a, disk_p->hair, y[0], metric);
                    g00 = metric[make_metric_index(0, 0)];
                    g03 = metric[make_metric_index(0, 3)];
                    g11 = metric[make_metric_index(1, 1)];
                    g22 = metric[make_metric_index(2, 2)];
                    g33 = metric[make_metric_index(3, 3)];
                    
                    den = g33*g00-sqr(g03);
                    u[0]  = (-g33 - gamma_p->b*g03)/den; // dt/dlambda'
                    u[1]  = y[0][4];
                    u[2]  = y[0][5];
                    u[3]  = (gamma_p->b*g00+g03)/den; // dphi/dlambda'
                    
                    dh=0.001;
                }
                /* ----------------- end runMode = 3 Fe K-alpha ----------------- */
                
		/*
                // pipe results to file
                if((flag>=0)&&(timeout<fail))
                {
                    int myEvent=total;
                    int it  = myEvent/1000;
                    myEvent-= it*1000;
                    
                    int ih  = myEvent/100;
                    myEvent-= ih*100;
                    int iz  = myEvent;
                    
                    double einf = -gamma_p->E*(u[0]*g00+u[3]*g03);
                    os <<total<<" "<<it<<" "<<ih<<" "<<iz<<" "<<y[0][0]<<" "<<y[0][1]<<" "<<y[0][2]<<" "<<y[0][3]<<" "<<
                    u[0]<<" "<<u[1]<<" "<<u[2]<<" "<<u[3]<<" "<<einf<<endl;
                };
		*/
            }
        }
    } while ( (fabs(dist2)<.5) &&  (y[0][1]>rh2) && (y[0][1]<rmax1) && (!NaN) && (timeout++ < fail));


    //    cout <<">> "<<total<<" "<<totalTo+totalEh+totalNaN+totalDi+totalTh+totalRe<<endl;
    if (timeout >=fail)
      {
	totalTo++;
	if ((totalTo<100)||(totalTo & 4096))
	  std::cout<<"# "<<total<<" to "<<timeout<<" y0 "<<y[0][0]<<" y1 "<<y[0][1]<<" y2 "<<y[0][2]<<" cSc "<<gamma_p->nScatter_Corona<<std::endl;
      }
    else if (y[0][1]<=rh2)
      totalEh++;
    else if (NaN)
      totalNaN++;
    else if (fabs(dist2)>=.5)
      totalDi++;
    else if (gamma_p->nScatter==0)
      totalTh++;
    else
      totalRe++;
    
    if (timeout < fail)
    {
      for (int i=0;i<4;i++) u[i] *= gamma_p->E;
        
      gamma_p->uBL[0] = u[0]; // d_theta/d_lambda
      gamma_p->uBL[1] = u[1]; // d_r/d_lambda
      gamma_p->uBL[2] = u[2]; // d_theta/d_lambda
      gamma_p->uBL[3] = u[3]; // d_phi/d_lambda
        
        gamma_p->fBL[0]=y[0][6];
        gamma_p->fBL[1]=y[0][7];
        gamma_p->fBL[2]=y[0][8];
        gamma_p->fBL[3]=y[0][9];
        
        if(y[0][1]>=disk_p->rmax)
        {
            // transform into coordinate-stationary frame.
            double emui[4][4];
            double eimu[4][4];
            
            CalculateMetricElements(disk_p->M, disk_p->a, disk_p->hair, y[0], metric);
            CS_emui(metric, emui);
            migs(emui,4,eimu);
            
            double uCS[4];
            double fCS[4];
            
            for (int i=0;i<4;i++){
                uCS[i]=0.;
                fCS[i]=0;
                for (int mu=0; mu<4; mu++) {
                    uCS[i] += eimu[i][mu]*u[mu];
                    fCS[i] += eimu[i][mu]*y[0][6+mu];
                }
                gamma_p->uCS[i] = uCS[i];
                gamma_p->fCS[i] = fCS[i];
            }
            
            
            double g00=metric[make_metric_index(0, 0)];
            double g03=metric[make_metric_index(0, 3)];
            double g11=metric[make_metric_index(1, 1)];
            double g22=metric[make_metric_index(2, 2)];
            double g33=metric[make_metric_index(3, 3)];
            
            dist2 = g00*sqr(u[0]) + g33*sqr(u[3]) + 2.*g03*u[0]*u[3] + g11*sqr(u[1]) + g22*sqr(u[2]); // velocities
            dist5 = g00*sqr(y[0][6]) + 2.*g03*y[0][6]*y[0][9] + g11*sqr(y[0][7]) + g22 * sqr(y[0][8]) + g33*sqr(y[0][9]); // f.f
            
            gamma_p->NuCS=0.;
            for (int i=0; i<4; i++)
                gamma_p->NuCS += eta[i]*sqr(uCS[i]);
            
            gamma_p->NfCS=0.;
            for (int i=0; i<4; i++)
                gamma_p->NfCS += eta[i]*sqr(fCS[i]);
            
            // get Stokes parameters in this frame.
            
            double kVV[3];
            normalize(&uCS[1],kVV);
            
            double ethetaN[3]={0.,-1.,0.};
            double etheta_perp[3];
            perp(ethetaN,kVV,etheta_perp);
            
            double f[3];
            normalize(&fCS[1],f);
            
            double f_perp[3];
            perp(f,kVV,f_perp);
            
            double help[3];
            cross(ethetaN,kVV,help);
            int sign=(dot(help,f_perp)>0);
            
            double chi= angle(etheta_perp,f_perp);
            
            gamma_p->stokes[0] = gamma_p->weight;
            gamma_p->stokes[1] = gamma_p->weight * gamma_p->pol * cos(2.*chi);
            gamma_p->stokes[2] = sign * gamma_p->weight * gamma_p->pol * sin(2.*chi);

	    if (y[0][1]>disk_p->rmax)
	      {
		double cosine=uCS[1]/sqrt(uCS[1]*uCS[1]+uCS[2]*uCS[2]+uCS[3]*uCS[3]);
	      
		y[0][0] -= (y[0][1]-disk_p->rmax)/cosine;
		y[0][1] = disk_p->rmax;
	      }

	    gamma_p->xBL[0]=y[0][0]; // t
	    gamma_p->xBL[1]=y[0][1]; // r
	    gamma_p->xBL[2]=y[0][2]; // theta
	    gamma_p->xBL[3]=y[0][3]; // phi
        
        }
        else {
            for (int i=0; i<4; i++){
                gamma_p->uCS[i] = 0.;
                gamma_p->fCS[i] = 0.;
            }
            gamma_p->NuCS=dist2;
            gamma_p->NfCS=dist5;
            gamma_p->stokes[0] = gamma_p->weight;
            gamma_p->stokes[1] = 0.;
            gamma_p->stokes[2] = 0.;
        }
    }
    
    gamma_p->timeout=timeout;
}
/* Physics functions */

void tab24(double mu, double &Is, double &pol)
/* from Chandrasekhar "Radiative Transfer", p. 248 */
{
#define Ntab24 21
    
    static double t_mu [Ntab24] = {0.,0.05,0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5,0.55,0.6,0.65,0.7,0.75,0.8,0.85,0.9,0.95,1.};
    static double Il_v [Ntab24] = {0.18294,0.21613,0.24247,0.26702,0.29057,0.31350,0.33599,0.35817,0.38010,0.40184,
        0.42343,0.44489,0.46624,0.48750,0.50869,0.52981,0.55087,0.57189,0.59286,0.61379,0.63469};
    static double Ir_v [Ntab24] = {0.23147,0.25877,0.28150,0.30299,0.32381,0.34420,0.36429,0.38417,0.40388,0.42346,
        0.44294,0.46233,0.48165,0.50092,0.52013,0.53930,0.55844,0.57754,0.59661,0.61566,0.63469};
    
    if (mu<0.) mu=0.;
    if (mu>1.) mu=1.;
    
    double Ir  = get_value_lin(t_mu,Ir_v,Ntab24,mu);
    double Il  = get_value_lin(t_mu,Il_v,Ntab24,mu);
    Is  = mu*(Ir+Il);
    pol = (Ir-Il)/(Ir+Il);
}

void tab25(double *F, double mu0, double phi0, double *I, double mu, double phi)
/* from Chandrasekhar "Radiative Transfer", p. 259 */
// be careful:
// the number of photons is propotional to the energy flux (in PF)
// we want to multiply a photon's weight with a factor which averages out to 1 when we average over all scattering processes (actually we could check that we get this right)
//
// important:
// let's call incoming flux "F"; Chandrasekhar's "F" is F/PI;
// if we want to have unit energy flux per unit area of the disk coming in, we have to feed the program with F/mu.
// outgoing is I, we have to multiply with mu to get the outgoing <flux per area>. 
//
// STILL TO DO: CHECK THET U ACQUIRES THE RIGHT SIGN.
//
{
#define Ntab25 21
    static double t_mu [Ntab25] = {0.,0.05,0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5,0.55,0.6,0.65,0.7,0.75,0.8,0.85,0.9,0.95,1.};
    
    static double Tpsi  [Ntab25] = {0.00000,0.04075,0.09144,0.15105,0.21916,0.29557,0.38014,0.47276,0.57338,0.68195,0.79843,0.92277,1.05497,1.19501,1.34286,1.49852,1.66198,1.83321,2.01223,2.19902,2.39357};
    static double Tphi  [Ntab25] = {1.00000,1.12988,1.20976,1.26850,1.31108,1.33973,1.35569,1.35971,1.35228,1.33374,1.30437,1.26432,1.21375,1.15279,1.08153,1.00003,0.90836,0.80655,0.69468,0.57276,0.44083};
    static double Tchi  [Ntab25] = {1.00000,1.10352,1.18638,1.26329,1.33687,1.40821,1.47801,1.54664,1.61435,1.68132,1.74772,1.81362,1.87911,1.94425,2.00907,2.07365,2.13799,2.20213,2.26609,2.32990,2.39356};
    static double Tceta [Ntab25] = {0.00000,0.01824,0.03764,0.05780,0.07852,0.09969,0.12121,0.14303,0.16510,0.18738,0.20984,0.23247,0.25523,0.27812,0.30112,0.32421,0.34739,0.37065,0.39398,0.41738,0.44083};
    static double Th1mu [Ntab25] = {1.00000,1.07167,1.11602,1.14837,1.17155,1.18685,1.19487,1.19599,1.19030,1.17774,1.15816,1.13118,1.09624,1.05256,0.99899,0.93381,0.85435,0.75611,0.63033,0.45471,0.00000};
    static double Th2   [Ntab25] = {1.00000,1.04967,1.08621,1.11762,1.14552,1.17075,1.19383,1.21508,1.23476,1.25308,1.27019,1.28624,1.30132,1.31554,1.32895,1.34166,1.35371,1.36515,1.37601,1.38638,1.39625};
    
    mu= fabs(mu);
    if (mu<0.) mu=0.;
    if (mu>1.) mu=1.;
    
    double S[3][3];
    double m1[3][3],m2[3][3],m3[3][3],m4[3][3];
    double Q[3][3]={{1.,0.,0.},{0.,1.,0.},{0.,0.,2.}};
    
    double psiMu   = get_value_lin(t_mu,Tpsi,Ntab25,mu);
    double psiMu0  = get_value_lin(t_mu,Tpsi,Ntab25,mu0);
    
    double phiMu   = get_value_lin(t_mu,Tphi,Ntab25,mu);
    double phiMu0  = get_value_lin(t_mu,Tphi,Ntab25,mu0);
    
    double chiMu   = get_value_lin(t_mu,Tchi,Ntab25,mu);
    double chiMu0  = get_value_lin(t_mu,Tchi,Ntab25,mu0);
    
    double cetaMu  = get_value_lin(t_mu,Tceta,Ntab25,mu);
    double cetaMu0 = get_value_lin(t_mu,Tceta,Ntab25,mu0);
    
    double h1Mu    = get_value_lin(t_mu,Th1mu,Ntab25,mu);
    double h1Mu0   = get_value_lin(t_mu,Th1mu,Ntab25,mu0);
    
    double h2Mu    = get_value_lin(t_mu,Th2,Ntab25,mu);
    double h2Mu0   = get_value_lin(t_mu,Th2,Ntab25,mu0);
    
    for (int i=0;i<3;i++)
        for (int j=0;j<3;j++)
            S[i][j]=m1[i][j]=m2[i][j]=m3[i][j]=m4[i][j]=0.;
    
    double st=sqrt(2.);
    
    m1[0][0]=0.75 * psiMu;
    m1[0][1]=0.75 * st * phiMu;
    m1[1][0]=0.75 * chiMu;
    m1[1][1]=0.75 * st * cetaMu;
    
    m2[0][0]=     psiMu0;
    m2[0][1]=     chiMu0;
    m2[1][0]=st * phiMu0;
    m2[1][1]=st * cetaMu0;
    
    //cout<<"m1-0 ";
    //Matrix_Print(m1[0],3,3);
    //cout<<"m2-0 ";
    //Matrix_Print(m2[0],3,3);
    
    Matrix_Mult(m1,m2,m1);
    
    //cout<<"m1 ";
    //Matrix_Print(m1[0],3,3);
    
    m3[0][0]= -4.* mu * mu0 * cos(phi0-phi);
    m3[0][2]=  2.* mu       * sin(phi0-phi);
    m3[2][0]=  2.* mu0      * sin(phi0-phi);
    m3[2][2]=                 cos(phi0-phi);
    
    double fac = 0.75 * h1Mu * h1Mu0;
    
    Matrix_SMult(fac,m3,m3);
    
    Matrix_Add(m1,m3,S);
    
    m4[0][0]= sqr(mu)*sqr(mu0)*cos(2*(phi0-phi));
    m4[0][1]=-sqr(mu)*cos(2*(phi0-phi));
    m4[0][2]=-sqr(mu)*mu0*sin(2*(phi0-phi));
    
    m4[1][0]=-sqr(mu0)*cos(2*(phi0-phi));
    m4[1][1]= cos(2*(phi0-phi));
    m4[1][2]= mu0*sin(2*(phi0-phi));
    
    m4[2][0]=-mu*sqr(mu0)*sin(2*(phi0-phi));
    m4[2][1]= mu*sin(2*(phi0-phi));
    m4[2][2]=-mu*mu0*cos(2*(phi0-phi));
    
    fac = 0.75 * h2Mu * h2Mu0;
    Matrix_SMult(fac,m4,m4);
    
    //   cout<<"m4 ";
    //   Matrix_Print(m4[0],3,3);
    
    Matrix_Add(S,m4,S);
    
    fac = 1./(1./mu0+1./mu);
    Matrix_SMult(fac,S,S);
    
    Matrix_Mult(Q,S,S);
    Matrix_SMult(2.*PI*mu/(4.*mu*mu0*PI),S,S);
    // we multiply with 2 PI because: Il+Ir integrated over upper hemisphere gives 1.
    // if it were constant, then we had Il+Ir = 1/ 2 PI everywhere; we would have to multiply with 2 PI to get a weight normalized to 1.
    // We multiply with mu to get the energy flux per area of the accretion disk. we divide by PI because we use F (and not PI x F) as the incoming flux.
    
    
    //    cout<<"F ";
    //    Matrix_Print(F,1,3);
    
    //    cout<<"S ";
    //    Matrix_Print(S[0],3,3);
    
    MatrixVector_Mult(S,F,I);
    
    //    cout<<"I ";
    //    Matrix_Print(I,1,3);
    
}
/*
 gg00 = g00[M, a, epsilon3, r];
 gg03 = g03[M, a, epsilon3, r];
 gg33 = g33[M, a, epsilon3, r];
 
 uu0 = ut[M, a, epsilon3, root, r];
 uu3 = uphi[M, a, epsilon3, root, r];
 
 
 AA = -(((gg03*uu0 + gg33*uu3)*Sqrt(-(Power(gg00*uu0 + gg03*uu3,2)/((Power(gg03,2) - gg00*gg33)*(gg00*Power(uu0,2) + uu3*(2*gg03*uu0 + gg33*uu3))))))/(gg00*uu0 + gg03*uu3));
 BB = Sqrt(Power(gg00*uu0 + gg03*uu3,2)/((-Power(gg03,2) + gg00*gg33)*(gg00*Power(uu0,2) + uu3*(2*gg03*uu0 + gg33*uu3))));
 */

void CS_emui(const std::vector<double> &metric, double emui[4][4])
{
    for(int i=0;i<4;i++)
        for(int j=0;j<4;j++)
            emui[i][j]=0.;
    
    emui[1][1]=1./sqrt(metric[make_metric_index(1, 1)]);
    emui[2][2]=1./sqrt(metric[make_metric_index(2, 2)]);
    
    double gg00=metric[make_metric_index(0, 0)];
    double gg03=metric[make_metric_index(0, 3)];
    double gg33=metric[make_metric_index(3, 3)];
    
    double AA=-(gg03/sqrt(gg00*(-sqr(gg03) + gg00*gg33)));
    double BB=sqrt(gg00/(-sqr(gg03) + gg00*gg33));
    emui[0][0]=1./sqrt(fabs(gg00));
    emui[0][3]=AA;
    emui[3][3]=BB;
}

void PF_eimu(double a, double r ,double eimu[4][4], double emui[4][4]) //BB // transforms BL to PF and vsv
{
    
    for(int i=0;i<4;i++){
        for(int j=0;j<4;j++){
            eimu[i][j]=0.;
            emui[i][j]=0.;
        }
    }
    
    double gg00=-(1 - 2/r);
    double gg03=-2*a*(pow(r,3))/pow(r,4);
    double gg33=(pow(r,6)+sqr(a)*(2+r)*(pow(r,3)))/pow(r,4);
    double gg11=pow(r,5)/(pow(r,4)*(-2+r)+sqr(a)*(pow(r,3)));
    double gg22=sqr(r);
    
    //    double g00i_equ=-1*((sqr(r)*(pow(r,6)+sqr(a)*(2+r)*(pow(r,3))))/((pow(r,3))*(pow(r,4)*(-2+r)+sqr(a)*(pow(r,3)))));
    //    double g03i_equ=-1*((2* sqr(r)* a)/(pow(r,4)*(-2+r)+ sqr(a)*(pow(r,3))));
    //    double g30i_equ=g03i_equ;
    //    double g33i_equ=(sqr(r)*(-2+r))/(pow(r,4)*(-2+r)+pow(a,2)*(pow(r,3)));
    
    
    double uu0=1/sqrt((pow(r,2)*((6*a)/pow(1/r,1.5)+2*pow(a,3)*sqrt(1/r) + (-3 + r)*pow(r,3) - 3*pow(a,2)*(1 + r)))/pow(pow(a,2) - pow(r,3),2));
    double uu3=(-(a*sqrt(1/r)) + r)/(sqrt(1/r)*(-pow(a,2) + pow(r,3))*sqrt((pow(r,2)*((6*a)/pow(1/r,1.5) + 2*pow(a,3)*sqrt(1/r) + (-3 + r)*pow(r,3) - 3*pow(a,2)*(1 + r)))/pow(pow(a,2) - pow(r,3),2)));
    
    double AA1 = -(((gg03*uu0 + gg33*uu3)*sqrt(-(pow(gg00*uu0 + gg03*uu3,2)/((pow(gg03,2) - gg00*gg33)*(gg00*pow(uu0,2) + uu3*(2*gg03*uu0 + gg33*uu3))))))/(gg00*uu0 + gg03*uu3));
    
    double BB1 = sqrt(pow(gg00*uu0 + gg03*uu3,2)/((-pow(gg03,2) + gg00*gg33)*(gg00*pow(uu0,2) + uu3*(2*gg03*uu0 + gg33*uu3))));
    
    double BB=BB1;
    double AA=AA1;
    
    if (BB1<=0) {
        AA=-1*AA1;
        BB=-1*BB1;
    }
    
    emui[0][0]=uu0;
    emui[0][3]=AA;
    emui[1][1]=1/(pow(gg11,0.5));
    emui[2][2]=1/(pow(gg22,0.5));
    emui[3][0]=uu3;
    emui[3][3]=BB;
    
    
    eimu[0][0]=(emui[1][1]*emui[2][2]*emui[3][3])/(-emui[0][3]*emui[1][1]*emui[2][2]*emui[3][0] + emui[0][0]*emui[1][1]*emui[2][2]*emui[3][3]);
    eimu[0][3]=-1*((emui[0][3]*emui[1][1]*emui[2][2])/(-emui[0][3]*emui[1][1]*emui[2][2]*emui[3][0] + emui[0][0]*emui[1][1]*emui[2][2]*emui[3][3]));
    eimu[1][1]=(-emui[0][3]*emui[2][2]*emui[3][0] + emui[0][0]*emui[2][2]*emui[3][3])/(-emui[0][3]*emui[1][1]*emui[2][2]*emui[3][0] + emui[0][0]*emui[1][1]*emui[2][2]*emui[3][3]);
    eimu[2][2]=(-emui[0][3]*emui[1][1]*emui[3][0] + emui[0][0]*emui[1][1]*emui[3][3])/(-emui[0][3]*emui[1][1]*emui[2][2]*emui[3][0] + emui[0][0]*emui[1][1]*emui[2][2]*emui[3][3]);
    eimu[3][0]=-1*((emui[3][0]*emui[1][1]*emui[2][2])/(-emui[0][3]*emui[1][1]*emui[2][2]*emui[3][0] + emui[0][0]*emui[1][1]*emui[2][2]*emui[3][3]));
    eimu[3][3]=(emui[0][0]*emui[1][1]*emui[2][2])/(-emui[0][3]*emui[1][1]*emui[2][2]*emui[3][0] + emui[0][0]*emui[1][1]*emui[2][2]*emui[3][3]);
}

// === general purpose functions ===

void load_table(double *t_x,int &numbins, char *fname1,int checknum)
/* load table from file */
{        
    cout <<"Opening "<<fname1<<"... ";
    
    ifstream fp_in;
    
    fp_in.open(fname1,ios::in);
    
    numbins=0;
    double val;
    
    while (fp_in >>val)
        t_x[numbins++]=val;
    
    cout <<"Read "<<numbins<<" values: "<<"Val[0]= "<<t_x[0]<<" ... Val["<<numbins-1<<"]="<<t_x[numbins-1]<<" . "<<endl;
    
    fp_in.close();
    
    if (numbins!=checknum) {cout << "The table does not fit:"<<numbins<<" read but "<<checknum<<" expected "<<endl; exit(1);}
}

double get_value_log(double *t_x,double *t_v,int numbins,double x)
{
    int i;
    double rest,res;
    double delta;
    
    if ((x<t_x[0])||(x>t_x[numbins-1]))
    {
        printf("Error in get_table : %f %f %f\n",x,t_x[0],t_x[numbins-1]);
        exit (-1);
    }
    
    for (i=0;i<numbins-1;i++)
        if (x<t_x[i]) break;
    
    delta =  log(t_x[i])-log(t_x[i-1]);
    rest  = (log(x     )-log(t_x[i-1]))/delta;
    res   = (1.-rest)*t_v[i-1] + rest*t_v[i];
    return res;
}

double get_value_lin(double *t_x,double *t_v,int numbins,double x)
{
    int i;
    double rest,res;
    double delta;
    
    if ((x<t_x[0])||(x>t_x[numbins-1]))
    {
        printf("Error in get_table : %f %f %f\n",x,t_x[0],t_x[numbins-1]);
        exit (-1);
    }
    
    for (i=0;i<numbins-1;i++)
        if (x<t_x[i]) break;
    
    delta =  t_x[i]-t_x[i-1];
    rest  = (x-t_x[i-1])/delta;
    res   = (1.-rest)*t_v[i-1] + rest*t_v[i];
    return res;
}

int linBin(double x1,double x2,int numbins,double x)
{
    double delta=(x2-x1)/(double)numbins;
    
    if ((x<x1)||(x>x2))
    {
        printf("Error in linBin : %f %f %f\n",x1,x2,x);
        exit (-1);
    }
    
    return (int)((x-x1)/delta);
}

int logBin(double x1,double x2,int numbins,double x)
{
    double delta=(log(x2)-log(x1))/(double)numbins;
    
    if ((x<x1)||(x>x2))
    {
        printf("Error in logBin : %f %f %f\n",x1,x2,x);
        exit (-1);
    }
    
    return (int)((log(x)-log(x1))/delta);
}

double binLog(double x1,double x2,int numbins,int nbin)
{
    double delta=(log(x2)-log(x1))/(double)numbins;
    double lval =exp(log(x1)+delta*((double)nbin+0.5));
    
    return lval;
}

void Matrix_SMult(double c,double a1[][3], double a2[][3])
{
    for(int i = 0; i < 3; i++)
        for(int j = 0; j < 3; j++)
            a2[i][j] =  c*a1[i][j];
}

void Matrix_Add(double a1[][3], double a2[][3], double a3[][3])
{
    for(int i = 0; i < 3; i++)
        for(int j = 0; j < 3; j++)
            a3[i][j] =  a1[i][j]+a2[i][j];
}

void Matrix_Mult(double a1[][3], double a2[][3], double a3[][3])
{
    double d[3][3];
    
    for(int i = 0; i < 3; i++)
        for(int j = 0; j < 3; j++)
            d[i][j] =  dot3(a1, a2, i, j);
    
    for(int i = 0; i < 3; i++)
        for(int j = 0; j < 3; j++)
            a3[i][j]=d[i][j];
}

double dot3(double a1[][3], double a2[][3], int aRow, int bCol)
{
    double sum = 0;
    for(int k = 0; k < 3; k++)
        sum += a1[aRow][k] * a2[k][bCol];
    return sum;
}

void MatrixVector_Mult(double a1[][3], double v[3], double r[3])
{
    double d[3];
    for(int i = 0; i < 3; i++) {
        d[i] = 0;
        for(int k = 0; k < 3; k++)
            d[i] += a1[i][k] * v[k];
    }
    for(int i = 0; i < 3; i++) r[i]=d[i];
}

void Matrix_Print(double *F,int n, int l)
{
    for (int i=0;i<n;i++,cout<<endl)
        for (int j=0;j<l;j++)
            cout<<F[i*l+j]<<" ";
}

double dot(double *a, double *b)
{
    double sum=0.;
    for(int i = 0; i < 3; i++)
        sum+=a[i]*b[i];
    return sum;
}

void normalize(double *v,double *n)
{
    double sum=0.;
    
    for (int i=0;i<3;i++) sum+=sqr(v[i]);
    sum=sqrt(sum);
    
    for (int i=0;i<3;i++) n[i] = v[i]/sum;
}

void perp(double *v,double *k,double *p)
{
    double knorm[3];
    normalize(k,knorm);
    
    double s=dot(v,knorm);
    
    for (int i=0;i<3;i++) p[i]=v[i]-s*knorm[i];
    normalize(p,p);
}

void cross(double *a, double *b, double *c)
{
    double d[3];
    d[0]=a[1]*b[2]-a[2]*b[1];
    d[1]=a[2]*b[0]-a[0]*b[2];
    d[2]=a[0]*b[1]-a[1]*b[0];
    for (int i=0;i<3;i++) c[i]=d[i];
}

double angle(double *aNorm,double *bNorm)
{
    double s=dot(aNorm,bNorm);
    return acos(s);
}

double getChi(double q, double u)
{
    double res = atan(u/q)/2.;
    if(q < 0.) res += PI/2.;
    else if (u < 0.) res += PI;
    return res;
}

/*
 double ran1()
 {
 return (double)rand()/(double)RAND_MAX;
 }
 */

/* Random number generator ran1 from Computers in Physics */
/* Volume 6 No. 5, 1992, 522-524, Press and Teukolsky */
/* To generate real random numbers 0.0-1.0 */
/* Should be seeded with a negative integer */
#define IA 16807
#define IM 2147483647
#define IQ 127773
#define IR 2836
#define NTAB 32
#define EPS (1.2E-07)
#define MAX(a,b) (a>b)?a:b
#define MIN(a,b) (a<b)?a:b

double ran1(long *idum)
{
    int j,k;
    static int iv[NTAB],iy=0;
    void nrerror();
    static double NDIV = 1.0/(1.0+(IM-1.0)/NTAB);
    static double RNMX = (1.0-EPS);
    static double AM = (1.0/IM);
    
    if ((*idum <= 0) || (iy == 0)) {
        *idum = MAX(-*idum,*idum);
        for(j=NTAB+7;j>=0;j--) {
            k = *idum/IQ;
            *idum = IA*(*idum-k*IQ)-IR*k;
            if(*idum < 0) *idum += IM;
            if(j < NTAB) iv[j] = *idum;
        }
        iy = iv[0];
    }
    k = *idum/IQ;
    *idum = IA*(*idum-k*IQ)-IR*k;
    if(*idum<0) *idum += IM;
    j = iy*NDIV;
    iy = iv[j];
    iv[j] = *idum;
    return MIN(AM*iy,RNMX);
}
#undef IA 
#undef IM 
#undef IQ
#undef IR
#undef NTAB
#undef EPS 
#undef MAX
#undef MIN

/************************************************** **********************/
/* Please Note: */
/* */
/* (1) This computer program is written by Tao Pang in conjunction with */
/* his book, "An Introduction to Computational Physics," published */
/* by Cambridge University Press in 1997. */
/* */
/* (2) No warranties, express or implied, are made for this program. */
/* */
/************************************************** **********************/

void migs (double a[][NMAX],int n,double x[][NMAX])
/* Function to invert matrix a[][] with the inverse stored
 in x[][] in the output. Copyright (c) Tao Pang 2001. */
/*
 double a[4][4]={{5., 0., 0., 1.}, {0., 2., 1., 0.}, {0., 3., 0., 0.}, {0., 0., 0.,
 5.}};
 double b[4][4];
 migs(a,4,b);
 for (int i=0;i<4;i++,cout<<endl)
 for (int j=0;j<4;j++)
 cout<<b[i][j]<<" ";
 */
{
    int i,j,k;
    double b[NMAX][NMAX];
    int indx[NMAX];
    
    if (n > NMAX)
    {
        printf("The matrix dimension is too large.\n");
        exit(1);
    }
    
    for (i = 0; i < n; ++i)
    {
        for (j = 0; j < n; ++j)
        {
            b[i][j] = 0;
        }
    }
    for (i = 0; i < n; ++i)
    {
        b[i][i] = 1;
    }
    
    elgs (a,n,indx);
    
    for (i = 0; i < n-1; ++i)
    {
        for (j = i+1; j < n; ++j)
        {
            for (k = 0; k < n; ++k)
            {
                b[indx[j]][k] = b[indx[j]][k]-a[indx[j]][i]*b[indx[i]][k];
            }
        }
    }
    
    for (i = 0; i < n; ++i)
    {
        x[n-1][i] = b[indx[n-1]][i]/a[indx[n-1]][n-1];
        for (j = n-2; j >= 0; j = j-1)
        {
            x[j][i] = b[indx[j]][i];
            for (k = j+1; k < n; ++k)
            {
                x[j][i] = x[j][i]-a[indx[j]][k]*x[k][i];
            }
            x[j][i] = x[j][i]/a[indx[j]][j];
        }
    }
}

void elgs (double a[][NMAX],int n,int indx[NMAX])

/* Function to perform the partial-pivoting Gaussian elimination.
 a[][] is the original matrix in the input and transformed
 matrix plus the pivoting element ratios below the diagonal
 in the output. indx[] records the pivoting order.
 Copyright (c) Tao Pang 2001. */
{
    int i, j, k, itmp;
    double c1, pi, pi1, pj;
    double c[NMAX];
    k=0;
    
    if (n > NMAX)
    {
        printf("The matrix dimension is too large.\n");
        exit(1);
    }
    
    /* Initialize the index */
    
    for (i = 0; i < n; ++i)
    {
        indx[i] = i;
    }
    
    /* Find the rescaling factors, one from each row */
    
    for (i = 0; i < n; ++i)
    {
        c1 = 0;
        for (j = 0; j < n; ++j)
        {
            if (fabs(a[i][j]) > c1) c1 = fabs(a[i][j]);
        }
        c[i] = c1;
    }
    
    /* Search the pivoting (largest) element from each column */
    
    for (j = 0; j < n-1; ++j)
    {
        pi1 = 0;
        for (i = j; i < n; ++i)
        {
            pi = fabs(a[indx[i]][j])/c[indx[i]];
            if (pi > pi1)
            {
                pi1 = pi;
                k = i;
            }
        }
        
        /* Interchange the rows via indx[] to record pivoting order */
        
        itmp = indx[j];
        indx[j] = indx[k];
        indx[k] = itmp;
        for (i = j+1; i < n; ++i)
        {
            pj = a[indx[i]][j]/a[indx[j]][j];
            
            /* Record pivoting ratios below the diagonal */
            
            a[indx[i]][j] = pj;
            
            /* Modify other elements accordingly */
            
            for (k = j+1; k < n; ++k)
            {
                a[indx[i]][k] = a[indx[i]][k]-pj*a[indx[j]][k];
            }
        }
    }
}
// Fe-K alpha vs Compton Hump from George and Fabian 1991
// Figure 6 E < 15
double comptonLow(double En, double th)
{
    double compton = 0.024835-0.004668*En+0.00039699*pow(En,2)-0.00006*th +(7E-6)*pow(th,2)-(1.6E-7)*pow(th,3)+(1.33E-9)*pow(th,4);
    return compton;
}
// Figure 6 E > 15
double comptonHigh(double En, double th)
{
    double compton = 0.017196 + 0.0017959*En + 0.00008*th -(5E-6)*pow(th,2)+(9E-8)*pow(th,3);
    return compton;
}
//Figure 1
double effectiveYield(double theta)
{
    double yield =  0.00207574 - 0.0000320427 * theta + 7.4266E-7*sqr(theta);
    return yield;
}

