//
//  config.h
//  
//
//  Created by Henric Krawczynski on 7/16/20.
//

#ifndef config_h
#define config_h

#include <stdio.h>
class Config
{
    // hold current coordinates
    int lampPost;
    bool thermalDisk;
    bool chandra;
    
public:

    Config()                {lampPost=0;thermalDisk=chandra=false;};

    // lamppost
    void setLampPost(int id) {lampPost=id;};
    int  getLampPost()       {return lampPost;};
    bool discreteEnergies()  {return (lampPost==1)||(lampPost==2);};
    bool getIronReflection() {return (lampPost==1)||(lampPost==2);};

    // thermal disk
    void setThermalDisk(bool flag)  {thermalDisk=flag;};
    bool getThermalDisk()           {return thermalDisk;};
    
    // Chandrasekhar reflection
    void setChandraReflection(bool flag)  {chandra=flag;};
    bool getChandraReflection()           {return chandra;};
    
};

#endif /* config_h */
