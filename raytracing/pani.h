#ifndef XTRACK_PANI_H_INCLUDED
#define XTRACK_PANI_H_INCLUDED

#include <vector>

class PaniMetric {
public:
  static void Initialize(double);
  static void CalculateChristoffelSymbols(double M, double a, double hair, const double *y, std::vector<double> &symbols);
  static void CalculateMetricElements(double M, double a, double hair, const double *y, std::vector<double> &metric);
  static void CalculateMetricElements3(double M, double a, double hair, const double *y, std::vector<double> &metric);
};


#endif // XTRACK_PANI_H_INCLUDED
