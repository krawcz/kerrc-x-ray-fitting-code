#ifndef XTRACK_GLAMPEDAKIS_H_INCLUDED
#define XTRACK_GLAMPEDAKIS_H_INCLUDED

#include <vector>

class GlampedakisMetric {
public:
  static void Initialize(double);
  static void CalculateChristoffelSymbols(double M, double a, double hair, const double *y, std::vector<double> &symbols);
  static void CalculateMetricElements(double M, double a, double hair, const double *y, std::vector<double> &metric);
  static void CalculateMetricElements3(double M, double a, double hair, const double *y, std::vector<double> &metric);
};


#endif // XTRACK_GLAMPEDAKIS_H_INCLUDED
