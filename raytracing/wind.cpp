#include <iostream>
#include <string>

#include "xTrack.h"
#include "xTrackMath.h"
#include "metric.h"
#include "macro.h"
#include "kerr.h"
#include "wind.h"

void Wind::init(std::string model,std::vector<double> &p,Disk *disk_p)
{
  d_p=disk_p;

  gC.resize(16, 0);

  // t_alpha->assign(D,vector<double>(4,0.));

  // copy input parameters

  for (auto element:p)
    parms.push_back(element);

  // which model are we talking about?
  model_ID=0;
  if (model.compare("cone") == 0) model_ID=1;
  if (model.compare("linear") == 0) model_ID=2;

  std::cout<<"Model "<<model_ID<<std::endl;

  // calculate missing model parameters
  if (model_ID==1)
    {
      double xoff = pow(1.7320508075688767,1./(parms[Cone::n]-1.))*parms[Cone::x0]*pow(parms[Cone::x0]/parms[Cone::n],1./(parms[Cone::n]-1.));
      parms[Cone::xoff]=xoff;
      parms[Cone::g0]=gMin(parms[Cone::x1]);
      
      std::cout<<std::endl;
      for (double x=parms[Cone::x1];x<10.;x*=1.1)
	std::cout<<"xc= "<<x<<" rc "<<rc(x)<<" g0 = "<<gMin(x)<<std::endl;

      std::cout<<std::endl;
      for (double x=parms[Cone::x1];x<10;x*=1.1)
	std::cout<<"xc= "<<x<<" rc "<<rc(x)<<" gamma= "<<gamma(x)<<std::endl;

      std::cout<<std::endl;

      std::cout<<"black hole spin a "<<parms[Cone::a]<<std::endl;
      std::cout<<"x1                "<<parms[Cone::x1]<<std::endl;
      std::cout<<"x2                "<<parms[Cone::x2]<<std::endl;
      std::cout<<"n                 "<<parms[Cone::n]<<std::endl;
      std::cout<<"x0                "<<parms[Cone::x0]<<std::endl;
      std::cout<<"z0                "<<parms[Cone::z0]<<std::endl;
      std::cout<<"GammaInfty        "<<parms[Cone::gInf]<<std::endl;
      std::cout<<"sProb             "<<parms[Cone::sProb]<<std::endl;
      std::cout<<"xoff              "<<parms[Cone::xoff]<<std::endl;
      std::cout<<"gammaMin(z=0)     "<<parms[Cone::g0]<<std::endl;
        initFlag=true;
    }

  if (model_ID==2)
    {
      debug_on();

      std::cout<<std::endl;

      std::cout<<"black hole spin a "<<parms[Cone::a]<<std::endl;
      std::cout<<"x1                "<<parms[Cone::x1]<<std::endl;
      std::cout<<"x2                "<<parms[Cone::x2]<<std::endl;

      std::cout<<"alpha             "<<parms[Linear::alpha]<<std::endl;

      parms[Linear::slope] = 1./tan(parms[Linear::alpha]/RtD);
      std::cout<<"dz/dr             "<<parms[Linear::slope]<<std::endl;

      std::cout<<"z0                "<<parms[Cone::z0]<<std::endl;
      std::cout<<"GammaInfty        "<<parms[Cone::gInf]<<std::endl;
      std::cout<<"sProb             "<<parms[Cone::sProb]<<std::endl;
      std::cout<<"xoff              "<<parms[Cone::xoff]<<std::endl;

      load_g(parms[Cone::x1],PI/2.);

      // ZAMO
      double w  = omega(gC);
      double u0 = sqrt(-1./(g00()+g33()*w*w+2.*g03()*w));
      Vector u;
      u[0]=u0;
      u[1]=0;
      u[2]=0;
      u[3]=u0*w;

      // Keppler
      double Omega = 1./(pow(blC[1],1.5)+parms[Cone::a]);
      double uk0 = sqrt(-1./(g00()+g33()*Omega*Omega+2.*g03()*Omega));
      Ll = uk0*(g03()+g33()*Omega);

      Vector uk;
      uk[0]=uk0;
      uk[1]=0;
      uk[2]=0;
      uk[3]=uk0*Omega;

      parms[Cone::g0] = gammaMin = -dot(u,uk);
      std::cout<<"gammaMin(z=0)     "<<parms[Cone::g0]<<std::endl;
      std::cout <<"Wind Model 2, "<<" Omega = "<<Omega<<" Ll = "<<Ll<<std::endl;

      std::cout<<std::endl;
      for (double x=parms[Cone::x1];x<20;x*=1.1)
	std::cout<<"xc= "<<x<<" rc "<<rc(x)<<" gamma= "<<gammaN(x)<<std::endl;

      for (double xc=parms[Cone::x1]; xc<50; xc*=1.2)
	{
	  double z=zc(xc);
	  double r=sqrt(xc*xc+z*z);
	  double t=PI/2.-atan(z/xc);
	  load_g(r,t);
	  Vector u;
	  uWind(u); 
	}
      debug_off();
    }
}

double Wind::zc(double xc)
{
  double res;

  switch (model_ID) {
  case 1 : 
    {
      double xd = xc + parms[Cone::xoff] - parms[Cone::x1];
      res = pow(xd/parms[Cone::x0],parms[Cone::n]) 
	- pow(parms[Cone::xoff]/parms[Cone::x0],parms[Cone::n]);
    }
    break;

  case 2 :
    {
      res = (xc-parms[Cone::x1])*parms[Linear::slope];
    }
    break;

  default:
    std::cout << "Invalid model in Wind::zc" << std::endl;
    exit(-1);
  }
  return res;
} 

double Wind::omega(std::vector<double> g)
{
  return -g[make_metric_index(3, 0)]/g[make_metric_index(3, 3)];
}

double Wind::gMin(double x)
{
  Vector y;

  y[0]=0.;
  y[1]=rc(x);
  y[2]=tc(x);
  y[3]=0.;
  
  std::vector<double> g(16, 0);
  CalculateMetricElements(d_p->M, d_p->a, d_p->hair, y, g);

  double w=omega(g);
  double a=parms[Cone::a];

  double r=y[1];
  double ct=cos(y[2]);
  double st=sin(y[2]);

  double res=1/sqrt((pow(a,2)*pow(ct,2)*(1 - (pow(a,2) + pow(r,2))*pow(w,2)*pow(st,2)) + 
		     r*(-2 + r - w*pow(st,2)*(-4*a + r*(pow(a,2) + pow(r,2))*w + 2*pow(a,2)*w*pow(st,2))))
		    /(pow(r,2) + pow(a,2)*pow(ct,2)));
  //  std::cout<<"gMin r "<<y[1]<<" t "<<y[2]<<" res "<<res<<std::endl;

  return res;
}

double Wind::rc(double xc)
{
  return sqrt(pow(xc,2)+pow(zc(xc),2));
}

double Wind::tc(double xc)
{
  return PI/2. - atan2(zc(xc),xc);
}

double Wind::gamma(double xx)
{
  double z=zc(xx);

  double fac   = 1. + (2./PI*atan2(z,parms[Cone::z0])*(parms[Cone::gInf]-1.));
  double gamma = fac*gMin(xx);
 
  return gamma;
}

double Wind::gammaN(double xx)
{
  double z=zc(xx);
  double gammaN   = gammaMin + (2./PI*atan2(z,parms[Cone::z0])*(parms[Cone::gInf]-gammaMin)); 
  return gammaN;
}
    
void Wind::load_g(double r, double t)
{
  blC[0]=0.;
  blC[1]=r;
  blC[2]=t;
  blC[3]=0.;
  rC=r; tC=t; xC=r*sin(t); zC=zc(xC);
  CalculateMetricElements(d_p->M, d_p->a, d_p->hair, blC, gC);

}

void Wind::printG()
{
  std::cout<<"Metric r "<<rC<<" t "<<tC<<std::endl;
  for (int i=0;i<D;i++)
    {
      for (int j=0;j<D;j++)
	std::cout<<"g_"<<i<<","<<j<<" = "<<gC[make_metric_index(i, j)]<<" ";
      std::cout<<std::endl;
    }
}

void Wind::uWind(Vector u) // calculate 4-vel. of wind at current location stored in blC, rC,tC,
{
  // given gamma,dr,dt,
  // looking for "sc" which gives u^2=-1 with u=(gamma, sc*dr, sc*dt,gamma*w).
  // use Mathematica derived equation for sc

  double delta=(1e-8)/2.;
  double dr=(rc(xC+delta)-rc(xC-delta))/2./delta;
  double dt=(tc(xC+delta)-tc(xC-delta))/2./delta;

  // new code
  double g=gammaN(xC); // gamma-factor
  double w=omega(gC); // ZAMO omega

  double r=rC; 
  double ct=cos(tC);
  double st=sin(tC);
  double spin=parms[Cone::a];

  double Delta=r*r-2*r+spin*spin;
  double Sigma=r*r+spin*spin*ct*ct;
  double A=pow(r*r+spin*spin,2.)-spin*spin*Delta*st*st;

  double u0  = sqrt(A/(Sigma*Delta));
  double u3  = u0*w;
  double e33 = sqrt(Sigma/A)/st;

  double bb = Ll/g33();
  double a  = g;
  double Omega = w+bb/(a*u0);
  double v3 = bb*g33()*e33;

  double cd=(g*g-v3*v3-1.)/(g11()*dr*dr+g22()*dt*dt);
  double c =sqrt(fabs(cd));

  u[0]=a*u0;
  u[1]=c*dr;
  u[2]=c*dt;
  u[3]=a*u3+bb;

  if (debug)
    {

      Vector e0,e3,bv;
      bv[0]=0.;
      bv[1]=0.;
      bv[2]=0.;
      bv[3]=bb;

      e0[0]=u0;
      e0[1]=0;
      e0[2]=0;
      e0[3]=u3;

      e3[0]=0;
      e3[1]=0;
      e3[2]=0;
      e3[3]=e33;

      std::cout<<" dot(e0,e3) "<<dot(e0,e3)<<std::endl;
      std::cout<<" dot(e0,e0) "<<dot(e0,e0)<<std::endl;
      std::cout<<" dot(e3,e3) "<<dot(e3,e3)<<std::endl;
      std::cout<<" dot(u,e0) "<<dot(u,e0)<<" "<<-a<<std::endl;
      std::cout<<" dot(bv,e0) "<<dot(bv,e0)<<" (=0)"<<std::endl;
      std::cout<<" dot(u,e3) "<<dot(u,e3)<<" "<<bb*g33()*e33<<std::endl;

      double Lw = g03()*u[0]+g33()*u[3];
      double norm1=g00()*pow(u0,2.)+g33()*pow(u3,2.)+2.*g03()*u0*u3;

      double norm2=g00()*pow(u[0],2.)+g11()*pow(u[1],2.)+g22()*pow(u[2],2.)+g33()*pow(u[3],2.)+2.*g03()*u[0]*u[3];
      double nn = -g*g+v3*v3+c*c*(g11()*dr*dr+g22()*dt*dt);

      std::cout<<" norm1 "<<norm1<<" norm2 "<<norm2<<" nn "<<nn<<" Ll "<<Ll<<" Lw "<<Lw<<" r st "<<r*st<<" r ct "<<r*ct<<" g "<<g<<" w "<<w<<" Omega "<<Omega<<" a "<<a<<" c "<<c<<std::endl;
    }

  /*
  double g=gamma(xC); // gamma-factor
  double w=omega(gC); // ZAMO omega

  double ct=cos(tC);
  double st=sin(tC);
  double a=parms[Cone::a];

  double sc= pow((double)((-2. + rC)*rC + pow(a,2.))*(-(pow(a,2.)*pow(ct,2.)) - pow(rC,2.) - 2*rC*pow(g,2.) + 
      pow(a,2.)*pow(ct,2.)*pow(g,2.) + pow(rC,2.)*pow(g,2.) + 4*a*rC*w*pow(st,2.)*pow(g,2.) - 
			pow(st,2.)*(pow(a,4.)*pow(ct,2.) + pow(rC,4.) + 
				   rC*pow(a,2.)*(rC + rC*pow(ct,2.) + 2*pow(st,2.)))*pow(g,2.)*pow(w,2.))*
				   pow(pow(dr,2.) + ((-2 + rC)*rC + pow(a,2.))*pow(dt,2.),-1)*
		 pow(pow(a,2.)*pow(ct,2.) + pow(rC,2.),-2.),0.5);

  u[0]=g;
  u[1]=sc*dr;
  u[2]=sc*dt;
  u[3]=g*w;
  */

#ifdef debugWind
  std::cout<<" dr "<<dr<<std::endl; 
  std::cout<<" dt "<<dt<<std::endl; 
  std::cout<<" zC "<<zC<<std::endl; 
  std::cout<<" gamma "<<g<<std::endl; 
  std::cout<<" omega "<<w<<std::endl; 
  std::cout<<" ct "<<ct<<std::endl; 
  std::cout<<" st "<<st<<std::endl; 
  std::cout<<" a "<<a<<std::endl; 
  std::cout<<" rC "<<rC<<std::endl; 
  std::cout<<" sc "<<sc<<std::endl; 
  std::cout<<"u = ( "<<u[0]<<" , "<<u[1]<<" , "<<u[2]<<" , "<<u[3]<<" )"<<std::endl;
  std::cout<<"square(u) "<<square(u)<<std::endl<<std::endl;
#endif
}

double Wind::dot(Vector u,Vector v)
{
  double sum=0.;
  for (int i=0;i<D;i++)
    for (int j=0;j<D;j++)
      sum+=gC[make_metric_index(i, j)]*u[i]*v[j];
  return sum;
} 

// get normalization of vector
double Wind::square(Vector u)
{
  return dot(u,u);
} 

// return normalized version of vector
void Wind::normalize(Vector u)
{
  double sc=sqrt(fabs(square(u)));
  for (int i=0;i<D;i++)
    u[i]/=sc;
} 

void Wind::add(Vector u,Vector v,double scale)
{
  for (int i=0;i<D;i++)
    u[i]+=scale*v[i];
}

// copy vector u into v
void Wind::copy(Vector u,Vector v)
{
  for (int i=0;i<D;i++)
    v[i]=u[i];
} 

void Wind::define_tetrad()
{
  // get t0
  uWind(tetrad[0]);
  
  Vector u; 
  // get t1
  double delta=(1e-8)/2.;
  double dr=(rc(xC+delta)-rc(xC-delta))/2./delta;
  double dt=(tc(xC+delta)-tc(xC-delta))/2./delta;
	     
  u[0]=0.;
  u[1]=dr;
  u[2]=dt;
  u[3]=0.;

  add(u,tetrad[0],dot(u,tetrad[0]));  
  normalize(u);
  copy(u,tetrad[1]);

  // get t3
  u[0]=0.;
  u[1]=0.;
  u[2]=0.;
  u[3]=1.;

  add(u,tetrad[0],dot(u,tetrad[0]));
  add(u,tetrad[1],-dot(u,tetrad[1]));
  normalize(u);
  copy(u,tetrad[3]);

  // get t2

  u[0]=0.;
  u[1]=0.;

  if (tC<PI/2.)
    u[2]=1.; // point "parallel" to e_theta, i.e. away from the z-axis "into" the wind
  else
    u[2]=-1.;

  u[3]=0.;

  add(u,tetrad[0],dot(u,tetrad[0]));
  add(u,tetrad[1],-dot(u,tetrad[1]));
  add(u,tetrad[3],-dot(u,tetrad[3]));
  normalize(u);
  copy(u,tetrad[2]);
}

void Wind::printTetrad()
{
  std::cout<<"Tetrad "<<std::endl;
  for (int i=0;i<D;i++)
    {
      std::cout<<"t"<<i<<" ( ";
      for (int j=0;j<D;j++)
	std::cout<<tetrad[i][j]<<" ";
      std::cout<<")"<<std::endl;
    }

  for (int i=0;i<D;i++)
    for (int j=i;j<D;j++)
      std::cout << "dot(t" << i << ", t" << j << ")=" <<dot(tetrad[i],tetrad[j])<<std::endl;
}

void Wind::Tetrad2BL(Vector tCoord,Vector sum)
{
  for(int i=0;i<D;i++)
    sum[i]=0.;
  
  for(int i=0;i<D;i++)
    add(sum,tetrad[i],tCoord[i]);
}

void Wind::BL2Tetrad(Vector bl,Vector sum)
{
  sum[0]=-dot(bl,tetrad[0]);
  for(int i=1;i<D;i++)
    sum[i]=dot(bl,tetrad[i]);
}

bool Wind::hit(double r,double t)
{
  // determine if we are outside (false) or inside (true) wind.
  if ((r<parms[Cone::x1])||(r>parms[Cone::x2])) return false;

  double myx=r*fabs(sin(t));
  double myz=r*fabs(cos(t));

  //  if (debug)
  //  std::cout<<"hit "<<r<<" "<<t<<" "<<myx<<" "<<myz<<" "<<zc(myx)<<std::endl;

  return (bool)(myz<zc(myx));
}

double Wind::correct(double r1,double t1,double r2,double t2)
{
  // the current step would take r1,t1 in front of wind to r2,t2 behind wind.
  // determine fraction step that will take you just in front of the wind.

  double xc1=xc(r1,t1);
  double xc2=xc(r2,t2);

#ifdef debugWind
  std::cout<<"xc1 "<<xc1<<" xc2 "<<xc2<<std::endl;
  std::cout<<"rc1 "<<rc(xc1)<<" tc1 "<<tc(xc1)<<std::endl;
  std::cout<<"rc2 "<<rc(xc2)<<" tc2 "<<tc(xc2)<<std::endl;
#endif

  // use theorem on intersecting lines here
  double dt1=r1*cos(t1)-rc(xc1)*cos(tc(xc1));
  double dt2=rc(xc2)*cos(tc(xc2))-r2*cos(t2);

#ifdef debugWind
  std::cout<<"dt1 "<<dt1<<" "<<r1<<" "<<t1<<" "<<rc(xc1)<<" "<<tc(xc1)<<std::endl;
  std::cout<<"dt2 "<<dt2<<" "<<rc(xc2)<<" "<<tc(xc2)<<" "<<r2<<" "<<t2<<std::endl;
#endif

  double res=dt1/(dt1+dt2);
  return res;
}
