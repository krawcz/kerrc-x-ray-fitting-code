#include "pani.h"
#include "metric.h"
#include "xTrackMath.h"

#include <cmath>
#include <iostream>


void PaniMetric::Initialize(double)
{
  ::CalculateMetricElements = &PaniMetric::CalculateMetricElements;
  ::CalculateMetricElements3 = &PaniMetric::CalculateMetricElements3;
  ::CalculateChristoffelSymbols = &PaniMetric::CalculateChristoffelSymbols;
}
  

void PaniMetric::CalculateChristoffelSymbols(double M, double a, double hair, const double *y, std::vector<double> &symbols)
{
  const double a_2 = a*a;
  const double a_3 = a_2*a;
  const double M_2 = M*M;
  const double hair_2 = hair*hair;
  const double y1 = y[1];
  const double y1_2 = y1*y1;
  const double y1_3 = y1_2*y1;
  const double y1_4 = y1_3*y1;
  const double y1_5 = y1_4*y1;
  const double y1_6 = y1_5*y1;
  const double y1_7 = y1_6*y1;
  const double y1_8 = y1_7*y1;
  const double y1_9 = y1_8*y1;
  const double y1_12 = y1_9*y1_3;
  const double y2 = y[2];
  const double sin_y2 = sin(y2);
  const double sin_y2_2 = sin_y2*sin_y2;
  const double sin_y2_3 = sin_y2_2*sin_y2;
  const double sin_y2_4 = sin_y2_3*sin_y2;
  const double cos_y2 = cos(y2);
  const double cos_y2_2 = cos_y2*cos_y2;
  const double h = pow(-56*y1_5*M + hair_2*(70*y1_2 + 120*y[1]*M + 189*M_2),2);
  
  symbols[1] = symbols[4] =
    -(a*(-56*y1_5*M + hair_2*(70*y1_2 + 120*y[1]*M + 189*M_2))*sin_y2_2*((a*(-280*y1_4*M + hair_2*(140*y[1] + 120*M))*sin_y2_2)/(28.*y1_6) - (3*a*(-56*y1_5*M + hair_2*(70*y1_2 + 120*y[1]*M + 189*M_2))*sin_y2_2)/(14.*y1_7)))/(56.*y1_6*(-(a_2*h*sin_y2_4)/(784.*y1_12) + y1_2*(-1 + (2*M)/y[1] - (2*a_2*M*cos_y2_2)/y1_3)*sin_y2_2*(1 + (a_2*(y[1] + 2*M*sin_y2_2))/y1_3))) + (y1_2*((-2*M)/y1_2 + (6*a_2*M*cos_y2_2)/y1_4)*sin_y2_2*(1 + (a_2*(y[1] + 2*M*sin_y2_2))/y1_3))/(2.*(-(a_2*h*sin_y2_4)/(784.*y1_12) + y1_2*(-1 + (2*M)/y[1] - (2*a_2*M*cos_y2_2)/y1_3)*sin_y2_2*(1 + (a_2*(y[1] + 2*M*sin_y2_2))/y1_3)));

  symbols[2] = symbols[8] =
    -(a_2*h*cos_y2*sin_y2_3)/(784.*y1_12*(-(a_2*h*sin_y2_4)/(784.*y1_12) + y1_2*(-1 + (2*M)/y[1] - (2*a_2*M*cos_y2_2)/y1_3)*sin_y2_2*(1 + (a_2*(y[1] + 2*M*sin_y2_2))/y1_3))) + (2*a_2*M*cos_y2*sin_y2_3*(1 + (a_2*(y[1] + 2*M*sin_y2_2))/y1_3))/(y[1]*(-(a_2*h*sin_y2_4)/(784.*y1_12) + y1_2*(-1 + (2*M)/y[1] - (2*a_2*M*cos_y2_2)/y1_3)*sin_y2_2*(1 + (a_2*(y[1] + 2*M*sin_y2_2))/y1_3)));

  symbols[7] = symbols[13] =
    (y1_2*sin_y2_2*((a*(-280*y1_4*M + hair_2*(140*y[1] + 120*M))*sin_y2_2)/(28.*y1_6) - (3*a*(-56*y1_5*M + hair_2*(70*y1_2 + 120*y[1]*M + 189*M_2))*sin_y2_2)/(14.*y1_7))*(1 + (a_2*(y[1] + 2*M*sin_y2_2))/y1_3))/(2.*(-(a_2*h*sin_y2_4)/(784.*y1_12) + y1_2*(-1 + (2*M)/y[1] - (2*a_2*M*cos_y2_2)/y1_3)*sin_y2_2*(1 + (a_2*(y[1] + 2*M*sin_y2_2))/y1_3))) - (a*(-56*y1_5*M + hair_2*(70*y1_2 + 120*y[1]*M + 189*M_2))*sin_y2_2*(y1_2*sin_y2_2*(a_2/y1_3 - (3*a_2*(y[1] + 2*M*sin_y2_2))/y1_4) + 2*y[1]*sin_y2_2*(1 + (a_2*(y[1] + 2*M*sin_y2_2))/y1_3)))/(56.*y1_6*(-(a_2*h*sin_y2_4)/(784.*y1_12) + y1_2*(-1 + (2*M)/y[1] - (2*a_2*M*cos_y2_2)/y1_3)*sin_y2_2*(1 + (a_2*(y[1] + 2*M*sin_y2_2))/y1_3)));

  symbols[11] = symbols[14] =
    (a*(-56*y1_5*M + hair_2*(70*y1_2 + 120*y[1]*M + 189*M_2))*cos_y2*sin_y2_3*(1 + (a_2*(y[1] + 2*M*sin_y2_2))/y1_3))/(28.*y1_4*(-(a_2*h*sin_y2_4)/(784.*y1_12) + y1_2*(-1 + (2*M)/y[1] - (2*a_2*M*cos_y2_2)/y1_3)*sin_y2_2*(1 + (a_2*(y[1] + 2*M*sin_y2_2))/y1_3))) - (a*(-56*y1_5*M + hair_2*(70*y1_2 + 120*y[1]*M + 189*M_2))*sin_y2_2*((4*a_2*M*cos_y2*sin_y2_3)/y[1] + 2*y1_2*cos_y2*sin_y2*(1 + (a_2*(y[1] + 2*M*sin_y2_2))/y1_3)))/(56.*y1_6*(-(a_2*h*sin_y2_4)/(784.*y1_12) + y1_2*(-1 + (2*M)/y[1] - (2*a_2*M*cos_y2_2)/y1_3)*sin_y2_2*(1 + (a_2*(y[1] + 2*M*sin_y2_2))/y1_3)));

  symbols[16] = (((2*M)/y1_2 - (6*a_2*M*cos_y2_2)/y1_4)*(1 - (2*M)/y[1] + (a_2*(y[1] + (-y[1] + 2*M)*cos_y2_2))/y1_3))/2.;

  symbols[19] = symbols[28] =
    ((1 - (2*M)/y[1] + (a_2*(y[1] + (-y[1] + 2*M)*cos_y2_2))/y1_3)*(-(a*(-280*y1_4*M + hair_2*(140*y[1] + 120*M))*sin_y2_2)/(28.*y1_6) + (3*a*(-56*y1_5*M + hair_2*(70*y1_2 + 120*y[1]*M + 189*M_2))*sin_y2_2)/(14.*y1_7)))/2.;

  symbols[21] = -((2*M)/y1_2 + (a_2*(1 - cos_y2_2))/y1_3 - (3*a_2*(y[1] + (-y[1] + 2*M)*cos_y2_2))/y1_4)/(2.*(1 - (2*M)/y[1] + (a_2*(y[1] + (-y[1] + 2*M)*cos_y2_2))/y1_3));

  symbols[22] = symbols[25] =
    (a_2*(-y[1] + 2*M)*cos_y2*sin_y2)/(y1_3*(1 - (2*M)/y[1] + (a_2*(y[1] + (-y[1] + 2*M)*cos_y2_2))/y1_3));

  symbols[26] = -(y[1]*(1 - (2*M)/y[1] + (a_2*(y[1] + (-y[1] + 2*M)*cos_y2_2))/y1_3));

  symbols[31] = ((1 - (2*M)/y[1] + (a_2*(y[1] + (-y[1] + 2*M)*cos_y2_2))/y1_3)*(-(y1_2*sin_y2_2*(a_2/y1_3 - (3*a_2*(y[1] + 2*M*sin_y2_2))/y1_4)) - 2*y[1]*sin_y2_2*(1 + (a_2*(y[1] + 2*M*sin_y2_2))/y1_3)))/2.;

  symbols[32] = (-2*a_2*M*cos_y2*sin_y2)/(y1_3*(y1_2 + a_2*cos_y2_2));

  symbols[35] = symbols[44] =
    -(a*(-56*y1_5*M + hair_2*(70*y1_2 + 120*y[1]*M + 189*M_2))*cos_y2*sin_y2)/(28.*y1_6*(y1_2 + a_2*cos_y2_2));

  symbols[37] = -((a_2*(-y[1] + 2*M)*cos_y2*sin_y2)/(y1_3*(y1_2 + a_2*cos_y2_2)*sqr(1 - (2*M)/y[1] + (a_2*(y[1] + (-y[1] + 2*M)*cos_y2_2))/y1_3)));

  symbols[38] = symbols[41] =
    y[1]/(y1_2 + a_2*cos_y2_2);

  symbols[42] = -((a_2*cos_y2*sin_y2)/(y1_2 + a_2*cos_y2_2));

  symbols[47] = ((-4*a_2*M*cos_y2*sin_y2_3)/y[1] - 2*y1_2*cos_y2*sin_y2*(1 + (a_2*(y[1] + 2*M*sin_y2_2))/y1_3))/(2.*(y1_2 + a_2*cos_y2_2));

  symbols[49] = symbols[52] =
    -(a*(-56*y1_5*M + hair_2*(70*y1_2 + 120*y[1]*M + 189*M_2))*((-2*M)/y1_2 + (6*a_2*M*cos_y2_2)/y1_4)*sin_y2_2)/(56.*y1_6*(-(a_2*h*sin_y2_4)/(784.*y1_12) + y1_2*(-1 + (2*M)/y[1] - (2*a_2*M*cos_y2_2)/y1_3)*sin_y2_2*(1 + (a_2*(y[1] + 2*M*sin_y2_2))/y1_3))) + ((-1 + (2*M)/y[1] - (2*a_2*M*cos_y2_2)/y1_3)*((a*(-280*y1_4*M + hair_2*(140*y[1] + 120*M))*sin_y2_2)/(28.*y1_6) - (3*a*(-56*y1_5*M + hair_2*(70*y1_2 + 120*y[1]*M + 189*M_2))*sin_y2_2)/(14.*y1_7)))/(2.*(-(a_2*h*sin_y2_4)/(784.*y1_12) + y1_2*(-1 + (2*M)/y[1] - (2*a_2*M*cos_y2_2)/y1_3)*sin_y2_2*(1 + (a_2*(y[1] + 2*M*sin_y2_2))/y1_3)));

  symbols[50] = symbols[56] =
    (a*(-56*y1_5*M + hair_2*(70*y1_2 + 120*y[1]*M + 189*M_2))*cos_y2*(-1 + (2*M)/y[1] - (2*a_2*M*cos_y2_2)/y1_3)*sin_y2)/(28.*y1_6*(-(a_2*h*sin_y2_4)/(784.*y1_12) + y1_2*(-1 + (2*M)/y[1] - (2*a_2*M*cos_y2_2)/y1_3)*sin_y2_2*(1 + (a_2*(y[1] + 2*M*sin_y2_2))/y1_3))) - (a_3*M*(-56*y1_5*M + hair_2*(70*y1_2 + 120*y[1]*M + 189*M_2))*cos_y2*sin_y2_3)/(14.*y1_9*(-(a_2*h*sin_y2_4)/(784.*y1_12) + y1_2*(-1 + (2*M)/y[1] - (2*a_2*M*cos_y2_2)/y1_3)*sin_y2_2*(1 + (a_2*(y[1] + 2*M*sin_y2_2))/y1_3)));

  symbols[55] = symbols[61] =
    -(a*(-56*y1_5*M + hair_2*(70*y1_2 + 120*y[1]*M + 189*M_2))*sin_y2_2*((a*(-280*y1_4*M + hair_2*(140*y[1] + 120*M))*sin_y2_2)/(28.*y1_6) - (3*a*(-56*y1_5*M + hair_2*(70*y1_2 + 120*y[1]*M + 189*M_2))*sin_y2_2)/(14.*y1_7)))/(56.*y1_6*(-(a_2*h*sin_y2_4)/(784.*y1_12) + y1_2*(-1 + (2*M)/y[1] - (2*a_2*M*cos_y2_2)/y1_3)*sin_y2_2*(1 + (a_2*(y[1] + 2*M*sin_y2_2))/y1_3))) + ((-1 + (2*M)/y[1] - (2*a_2*M*cos_y2_2)/y1_3)*(y1_2*sin_y2_2*(a_2/y1_3 - (3*a_2*(y[1] + 2*M*sin_y2_2))/y1_4) + 2*y[1]*sin_y2_2*(1 + (a_2*(y[1] + 2*M*sin_y2_2))/y1_3)))/(2.*(-(a_2*h*sin_y2_4)/(784.*y1_12) + y1_2*(-1 + (2*M)/y[1] - (2*a_2*M*cos_y2_2)/y1_3)*sin_y2_2*(1 + (a_2*(y[1] + 2*M*sin_y2_2))/y1_3)));

  symbols[59] = symbols[62] =
    -(a_2*h*cos_y2*sin_y2_3)/(784.*y1_12*(-(a_2*h*sin_y2_4)/(784.*y1_12) + y1_2*(-1 + (2*M)/y[1] - (2*a_2*M*cos_y2_2)/y1_3)*sin_y2_2*(1 + (a_2*(y[1] + 2*M*sin_y2_2))/y1_3))) + ((-1 + (2*M)/y[1] - (2*a_2*M*cos_y2_2)/y1_3)*((4*a_2*M*cos_y2*sin_y2_3)/y[1] + 2*y1_2*cos_y2*sin_y2*(1 + (a_2*(y[1] + 2*M*sin_y2_2))/y1_3)))/(2.*(-(a_2*h*sin_y2_4)/(784.*y1_12) + y1_2*(-1 + (2*M)/y[1] - (2*a_2*M*cos_y2_2)/y1_3)*sin_y2_2*(1 + (a_2*(y[1] + 2*M*sin_y2_2))/y1_3)));
}


void PaniMetric::CalculateMetricElements(double M, double a, double hair, const double *y, std::vector<double> &metric)
{
  const double a_2 = sqr(a);
  const double M_2 = sqr(M);
  const double hair_2 = sqr(hair);
  const double y1_2 = sqr(y[1]);
  const double y1_3 = y1_2*y[1];
  const double y1_5 = y1_3*y1_2;
  const double y1_6 = sqr(y1_3);
  const double cos_y2 = cos(y[2]);
  const double cos_y2_2 = sqr(cos_y2);
  const double sin_y2_2 = 1 - cos_y2_2;

  metric[0] = -1 + (2*M)/y[1] - (2*a_2*M*cos_y2_2)/y1_3;
  metric[3] = (a*(-56*y1_5*M + hair_2*(70*y1_2 + 120*y[1]*M + 189*M_2))*sin_y2_2)/(28.*y1_6);
  metric[5] = 1/(1 - (2*M)/y[1] + (a_2*(y[1] + (-y[1] + 2*M)*cos_y2_2))/y1_3);
  metric[10] = y1_2 + a_2*cos_y2_2;
  metric[12] = (a*(-56*y1_5*M + hair_2*(70*y1_2 + 120*y[1]*M + 189*M_2))*sin_y2_2)/(28.*y1_6);
  metric[15] = y1_2*sin_y2_2*(1 + (a_2*(y[1] + 2*M*sin_y2_2))/y1_3);
}


void PaniMetric::CalculateMetricElements3(double M, double a, double hair, const double *y, std::vector<double> &metric)
{
  const double a_2 = sqr(a);
  const double M_2 = sqr(M);
  const double hair_2 = sqr(hair);
  const double y1_2 = sqr(y[1]);
  const double y1_3 = y1_2*y[1];
  const double y1_5 = y1_3*y1_2;
  const double y1_6 = sqr(y1_3);
  const double cos_y2 = cos(y[2]);
  const double cos_y2_2 = sqr(cos_y2);
  const double sin_y2_2 = 1 - cos_y2_2;

  metric[0] = -1 + (2*M)/y[1] - (2*a_2*M*cos_y2_2)/y1_3;
  metric[3] = (a*(-56*y1_5*M + hair_2*(70*y1_2 + 120*y[1]*M + 189*M_2))*sin_y2_2)/(28.*y1_6);
  metric[12] = (a*(-56*y1_5*M + hair_2*(70*y1_2 + 120*y[1]*M + 189*M_2))*sin_y2_2)/(28.*y1_6);
  metric[15] = y1_2*sin_y2_2*(1 + (a_2*(y[1] + 2*M*sin_y2_2))/y1_3);
}
