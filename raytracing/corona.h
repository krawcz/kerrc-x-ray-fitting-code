//
//  corona.h
//  
//
//  Created by Henric Krawczynski on 7/16/20.
//
#ifndef corona_h
#define corona_h

class Corona
{
        
    // corona parameters
    int config;
    double a,geometry,LoM,tempC,thetaC,dtheta,tauC,rC,rC2,hard,r1,minHeight,zCorona,xCorona;
    std::string fname;

    // careful: these are calculate in dtau, but also used in scatter
    double emunu[4][4];
    double enumu[4][4];

    bool initFlag,activeFlag,debugFlag;

    void    BZ_emunu(double *y ,double emunu[4][4], double enumu[4][4]);
    void    Lorentz (double gamma_e, double beta_e, double *n ,double LAMBDA[4][4], double LAMBDAi[4][4]);
    double Gamma_MB(double T);
    double RootFinder(double THETA,double lambda0);
    int zipf_sample ( double a, int &seed );
    double gamma_sample ( double a, double b, double c, int &seed );
    double r8_uniform_01 ( int &seed );
    double exponential_01_sample ( int &seed );
    double normal_01_sample ( int &seed );
    double r8_abs ( double x );
    double r8_sign ( double x );
    long seed;

    
public:
    Corona() {initFlag=activeFlag=false;}
    void init(std::string id);

    bool initialized() {return initFlag;};
    bool active() {return activeFlag;};

    void setR1(double x){r1=x;};

    double getConfig(){return config;};
    double getHard(){return hard;};
    double getLoM(){return LoM;};
    double getTauC(){return tauC;};
    double maxDtau();

    bool inCorona(double *y);
    bool inRegion(double *y);
    double  height  (double r, double theta);
    double  yheight  (double r, double theta);
    double  xheight  (double r, double theta);

    void loadEmunu(double *y);
    void loadEmunu(double *y, double *yn);
    double dtau(double *yp, double *y);

    double correct(double r, double t,double rn, double tn);

    double planck_sample ( double a, double b, int &seed );

    bool scatter(long int total,Photon *gamma_p, double u[4], double f_corona[4], double fac);

    void debug(bool flag) {debugFlag=flag;};
};

#endif /* corona_h */
