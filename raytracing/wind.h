#define NO_debugWind
// typedef vector<vector<double>> Matrix;

typedef std::vector<double> Metric;

#define D 4
typedef double Matrix[D][D];
typedef double Vector[D];

enum Cone   : int {a=0,x1,x2,n,x0,z0,gInf,sProb,xoff,g0,max};
enum Linear : int {la=0,lx1,lx2,alpha,slope,lz0,lgInf,lsProb,lxoff,lg0,lmax};

class Wind
{
  // hold current coordinates

  Vector blC; // "current" BL coordinates 
  double rC,tC; // current r and theta coordinates (assigned in load_g)
  double xC; // current x-coordinate of wind: xC = r*sin(theta) 
  double zC; // current z-coordinate of wind.
  double Ll; // angular momentum of wind when it is launched. 

  // hold metric at one point

  Metric gC;

  // define tetrad
  Matrix tetrad;

  // define model
  int model_ID;
  std::vector<double> parms;

  Disk *d_p;
  bool initFlag;
  // input parameters

  /*
    cone:

    arguments to be passed to creator:

enum Cone : int {a=0,x1,x2,n,x0,z0,gInf,sProb,xoff,g0,max};
    parms[Cone::a]: black hole spin a
    parms[Cone::x1]: structure parameter start x1 = ISCO[a]
    parms[Cone::x2]: sturcture parameter end   x2 = 100.;
    parms[Cone::n]: power law index of cone n = 2;
    parms[Cone::x0]: scaling parameter for xc-extension x0 = 1.;
    parms[Cone::z0]: scaling parameter for zc-extension z0 = 10.;
    parms[Cone::gInf]: Lorentz factor at infinity, e.g. 20.;
    parms[Cone::sProb]: scattering probability, e.g. 0.01

    calculated paramaters:
    parms[Cone::xoff]: xoff
    parms[Cone::g0]: Lorentz factor at zc=0

    linear:
    parms[Linear::alpha]: angle alpha from z-axis in degree
    parms[Linear::slope]: slope to get the angle alpha
  */
  
  double dot(Vector u,Vector v);
  void normalize(Vector u);

  void add(Vector u,Vector v,double scale);
  void copy(Vector u,Vector v);

  // angular frequency of ZAMOs
  double omega(std::vector<double> g);

  // minimum gamma-factor for any ZAMO = gamma g-factor for stationary ZAMO
  double gMin(double x);
  double gammaMin;
  bool debug;

public:
    Wind() {initFlag=false;}
    void init(std::string model,std::vector<double> &p,Disk *disk_p);
    bool active(){return initFlag;};
    
  double xc(double r, double t) { return sin(t)*r;};
  double zc(double xc); 
  double rc(double xc); 
  double tc(double xc);
  double gamma(double zc);
  double gammaN(double zc);
  double sProb() {return parms[Cone::sProb];};
  bool out(double r,double t) {return ((xc(r,t)<parms[Cone::x1])||(xc(r,t)>parms[Cone::x2]));}; // valid range?


  void load_g(double r, double t);
  void load_g(double xc) { load_g(rc(xc),tc(xc)); };

  double square(Vector u);
  double BLr() { return blC[1];};
  double BLt() { return blC[2];};
  double g00() { return gC[make_metric_index(0, 0)]; };
  double g03() { return gC[make_metric_index(0, 3)]; };
  double g11() { return gC[make_metric_index(1, 1)]; };
  double g22() { return gC[make_metric_index(2, 2)]; };
  double g33() { return gC[make_metric_index(3, 3)]; };

  void uWind(Vector u);
  void define_tetrad();
  void printG();
  void printTetrad();

  void debug_on() {debug=true;};
  void debug_off() {debug=false;};

  void Tetrad2BL(Vector tCoord,Vector sum);
  void BL2Tetrad(Vector bl,Vector sum);

  bool hit(double r,double t);
  double correct(double r1,double t1,double r2,double t2);
};

