// histogram class
// Henric Krawczynski 6/22/2020


// C++
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
// #include <iomanip>   // format manipulation

// C
#include <stdio.h>

class Hist {    
  int n;
  double x1,x2,delta;
  vector<double>cont;
  vector<double>contSq; // add for error calculation
  int Num;
  bool error;

 public:
  Hist(int In,double Ix1, double Ix2,string opt);

  int bins() {return n; };

  int index(double x);
  
  // modify contents
  void fill(double x,double w);

  double set(int k,double val) {cont[1+k]=val; return val;};
  double get(int k) {return cont[1+k]; };
  double get(double x) {return cont[index(x)]; };

  double setRaw(int k,double val) {cont[k]=val; return val;};
  double getRaw(int k) {return cont[k]; };
  double getRawSq(int k) {return contSq[k]; };
  int entries() {return Num;};

  // math
  void add(Hist *h);
  void scale(double fac);

  // output
  bool print(string title);
  bool write(string fName,string id);
};

// creator
Hist::Hist(int In,double Ix1, double Ix2,string opt="")
{
  Num=0;
  n=In;
  x1=Ix1;
  x2=Ix2;
  delta=(x2-x1)/(double)n;

  cont.assign(n+2,0.);
  error=(opt.compare("E")==0);
  if (error)
    contSq.assign(n+2,0.);
}

// get index for value x
int Hist::index(double x)
{
  if (x<x1) return 0;
  if (x>=x2) return n+1;
  return 1+(int)((x-x1)/delta);
}

//  fill entry
void Hist::fill(double x,double w=1.)
{
  int k=index(x);
  cont[k]+=w;
  Num++;
  if (error)
    contSq[k]+=w*w;
}

// add second histogram
void Hist::add(Hist *h)
{
  for (int k=0;k<n+2;k++)
    {
      cont[k]+=h->getRaw(k);
      if (error)
	contSq[k]+=h->getRawSq(k);
    }
  Num+=h->entries();
}

// scale histogram contents
void Hist::scale(double fac)
{
  for (int k=0;k<n+2;k++)
    cont[k]*=fac;
}

// print histogram contents to screen
bool Hist::print(string title="") 
{
  cout<<"Histogram "<<title<<", x-axis: "<<x1<<" - "<<x2<<" , delta = "<<delta<<endl;
  cout<<"U "<<cont[0]<<endl;

  for (int k=1;k<n+1;k++)
    if (!error)
      cout <<k<<" "<<x1+((double)k-0.5)*delta<<" "<<cont[k]<<endl;
    else
      cout <<k<<" "<<x1+((double)k-0.5)*delta<<" "<<cont[k]<<" "<<sqrt(contSq[k])<<endl;

  cout<<"O "<<cont[n+1]<<endl;
  return true;
}

// write histogram contents to file
bool Hist::write(string fName,string id)
{
  ofstream out;
  out.open(fName, fstream::out | fstream::app);

  if ( !out.is_open()) {cout << "couldn't open output file "<<fName << endl; return false;}
  out <<id<<" ";
  for (int k=0; k<n; k++) 
    if (!error)
      out <<cont[1+k]<<" ";
    else
      out <<cont[1+k]<<" "<<sqrt(contSq[1+k])<<" ";

  out<<endl;
  out.close();

  cout <<"Wrote to file "<<fName<<endl;
  return true;
}


