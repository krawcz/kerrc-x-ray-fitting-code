# kerrC raytracing and fitting code

three directories:
- raytracing: do the raytracing
needs hdf5: https://www.hdfgroup.org/solutions/hdf5/

- quickstart: C++ python module
needs Boost environment https://www.boost.org

- sherpana: fitting inside Sherpa
needs Sherpa: https://cxc.cfa.harvard.edu/sherpa/

First compile the code in raytracing by typing 
      make

Run the code with a statement of the type:
    ./xTrack -d -D -K -n 2000 -r -10 -c 0000000 -f outdir 
The options are explained in the code xTrack.cpp.

One suitable raytracing output has been generated, you need to compile a python module that the X-ray fitting code Sherpa can load.

For this download Boost, and move the directory quickstart into the directory:
boost_1_76_0/libs/python/example/quickstart

Compile the code with ./comp in this directory.
Copy the python module (dataReader.so) into the directory sherpana.

Launch sherpa, and start fitting data by running run -i cPl.py.