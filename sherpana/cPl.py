import importlib
import numpy as np
import os
import sys

# import polarization simulator
    
from simPol import *
    
print('Number of arguments:', len(sys.argv), 'arguments.')
numA=len(sys.argv)

if (numA==1):
    print('interactive mode')
else:
    print('batch mode')
    import matplotlib as mpl
    mpl.use('Agg')

import matplotlib.pyplot as pl
import math as ma
import scipy as sc
import sherpa.astro.ui as ui 
import astropy.io.fits as af

from datetime import datetime

def pTime():
    # datetime object containing current date and time
    now = datetime.now()
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
    return dt_string

print('time: ',pTime())

import plotPol as pp
importlib.reload(pp)

np.random.seed(1000)    

# load polarization model
import kHdf5 as kc
importlib.reload(kc)

# pol   = kc.kerrcModel()
polar = kc.kerrModel()

pl.rcParams['font.size'] = 20
pl.rcParams['legend.fontsize'] = 'small'
pl.rcParams['figure.titlesize'] = 'medium'
#.        0.     1.     2.    3.      4.     5.       6.   7.     8.     9.     10.   11.    12.    13.    14.   
#pNames = ["a"   ,"geom","M"  ,"tempC","tauC","thetaC","rC","incl","Mdot","dist","chi","refl","AFe", "kTe", "logDens"]
#pMin   = [-1.   ,0.    ,0.   ,1.     ,0.    ,      5., 25.,    5.,    0.,    0., -150,    0.,  0.5,    1.,15.]
#pMax   = [0.998 ,1.    ,100. ,100.   ,2.    ,     85.,100.,   85., 1e+30, 1e+30, 150.,    2.,  10.,  400.,23.01]

pNames = ["a"   ,"geom","M"  ,"tempC","tauC","thetaC","rC","incl","Mdot","dist","chi","refl","AFe", "kTe", "logDens","eFit","alpha","l1","l2"]
pMin   = [0.5   ,0.    ,0.   ,1.     ,0.    ,      5., 25.,    5.,    0.,    0., -150,    0.,  -1.01,    1.,15.,0.1,-5.,1.,1.]
pMax   = [0.998 ,1.    ,100. ,500.   ,2.    ,     85.,100.,   85., 1e+30, 1e+30, 150.,    100.,  10.,  400.,23.01,10.,5.,100.,100.]

pUnit  = ['none','none','Solar Masses','KeV','none','degree','rg','degree','10^18 g s-1','kpc','degree','none','solar abundances','keV','log10(1/cm^3)','none','none','rg','rg']

a       = np.array([-1., 0., 0.5, 0.75, 0.9, 0.95, 0.98, 0.99, 0.998])

load_user_model(polar.myi, "mI")
add_user_pars("mI",pNames,parvals=pMin,parmins=pMin,parmaxs=pMax,parunits=pUnit)

load_user_model(polar.myq, "mQ")
add_user_pars("mQ",pNames,parvals=pMin,parmins=pMin,parmaxs=pMax,parunits=pUnit)

load_user_model(polar.myu, "mU")
add_user_pars("mU",pNames,parvals=pMin,parmins=pMin,parmaxs=pMax,parunits=pUnit)

# set models for data sets 1-3

set_source(1,mI)
#set_source(2,mQ)
#set_source(3,mU)
freeze(mI)
freeze(mQ) 
freeze(mU) 

# simulated values
# a        = np.array([-1., 0., 0.5, 0.75, 0.9, 0.95, 0.98, 0.99, 0.998])
# geometry = np.array([0,1])
# LoM      = np.array([0.25, 0.5, 1, 2.])
# tempC    = np.array([5.,10.,25.,100.,250.,500.])
# tauC     = np.array([0.,0.125,0.25,0.5,0.75,1.,2.])
# thetaA  = np.array([5.,45.,85.])
# thetaB  = np.array([5.,25.,56.])
# thetaC  = np.vstack((thetaA,thetaB))
# rA = np.array([25,50.,100.])
# rB = np.array([2.5,10.,50.])
# rC = np.vstack((rA,rB))
# rC2 = np.array([20.,50.,100.])
# incl = np.linspace(5,85,9)

kc.tie_pars(mI,mQ)
kc.tie_pars(mI,mU)

# laod nicer

nicer="/data/krawcz/ixpe/cygx1/NICER/220519/"
narf=nicer+"cygx1_kgfilt.arf"
nrmf=nicer+"cygx1_kgfilt.rmf"
ui.load_pha(1,nicer+"cygx1_kgfilt.pha")
ui.load_arf(1,nicer+"cygx1_kgfilt.arf")
ui.load_rmf(1,nicer+"cygx1_kgfilt.rmf")
ui.load_bkg(1,nicer+"back_cygx1_kgfilt.pha")
ui.subtract(1)
ui.ignore_id(1,None,2.)
ui.ignore_id(1,12.,None)

nustar="/data/krawcz/ixpe/cygx1/NuSTAR/products/"

arf=nustar+"nu30702017002A01_sr.arf"
rmf=nustar+"nu30702017002A01_sr.rmf"
ui.load_pha(2,nustar+"nu30702017002A01_sr.pha")
ui.load_arf(2,nustar+"nu30702017002A01_sr.arf")
ui.load_rmf(2,nustar+"nu30702017002A01_sr.rmf")
ui.load_bkg(2,nustar+"nu30702017002A01_bk.pha")
ui.subtract(2)
ui.ignore_id(2,None,3.)
ui.ignore_id(2,75.,None)

ui.load_pha(3,nustar+"nu30702017002B01_sr.pha")
ui.load_arf(3,nustar+"nu30702017002B01_sr.arf")
ui.load_rmf(3,nustar+"nu30702017002B01_sr.rmf")
ui.load_bkg(3,nustar+"nu30702017002B01_bk.pha")
ui.subtract(3)
ui.ignore_id(3,None,3.)
ui.ignore_id(3,75.,None)

group_bins(1,200)
group_bins(2,200)
group_bins(3,200) 
ui.plot_data(1,ylog=True,xlog=True)
ui.plot_data(2,ylog=True,xlog=True,overplot=True)
ui.plot_data(3,ylog=True,xlog=True,overplot=True)
pl.show()

def pA(path,r):
    """
    plot the current model, and save the results to a files in the specified path 
    """
    get_data(1).name=''
    get_data(2).name=''
    get_data(3).name=''
    plot_fit_ratio(3,ylog=True,xlog=True)
    plot_fit_ratio(1,ylog=True,xlog=True,overplot=True)
    plot_fit_ratio(2,ylog=True,xlog=True,overplot=True)
    pl.show()
    plot_fit_ratio(3,ylog=True,xlog=True)
    plot_fit_ratio(1,ylog=True,xlog=True,overplot=True)
    plot_fit_ratio(2,ylog=True,xlog=True,overplot=True)
    pl.tight_layout()
    pl.savefig(path+r+'.eps')
    pp.plotPolModel(3,4,5,50)
    pl.show()
    pl.savefig(path+r+'-pol.eps')
    show_model(outfile=path+r+'.mdl',clobber=True)
    show_fit(outfile=path+r+'.fit',clobber=True)
    save_all(path+r+'.sherpa',clobber=True)
    print('>>saved to ',path+r,'\n\n')

def clean():
    """
    Let's try to produce a clean figure
    """
    fig=pl.gcf()
    ax=pl.gca()
    fig.suptitle('')
    ax.set_title('')
    pl.tight_layout()

get_data(1).name=''
get_data(2).name=''
get_data(3).name=''

# nH absorption
set_xsxsect('vern')
set_xsabund('wilm')

#create_model_component("xstbabs", "mabs")
create_model_component("xsphabs", "mabs1")
ui.freeze(mabs1)

create_model_component("scale1d", "cA")
ui.freeze(cA)
create_model_component("scale1d", "cB")
ui.freeze(cB)

# here are our best models
def ip0():
    mabs1.nH.val     = 0.2 # 0.8
    mI.a.val     = 0.9
    mI.geom.val     = 0.0
    mI.M.val     = 21.2
    mI.tempC.val     = 100. #250
    mI.tauC.val     = 0.35 # 0.122
    mI.thetaC.val     = 10. # 56.662899833844364
    mI.rC.val     = 100.
    mI.incl.val     = 75. # 65. 
    mI.Mdot.val     =  0.0505 # 0.075 0.0505
    mI.dist.val     = 2.22
    mI.chi.val     = 0.0
    mI.refl.val     = 1.0
    mI.AFe.val     = 1.0
    mI.logDens.val     = 17.5
    mI.eFit.val     = 1.5
    mI.alpha.val     = 0.0
    mI.l1.val     = 1.0
    mI.l2.val     = 100.0
    cB.c0.val     = 1.01
    cA.c0.val     = 1.0
# 6/7
#    mabs1.nH.val     = 3.0169685880453376
#    mI.a.val     = 0.5
#    mI.geom.val     = 0.0
#    mI.M.val     = 21.2
#    mI.tempC.val     = 123.7320808151728
#    mI.tauC.val     = 0.13906760102866075
#    mI.thetaC.val     = 10. # 56.662899833844364
#    mI.rC.val     = 32.28944563229033
#    mI.incl.val     = 65. # 74.333994661202
#    mI.Mdot.val     = 0.36
#    mI.dist.val     = 2.22
#    mI.chi.val     = 0.0
#    mI.refl.val     = 1.0
#    mI.AFe.val     = 1.0
#    mI.kTe.val     = 250.35695557184175
#    mI.logDens.val     = 19.28199107927716
#    mI.eFit.val     = 1.5
#    mI.alpha.val     = 0.0
#    mI.l1.val     = 1.0
#    mI.l2.val     = 100.0
#    cB.c0.val     = 1.01
#    cA.c0.val     = 1.0
# 6/6
#    mabs1.nH.val     = 0.3620079678867783
#    mI.a.val     = 0.5650037391999553
#    mI.geom.val     = 0.0
#    mI.M.val     = 21.2
#    mI.tempC.val     = 246.02964385046747
#    mI.tauC.val     = 0.12307873518943171
#    mI.thetaC.val     = 10.
#    mI.rC.val     = 99.40122502954597
#    mI.incl.val     = 65.
#    mI.Mdot.val     = 0.15
#    mI.dist.val     = 2.22
#    mI.chi.val     = 0.0
#    mI.refl.val     = 1.0
#    mI.AFe.val     = 1.0
#    mI.kTe.val     = 257.15675769967197
#    mI.logDens.val     = 17.91058722396156
#    mI.eFit.val     = 1.5
#   mI.alpha.val     = 0.0
#    mI.l1.val     = 1.0
#    mI.l2.val     = 100.0
#    cB.c0.val     = 1.01
#    cA.c0.val     = 1.0    
# even older
#    mabs1.nH.val     = 0.3620079678867783
#    mI.a.val     = 0.9499837137805024
#    mI.geom.val     = 0.0
#    mI.M.val     = 21.2
#    mI.tempC.val     = 250.0
#    mI.tauC.val     = 0.12570551636886587
#    mI.thetaC.val     = 56.888906977515035
#    mI.rC.val     = 100.0
#    mI.incl.val     = 65.0
#    mI.Mdot.val     = 0.05008066677800251
#    mI.dist.val     = 2.22
#    mI.chi.val     = 0.0
#    mI.refl.val     = 1.0
#    mI.AFe.val     = 1.0
#    mI.kTe.val     = 250.0
#    mI.logDens.val     = 19.85118481022592
#    mI.eFit.val     = 1.5
#    mI.alpha.val     = 0.0
#    mI.l1.val     = 1.0
#    mI.l2.val     = 100.0
#    cB.c0.val     = 1.01
#    cA.c0.val     = 1.0
    mI.kTe     = mI.tempC

def ip1():
    mabs1.nH.val     = 4.0089 
    mI.a.val     = 0.89981
    mI.geom.val     = 1.0
    mI.M.val     = 21.2
    mI.tempC.val     = 150.0
    mI.tauC.val     = 0.790102
    mI.thetaC.val     = 45.
    mI.rC.val     = 25.0
    mI.incl.val     = 75. #85.0
    mI.Mdot.val     = 0.1
    mI.dist.val     = 2.22
    mI.chi.val     = 0.0
    mI.refl.val     = 1.0
    mI.AFe.val     = 1.0
    mI.logDens.val     = 17.7278  
    mI.eFit.val     = 1.5
    mI.alpha.val     = 0.0
    mI.l1.val     = 1.0
    mI.l2.val     = 100.0
    cB.c0.val     = 1.01
    cA.c0.val     = 1.0
    mI.kTe     = mI.tempC
    
def ip():
    print(mI.geom.val)
    if (int(mI.geom.val)==0):
        print("Geometry 0")
        ip0()
    else:
        ip1()
        print("Geometry 1")

# main analysis

set_source(1,mabs1*(mI))
set_source(2,cA*mabs1*(mI))
set_source(3,cB*mabs1*(mI))

#create_model_component("xsgaussian", "gauss")
#gauss.LineE.val=6.4
#gauss.Sigma.val=0.01
#gauss.norm.val=0.002

#set_source(1,mabs1*(mI+gauss))
#set_source(2,fpm*mabs1*(mI+gauss))
#set_source(3,mco*mabs1*(mI+gauss))

# hhhhh

# set geometry:
# 0 wedge
# 1 (fan)

mI.geom.val=0
ip()

# do analysis:
# 0 standard fit
# 1 fit all parameters of interest
# 2 systematically run through all configurations
# 5 show KERRB comparisons
# 6 plot Javier's results 
# 7 pol predictions 
# 8 dataWalk
# 9 do nothing

num=9

# select which data to fit
# 0 all
# 1 NuSTAR only
# 2&3 refit

fitVal=0

print("geom ",mI.geom.val)
print("num  ",num)
print("fitVal ",fitVal)

# select fitting method

method="levmar"
# method="neldermead"
set_method_opt('ftol',1e-4)

print("method ",method)

path='/data/krawcz/sherpana/'+'d'+str(int(mI.geom.val))+str(num)+str(fitVal)+'/'

print("path  ",path)
try:
    os.mkdir(path)
except:
    print('directory already exists')

# mI.logDens.val=16.

print("Current model parameters:")
print(cA)
print(cB)
print(mabs1)
print(mI)

#pl.figure("NICER and NuSTAR")
plot_fit_ratio(2,ylog=True,xlog=True,overplot=False);plot_fit_ratio(3,ylog=True,xlog=True,overplot=True);pl.show()
pl.tight_layout()
pl.show()
pl.tight_layout()
pl.savefig(path+'polModelSpec.eps')

# we want to know kerrC's model
#set_source(3,mabs1*mI)
#set_source(4,mabs1*mQ)
#set_source(5,mabs1*mU)
set_source(3,mI)
set_source(4,mQ)
set_source(5,mU)

tObs=300000
fake_pha(4,arf,rmf,tObs)
fake_pha(5,arf,rmf,tObs)

pp.plotPolModel(3,4,5,2)
pl.tight_layout()
pl.show()
pl.tight_layout()
pl.savefig(path+'polModel.eps')

# new stuff
if (True):
    plot_fit_ratio(1,ylog=True,xlog=True,overplot=False);pl.show()
    set_source(1,mI)
    set_source(7,mQ)
    set_source(8,mU)
    fake_pha(7,narf,nrmf,tObs)
    fake_pha(8,narf,nrmf,tObs)

    pp.plotPolModel(1,7,8,3)
    pl.tight_layout()
    pl.show()
    pl.tight_layout()
    pl.savefig(path+'polModel2.eps')

    os.system("cat test1.out")
    print("")
    os.system("cat test3.out")

set_stat("leastsq")
ui.thaw(mabs1.nH)
ui.thaw(mI.Mdot)
ui.thaw(mI.tauC)
# ui.fit(2,3)

def myFit():    
    parms=['mabs1.nH','mI.a','mI.thetaC','mI.rC','mI.tempC','mI.tauC','mI.incl'] 
    numPar=4
    ui.calc_stat_info()
    # do the actual fitting
    for i in range(5000):
        print('Fitting method ',method)
        set_method(method)
#        if (i%500==499):
#            set_method('moncar')

        ui.freeze(mabs1)
        ui.freeze(mI)
        ui.freeze(cA)
        ui.freeze(cB)
        ui.thaw(mI.Mdot)
        print('thawing Mdot')

        repl=np.random.choice(parms,numPar,replace=False)
        for x in repl:
            ui.thaw(x)
        print('thawed ',repl)

        print(cA)
        print(cB)
        print(mabs1)
        print(mI)

        ui.fit(2,3)
        pA(path,str(i))

if (num==0):
    myFit()

if (num>8):
    sys.exit()

myCounter=0
minChi=calc_stat(2,3)
print('starting chi2 ',minChi)
# pNames = ["a"   ,"geom","M"  ,"tempC","tauC","thetaC","rC","incl","Mdot","dist","chi","refl","AFe", "kTe" "logDens","eFit","alpha","l1","l2"]

nParms=np.array(pNames)
nMin=np.array(pMin)
nMax=np.array(pMax)
flag=np.array([True,False,False,True,True,True,True,True,False,False,False,False,False,False,True,False,False,False,False])
delta=np.array([0.1,0.,0.,5.,0.025,3.,10.,3.,0.,0.,0.,0.,0.,0.,0.3,0.,0.,0.,0.])

fParms=nParms[flag]
fMax=nMax[flag]
fMin=nMin[flag]
print("Fitting ",fParms)
myDelta = np.dstack((pNames,delta))
print("delta ",myDelta)


fitParms=[]
for x in pNames:
    fitParms.append("mI."+x)
print(fitParms)

def myFit3(n):   
    global myCounter,path
    parms=['mabs1.nH','mI.a','mI.thetaC','mI.rC','mI.tempC','mI.tauC','mI.incl'] 
    # do the actual fitting
    ui.freeze(mabs1)
    ui.freeze(mI)
    ui.freeze(cA)
    ui.freeze(cB)
    ui.thaw(mI.Mdot)
    print('thawing Mdot')

    if (n>0):
        repl=np.random.choice(parms,n,replace=False)
        for x in repl:
            ui.thaw(x)
            print('thawed ',repl)

    print(cA)
    print(cB)
    print(mabs1)
    print(mI)

    ui.fit(2,3)
#    myCounter+=1
#    pA(path,str(myCounter))

def getVar(handle,sigma):
    res=np.random.normal(handle.val, sigma)
    while (res<handle.min) or (res>handle.max):
        res=np.random.normal(handle.val, sigma)
    return res

def exploreChi(parm,flag,delta,sigma):
    global minChi,fitVal,myCounter,path
    save=np.zeros_like(parm,dtype=float)
    expl=np.zeros_like(parm,dtype=float)

    # assign random value within delta*sigma 
    for i,p in enumerate(parm):
        print(p)
        handle=eval(p)
        save[i]=handle.val
        if (flag[i]):
            expl[i]=getVar(handle,delta[i]*sigma)
        else:
            expl[i]=handle.val
        handle.val = expl[i]
        
    if (fitVal==0):
        for index in range(20):
            myFit3(3)
    else:
        myFit3(0)
    # calculate chi^2
    chi2= calc_stat(2,3)

    # save chi^2 and all parm values to file (in append mode)
    out=str(chi2)
    for x in expl:
        out+=' '+str(x)
    out+='\n'
    
    file = open(path+"ana.dat", "a+")  
    file.write(out)
    file.close()    
    
    if (chi2>minChi):
        for i,p in enumerate(parm):
            handle=eval(p)
            handle.val=save[i]
    else:
        print('bingo: ',minChi,' --> ',chi2)

        file = open(path+"best1.dat", "a+")  
        file.write(out)
        file.close()    

        out1=str(mI)+'\n'
        out2=str(mabs1)+'\n'
        out3=str(chi2)+'\n'

        file2 = open(path+"best2.dat", "a+")      
        file2.write(out1)
        file2.write(out2)
        file2.write(out3)
        file2.close()
        minChi=chi2
        
        myCounter+=1
        pA(path,str(myCounter))

    return chi2

def walk3(n):
    global minChi
    for i in range(n):
        chi2=exploreChi(fitParms,flag,delta,1.)
        if (i%100==0):
            print('iteration ',i,minChi)

if (num==8):
    walk3(1000000)

sys.exit()

