import numpy as np
import sherpa.astro.ui as ui 

def debugSim(*args): 
    if (False):
        print(*args) 
        
class quGenerator:
    """Generate N random q and u parameters for given Stokes i, q, u, and mu.
    Author: Henric Krawczynski, 8/9/2020
    """
    def __init__(self):
        n=1000
        # all angles from 0 to pi
        self.x  = np.linspace(0.,np.pi,n)

        # get Cumulative PDF
        half  = np.pi/(n-1)/2.
        xd    = 2.*(self.x-half)
        yd    = 1.+np.cos(xd)
        yd[0] = 0.
        self.y  = np.cumsum(yd)
        self.y /= self.y[-1]

    def get(self,i,q,u,mu,N):
        pol = np.sqrt(q**2+u**2)/i
        chi0= np.arctan2(u,q)/2
        unModulated=1.-mu*pol
        qVal=0.
        uVal=0.
        for k,ran in enumerate(np.random.random(N)):
            chi=0.
            if (ran<unModulated):
                chi = np.pi*np.random.random()
            else:
                chi=np.interp(np.random.random(),self.y,self.x)
            qVal+=np.cos(2.*(chi+chi0))
            uVal+=np.sin(2.*(chi+chi0))
            sigmaQ=i*np.sqrt((1/2-mu**2*pol**2/4*np.cos(2.*chi0)**2)/N)
            sigmaU=i*np.sqrt((1/2-mu**2*pol**2/4*np.sin(2.*chi0)**2)/N)
        return 2.*qVal, 2.*uVal, 2.*sigmaQ, 2.*sigmaU

    def get2(self,N,i,q,u,mu):
        getX = lambda y: (y**2-1+np.sqrt(y**2-y**4))/(2*y**2-1)
        pol = np.sqrt(q**2+u**2)/i
        chi0= np.arctan2(u,q)/2

        # sigmas and Pearson correlation coefficient of q and u
        sigmaQ= 2.*np.sqrt(N*(1/2-mu**2*pol**2/4*np.cos(2.*chi0)**2))/mu
        sigmaU= 2.*np.sqrt(N*(1/2-mu**2*pol**2/4*np.sin(2.*chi0)**2))/mu
        pcc   = mu**2 * pol**2 *np.sin(4.*chi0)/ \
            np.sqrt(16-8*mu**2*pol**2+mu**4*pol**4*np.sin(4.*chi0)**2)

        # get random numbers a and c with mean=0, sigma=1, and pcc 
        a= np.random.normal()
        b= np.random.normal()
        x = getX(pcc)
        f = np.sqrt( (1-x)**2 + x**2 )
        c = ( -(1-x)*a + x*b ) / f

        qVal = q+sigmaQ*a
        uVal = u+sigmaU*c
        return qVal, uVal, sigmaQ, sigmaU
    
class simPol:
    ''' create simulated polarization data set 
        Author: Henric Krawczynski, 8/10/2020
    '''
    def __init__(self,arf,mrf):
        self.quGen = quGenerator()
        # are the two arf arrays compatible?
        compatible1 = (arf.energ_lo==mrf.energ_lo)
        compatible2 = (arf.energ_hi==mrf.energ_hi)
        if (compatible1.all() and compatible2.all()):
            self.x = (arf.energ_lo+arf.energ_hi)/2.        
            x =np.maximum(arf.specresp,1e-7*np.ones_like(arf.specresp))
            self.mu = mrf.specresp/x
        else:
            print('The arf and mrf arrays are not compatible')
            print(arf.xlo)
            print(mrf.xlo)
    
    def fake(self,idI, idQ,idU,modI,modQ,modU):
        # get the simulated data
        di   =ui.get_data(idI)
        dq   =ui.get_data(idQ)
        du   =ui.get_data(idU)
        q    =np.zeros_like(dq.counts)
        u    =np.zeros_like(q)
        qSig =np.zeros_like(q)
        uSig =np.zeros_like(q)

        for k,i in enumerate(di.counts):
            if (i>0.):
                q[k],u[k],qSig[k],uSig[k] = self.quGen.get2(i,modI[k],modQ[k],modU[k],self.mu[k])
                if (k<2000) and (k%10==0):
                    debugSim(k,' i ',i,' mi ',modI[k],' mu ',self.mu[k], \
                          ' mQ ',modQ[k],' q ',q[k],' mU ',modU[k],' u ',u[k], \
                          ' modPol ',np.sqrt(modQ[k]**2+modU[k]**2)/modI[k], \
                          ' pol ',np.sqrt(q[k]**2+u[k]**2)/i,' qS ',qSig[k],' uS ',uSig[k])
        dq.counts=q
        du.counts=u
        dq.staterror=qSig
        du.staterror=uSig
