import numpy as np
import matplotlib.pyplot as pl
import sherpa.astro.ui as ui 
from matplotlib.ticker import ScalarFormatter,FuncFormatter
import sys

def rebinModel(x,y,n):
    oldN=len(x)
    if (len(y)!=oldN):
        print("Error: x and y lengths are not equal ",oldN," ",len(y))
        sys.exit()
    newN=int(oldN/n);
    if (newN*n!=oldN):
        newN+=1
    xx=np.zeros(newN)
    yy=np.zeros(newN)
    nn=np.zeros(newN)
    for i,myx in enumerate(x):
        newIndex=int(i/n)
        xx[newIndex]+=np.log(myx)
        yy[newIndex]+=y[i]
        nn[newIndex]+=1
    xx/=nn
    xx=np.exp(xx);
    yy/=nn
    return xx,yy
 
def rebinModelE(x,y,ye,n):
    oldN=len(x)
    if (len(y)!=oldN):
        print("Error: x and y lengths are not equal ",oldN," ",len(y))
        sys.exit()
    newN=int(oldN/n);
    if (newN*n!=oldN):
        newN+=1
    xx=np.zeros(newN)
    yy=np.zeros(newN)
    ee=np.zeros(newN)
    nn=np.zeros(newN)
    for i,myx in enumerate(x):
        newIndex=int(i/n)
        xx[newIndex]+=np.log(myx)
        yy[newIndex]+=y[i]
        ee[newIndex]+=ye[i]**2
        nn[newIndex]+=1
    xx/=nn
    xx=np.exp(xx);
    yy/=nn
    ee=np.sqrt(ee)/nn
    return xx,yy,ee

def rebinLog(x,y,n,flag=False):
    oldN=len(x)
    if (len(y)!=oldN):
        print("Error: x and y lengths are not equal ",oldN," ",len(y))
        sys.exit()
    newN=int(n*np.log10(oldN+1))+1;
    xx=np.zeros(newN)
    yy=np.zeros(newN)
    nn=np.zeros(newN)
    for i,myx in enumerate(x):
        newIndex=int(n*np.log10(i+1));
        xx[newIndex]+=np.log(myx)
        yy[newIndex]+=y[i]
        nn[newIndex]+=1
        if (flag):
            print(i,' ',newIndex,' ',y[i],' ',yy[newIndex],' ',nn[newIndex])
    xx/=nn
    xx=np.exp(xx);
    yy/=nn
    return xx,yy

def rebinLogE(x,y,ye,n,flag=False):
    oldN=len(x)
    if (len(y)!=oldN):
        print("Error: x and y lengths are not equal ",oldN," ",len(y))
        sys.exit()
    newN=int(n*np.log10(oldN+1))+1;
    xx=np.zeros(newN)
    yy=np.zeros(newN)
    ee=np.zeros(newN)
    nn=np.zeros(newN)
    for i,myx in enumerate(x):
        newIndex=int(n*np.log10(i+1));
        xx[newIndex]+=np.log(myx)
        yy[newIndex]+=y[i]
        ee[newIndex]+=ye[i]**2
        nn[newIndex]+=1
        if (flag):
            print(i,' ',newIndex,' ',y[i],' ',yy[newIndex])
    xx/=nn
    xx=np.exp(xx);
    yy/=nn
    ee2=np.sqrt(ee)
    ee3=ee2/nn
    return xx,yy,ee3

def statError(val,err):
    if (isinstance(err,np.ndarray)):
        return err
    return np.sqrt(np.abs(val))
        
def plusMinus(ene,val,err,lbl,myC):
    err=statError(val,err)
    sel=(val>0.)
    pl.errorbar(ene[sel],val[sel],err[sel],marker='o',c=myC,mfc=myC,mec=myC,ecolor=myC,ms=6,ls='',label=lbl)  
    sel=(val<0.)
    pl.errorbar(ene[sel],-val[sel],err[sel],marker='o',c=myC,mfc='white',mec=myC,ecolor=myC,ms=6,ls='')  

def groupSpec(ene,d):
    val=d.counts
    err=statError(val,d.staterror)
    grp=d.grouping
    if (isinstance(grp,np.ndarray)):
        x=[]
        y=[]
        ye=[]
        entries=[]
        for k,yVal in enumerate(val):
            if (grp[k]==1):
                x.append(0.)
                y.append(0.)
                ye.append(0.)
                entries.append(0.)
            x [-1]+=ene[k]
            y [-1]+=yVal
            ye[-1]+=err[k]**2
            entries[-1]+=1

        x=np.array(x)
        y=np.array(y)    
        ye=np.array(ye) 
        entries=np.array(entries)
        x=x/entries
        ye=np.sqrt(ye)
        return x,y,ye
    else:
        return ene,val,err


def plotPolData(idI,idQ,idU,p=False):
#ax.set_aspect('equal')
    i=ui.get_data(idI)
    q=ui.get_data(idQ)
    u=ui.get_data(idU)
    arf=ui.get_arf(idI)
    ene = (arf.energ_lo+arf.energ_hi)/2

    fig, ax = pl.subplots(figsize=(8, 5))  
    ax.set_xscale('log')
    ax.set_yscale('log')
    formatter = FuncFormatter(lambda y, _: '{:.4g}'.format(y))
    ax.xaxis.set_major_formatter(formatter)
    ax.yaxis.set_major_formatter(formatter)

    pl.ylabel('Counts')
    pl.xlabel('Energy [keV]')    

    eI,vI,errI = groupSpec(ene,i)
    eQ,vQ,errQ = groupSpec(ene,q)
    eU,vU,errU = groupSpec(ene,u)
    plusMinus(eI,vI,errI,'Stokes I','black')
    plusMinus(eQ,vQ,errQ,'Stokes Q','g')
    plusMinus(eU,vU,errU,'Stokes U','r')    
    if (p=='Y'):
        for k, x in enumerate(eI):
            if (p and (vI[k]>0.)):
                print('Energy {:>.3f} IQU {:>.3e} {:>.3e} {:>.3e} pol {:>.3f} '.format(x,vI[k],vQ[k],vU[k],100.*np.sqrt(vQ[k]**2+vU[k]**2)/vI[k]))
        
    ax.set(xlim=(0.9,np.max(eI)*1.1), ylim = (1,2*np.max(vI)))
    ax.legend()
    pl.grid(True)
    pl.show()
   
from matplotlib.ticker import FuncFormatter

#ax.axis([0.01, 10000, 1, 1000000])

def plotIQ(idI,idQ,idU,x,mu,rebinN):
    i=ui.get_data(idI).counts
    ie=ui.get_data(idI).staterror

    q=ui.get_data(idQ).counts
    qe=ui.get_data(idQ).staterror

    u=ui.get_data(idU).counts
    ue=ui.get_data(idU).staterror

    fac=i
    sq=q/fac
    su=u/fac
    sqe=qe/fac
    sue=ue/fac

    print("fac ",fac[0:10])
    print("q ",q[0:10])
    print("sq ",sq[0:10])

    rx,rq,rqe=rebinLogE(x,sq,sqe,8.)
    rx,ru,rue=rebinLogE(x,su,sue,8.)
    rx2=rx*1.01

    flag=np.logical_and((rqe<1.),(rue<1.))
    pl.errorbar(rx[flag], rq[flag],rqe[flag],mfc='orange',mec='orange',ecolor='orange',marker='o',ls='none',label='Stokes Q/I')
    pl.errorbar(rx2[flag], ru[flag],rue[flag],mfc='green',mec='green',ecolor='green',marker='s',ls='none',label='Stokes U/I')



def plotPolModel(idI,idQ,idU,rebinN=1,p=False):
    fig, axs = pl.subplots(3,1,figsize=(7, 8),sharex=True,gridspec_kw={'hspace': 0})  
#    fig.suptitle('Polarization Model')

    formatter = FuncFormatter(lambda y, _: '{:.4g}'.format(y))
    for k,ax in enumerate(axs):
        if (k<2):
            ax.loglog()
            ax.set_xscale('log')
            ax.set_yscale('log')
            ax.xaxis.set_major_formatter(formatter)
            ax.yaxis.set_major_formatter(formatter)
        else:
            ax.set_xscale('log')

    i=ui.get_source_plot(idI)
    ilo=np.copy(i.xlo) 
    ihi=np.copy(i.xhi)
    ix=(ilo+ihi)/2.
    dix=(ihi-ilo)
    iy=np.copy(i.y)
    print("length of original arrays ",len(ix))
    print("ix ",ix[::20])
    print("dix ",dix[::20])
    
    q=ui.get_source_plot(idQ)
    qlo=np.copy(q.xlo) 
    qhi=np.copy(q.xhi)
    qx=(ilo+ihi)/2.
    qy=np.copy(q.y)

    qxx=qy/iy
    print("q ",qxx[::20])

#    qy=np.abs(qy)

    u=ui.get_source_plot(idU)
    ulo=np.copy(u.xlo) 
    uhi=np.copy(u.xhi)
    ux=(ulo+uhi)/2.
    uy=np.copy(u.y)

    uxx=uy/iy
    print("u ",uxx[::20])

#    uy=np.abs(uy)

# hhh    
    myN=5
    m1=np.array([2.,3.,4.,6.,2.])
    m2=np.array([3.,4.,6.,8.,8.])

    sumi=np.zeros(myN)
    sumq=np.zeros(myN)
    sumu=np.zeros(myN)

    for i,energy in enumerate(ix):
        for j in range(myN):
            if (energy>m1[j] and energy<m2[j]):
                sumi[j]+=dix[i]*iy[i]
                sumq[j]+=dix[i]*qy[i]
                sumu[j]+=dix[i]*uy[i]

    for j in range(myN):
        pol=np.sqrt(sumq**2+sumu**2)/sumi    
        pDir=np.arctan2(sumu,sumq)/2.*180./3.14159265359    
        print(m1[j],' ',m2[j],' ',pol[j],' ',pDir[j])

    iy*=ix**2*1.60218e-9
    qy*=ix**2*1.60218e-9
    uy*=ix**2*1.60218e-9
    
    rx,ri=rebinLog(ix,iy,rebinN)
    rx,rq=rebinLog(qx,qy,rebinN)
    rx,ru=rebinLog(ux,uy,rebinN)

#    axs[0].plot(ix,iy,label='Stokes I')
    axs[0].plot(ix,iy)

    print("rx ",rx)
    print("ri ",ri)
    print("rq ",rq)
    print("ru ",ru)
    print("rq/ri ",rq/ri)
    print("ru/ri ",ru/ri)

    rrq=np.abs(rq)
    rru=np.abs(ru)
    axs[0].plot(rx,rrq)
    axs[0].plot(rx,rru)
#    axs[0].plot(rx,rrq,label='abs(Stokes Q)')
#    axs[0].plot(rx,rru,label='abs(Stokes U)')

    axs[0].set(ylabel='E$^2\,$dN/dE [CGS]')
    axs[0].set(ylim=(5e-12,1.1e-8))
#    axs[0].set(xlim=(1.5,81.))
    axs[0].grid(False)
#    axs[0].legend()
#    axs[1].set(ylim=(5e-12,1.1e-8))
#    axs[1].set(xlim=(1.5,81.))

    pol=100.*np.sqrt(rq**2+ru**2)/ri    
    axs[1].plot(rx,pol)
    axs[1].set(xlabel='Energy [keV]',ylabel='Pol. Degree [%]')
    axs[1].set(ylim=(0.9,10.1))
#    axs[0].set(xlim=(1.5,81.))
    axs[1].set(xlim=(0.1,81.))
    if p:
        print(ix,'\n',iy,'\n',qy,'\n',uy)
        print(rx,'\n',ri,'\n',rq,'\n',ru)

    axs[1].grid(False)

    pDir=np.arctan2(ru,rq)/2.*180./3.14159265359    
    for i,x in enumerate(pDir):
#        if (i<5) and (x<-50.):
#        if (x<-50.):
        if (False):
            pDir[i]=180.+x

    print("pol  ",pol)
    print("pDir ",pDir)
    axs[2].plot(rx,pDir)
    axs[2].set(xlabel='Energy [keV]',ylabel='Pol. Angle [$^{\circ}$]')
    axs[2].set(ylim=(-15.,99.9))
    d=np.column_stack([rx, pol,pDir])    
    name='test'+str(idI)+'.out'
    print('writing to ',name,':\n',d)
    np.savetxt(name, d, delimiter=' ',fmt='%2.6e')

def plotPolModelFlux(idI,idQ,idU,p=False):
    fig, axs = pl.subplots(2,1,figsize=(7, 7),sharex=True,gridspec_kw={'hspace': 0})  
#    fig.suptitle('Polarization Model')

    formatter = FuncFormatter(lambda y, _: '{:.4g}'.format(y))
    for ax in axs:
        ax.loglog()
        ax.set_xscale('log')
        ax.set_yscale('log')
        ax.xaxis.set_major_formatter(formatter)
        ax.yaxis.set_major_formatter(formatter)

    i=ui.get_source_plot(idI)
    ilo=np.copy(i.xlo) 
    ihi=np.copy(i.xhi)
    ix=(ilo+ihi)/2.
    iy=np.copy(i.y)
    
    q=ui.get_source_plot(idQ)
    qlo=np.copy(q.xlo) 
    qhi=np.copy(q.xhi)
    qx=(ilo+ihi)/2.
    qy=np.copy(q.y)
    qy=np.abs(qy)

    u=ui.get_source_plot(idU)
    ulo=np.copy(u.xlo) 
    uhi=np.copy(u.xhi)
    ux=(ulo+uhi)/2.
    uy=np.copy(u.y)
    uy=np.abs(uy)
    
#    axs[0].plot(ix,iy,label='Stokes I')
#    axs[0].plot(qx,qy,label='Stokes Q')
#    axs[0].plot(ux,uy,label='Stokes U')

    axs[0].plot(ix,iy)
#    axs[0].plot(qx,qy)
#    axs[0].plot(ux,uy)
    axs[0].set(ylabel='Photons/sec/cm$^2$/keV')
    axs[0].grid(False)
    axs[0].legend()

    pol=100.*np.sqrt(qy**2+uy**2)/iy    
    axs[1].plot(ix,pol)
    axs[1].set(xlabel='Energy [keV]',ylabel='Pol. Degree [%]')
    if p:
        print(ix,'\n',iy,'\n',qy,'\n',uy)

    axs[1].grid(False)

def plotSED(idI,xLim=[],yLim=[],overplot=False,report=False):
    i=ui.get_source_plot(idI)
    ilo=np.copy(i.xlo) 
    ihi=np.copy(i.xhi)
    ix=(ilo+ihi)/2.
    iy=np.copy(i.y)
    iy*=ix**2*1.60218e-9
 
    if report:
        print(ix,'\n',iy)

    if (not overplot):
        fig, ax = pl.subplots(figsize=(6, 4),sharex=True,gridspec_kw={'hspace': 0})  
#        fig.suptitle('Polarization Model')
        formatter = FuncFormatter(lambda y, _: '{:.4g}'.format(y))
        ax.loglog()
        ax.set_xscale('log')
        ax.set_yscale('log')
        ax.xaxis.set_major_formatter(formatter)
        ax.yaxis.set_major_formatter(formatter)
#        ax.grid(True)
        ax.set(xlabel='Energy [keV]',ylabel='E$^2$dN/dE [erg cm$^{-2}$ sec$^{-1}$]')
        ax.plot(ix,iy)
    else:
        pl.plot(ix,iy)

    yMax=np.max(iy)
    if (np.size(yLim)==0):
        pl.ylim([yMax/1e+3,1.5*yMax])

    if (np.size(yLim)==1):
        pl.ylim([yMax/yLim,1.5*yMax])

    if (np.size(yLim)==2):
        pl.ylim(yLim)

    if (np.size(xLim)==2):
        pl.xlim(xLim)

    pl.tight_layout()

def plotPolFraction(idI,idQ,idU,report=False,overplot=False):
    i=ui.get_source_plot(idI)
    ilo=np.copy(i.xlo) 
    ihi=np.copy(i.xhi)
    ix=(ilo+ihi)/2.
    iy=np.copy(i.y)
    
    q=ui.get_source_plot(idQ)
    qlo=np.copy(q.xlo) 
    qhi=np.copy(q.xhi)
    qx=(ilo+ihi)/2.
    qy=np.copy(q.y)
    qy=np.abs(qy)

    u=ui.get_source_plot(idU)
    ulo=np.copy(u.xlo) 
    uhi=np.copy(u.xhi)
    ux=(ulo+uhi)/2.
    uy=np.copy(u.y)
    uy=np.abs(uy)
    
    pol=100.*np.sqrt(qy**2+uy**2)/iy    
    
    if report:
        print(ix,'\n',iy,'\n',qy,'\n',uy)

    if (not overplot):
        fig, ax = pl.subplots(figsize=(6, 4),sharex=True,gridspec_kw={'hspace': 0})  
        fig.suptitle('Polarization Model')
        formatter = FuncFormatter(lambda y, _: '{:.4g}'.format(y))
        ax.loglog()
        ax.set_xscale('log')
        ax.set_yscale('log')
        ax.xaxis.set_major_formatter(formatter)
        ax.yaxis.set_major_formatter(formatter)
        ax.grid(True)
        ax.set(xlabel='Energy [keV]',ylabel='Pol. Fraction [%]')        
        ax.plot(ix,pol)
    else:
        pl.plot(ix,pol)

    
def findInterval(xList,yList,deltaChi):
    res=[]
    minX=0.
    minY=1e+20
    for k,y in enumerate(yList):
        if (y<minY):
            minY = y
            minX = xList[k]
    res.append(minX)    
    for dc in deltaChi:
        x1=1e+20
        x2=-1e+20
        for k,y in enumerate(yList):
            if (y<minY+dc):
                x1 = min(x1,xList[k])
                x2 = max(x2,xList[k])
        res.append(x1)
        res.append(x2)
    print(res)
    return res
    
def getChi2(par,delta,numStep=21):
    mem=par.val
    interval=np.linspace(mem-delta,mem+delta,numStep)
    x=[]
    y=[]
    for val in interval:
        par.val=val
        res=ui.calc_stat()
        x.append(val)
        y.append(res)
        print(val,' ',res)
    par.val=mem
    return x,y,findInterval(x,y,[1.,1.6,4.])

def getChi22(par1,delta1,par2,delta2,numStep=21):
    mem1=par1.val
    mem2=par2.val
    interval1=np.linspace(mem1-delta1,mem1+delta1,numStep)
    interval2=np.linspace(mem2-delta2,mem2+delta2,numStep)
    x, y = np.meshgrid(interval1,interval2)
    z=np.empty_like(x)
    
    for i in range(x.shape[0]):                     
        for j in range(x.shape[1]):
            par1.val=x[i][j]
            par2.val=y[i][j]
            z[i][j]=ui.calc_stat()
            print(par1.val,' ',' ',par2.val,' ',z[i][j])
    minVal = np.min(z)
    fig, ax = pl.subplots() 
    cs = ax.contour(x,y,z,levels=[minVal+0.1,minVal+1,minVal+1.6**2,minVal+4,minVal+9,minVal+16])
    ax.clabel(cs, inline=True, fontsize=10) 
    pl.show()

    par1.val=mem1
    par2.val=mem2

    return fig,ax

'''
def getContour(p1,p2,d1,d2,numStep=11):
    m1=p1.val
    m2=p2.val

    interval=np.linspace(mem-delta,mem+delta,numStep)
    x=[]
    y=[]

    for val in interval:
        par.val=val
        res=ui.calc_stat()
        x.append(val)
        y.append(res)
        print(val,' ',res)
    par.val=mem
    return x,y,findInterval(x,y,[1.,1.6,2.,3.])'''
