from scipy.interpolate import RegularGridInterpolator
from numpy import sin,cos,pi
import numpy as np
from astropy.io import fits
import matplotlib.pyplot as pl
from dataReader import *

def debugKerrc(*args): 
    if (False):
        print(*args) 

def loadFits(name):
    inp=fits.open(name)
    inp.info()
    data=inp[0].data
    inp.close()
    return data

def combine(a,b):
    """Make a new array with the elements from the first array,
    combined with one element from the second array """
    p=np.empty((len(b),len(a)+1))
    for i in range(len(b)):
        for j in range(len(a)):
            p[i,j]=a[j]
        p[i,-1]=b[i] 
    return p

def scaleAndRotate(q,u,ampl,chi):
    qDash=np.empty_like(q)
    uDash=np.empty_like(u)
    rotAngle=2.*(chi*np.pi/180.)
    c=cos(rotAngle)
    s=sin(rotAngle)
    for i in range(len(q)):
        qDash[i]=ampl*(c*q[i]-s*u[i])
        uDash[i]=ampl*(c*u[i]+s*q[i])
    return qDash,uDash

def div0( a, b ):
    ''' divide two arrays, set result to 0 if denominator is 0'''
    with np.errstate(divide='ignore', invalid='ignore'):
        c = np.true_divide( a, b )
        c[ ~ np.isfinite( c )] = 0  # -inf inf NaN
    return c

class kerrModel:
    """kerr model.
    Author: Henric Krawczynski, 4/6/2021
    """

    def __init__(self):        
        self.reader=dataReader()
        self.Mdot = np.array([3.7051669608864184e18,2.445638162425853e18,1.7032611826597898e18, \
                              1.2510545631668196e18,8.980141483098305e17,7.35593547554075e17, \
                              5.980603496793061e17, 5.297441102019783e17,4.3573499282791e17])
        self.Mdot /= 1e+18
        self.first  = True
        self.polOnlyFlag=False
        self.num=0
        
    def myi(self, pars, x1, x2):
        """ Calculate Stokes I,
        in: pars: array of parameters,
        x1,x2: lists with left (x1) and right (x2) edges of each bin
        We can get the center of the energy from x3=(x1+x2)/2.
        out: list of I values"""
        self.num+=1
        val=np.array(pars)
        res=self.reader.getI(val,x1,x2)
#        print(res)
        return res
    
    def myq(self, pars, x1, x2):
        """ Calculate Stokes I, see myi for more information."""
        x = self.reader.getQ()
#        print('getQ ',x)
        return x

    def myu(self, pars, x1, x2):
        """ Calculate Stokes U, see myi for more information."""
        x = -self.reader.getU()
#        print('getU ',x)
        return x
        
    def getNode(self,input,number):
        return self.reader.getNode(input,number)

class kerrcModel:
    """kerrc table model.
    Author: Henric Krawczynski, 6/16/2020
    """

    def __init__(self):
        print('calling init')
        self.dataI =loadFits('kerrI.fits')
        self.dataI =np.log10(np.maximum(self.dataI,1e-24))
        self.dataI =np.nan_to_num(self.dataI,copy=False,nan=-24.,posinf=24, neginf=-24)
        self.dataQ =loadFits('kerrQ.fits')        
        self.dataU =loadFits('kerrU.fits')        
        print("Loaded fits data arrays with the shapes: \n", \
              np.shape(self.dataI),'\n',np.shape(self.dataQ),'\n',np.shape(self.dataU))

        # parameter arrays
        a       = np.array([-1.,-0.5, 0., 0.5, 0.75, 0.85, 0.9, 0.95, 0.98, 0.99, 0.998])
        LoM     = np.array([0.2, 1, 5])
        tempC   = np.array([1.,5.,10.,25.,50.,100.])
        tauC    = np.array([0.,0.001,0.01,0.05,0.1,0.25,0.5,0.75,1.,1.5,2.,2.5,3.,5.])
        thetaC  = np.linspace(5,85,5)
        rC = np.linspace(25,100,4)
        refl    = np.array([0.,1.])
        incl    = np.linspace(5,85,9)
        logE    = np.linspace(-1.,2.,161)
        self.energyCenter =np.empty(160)

        for i in range(len(self.energyCenter)):
            self.energyCenter[i]=10**((logE[i]+logE[i+1])/2.)
            
        print('Required dimensions : ',len(a),' ',len(LoM),' ', \
              len(tempC),' ',
              len(tauC),' ', len(thetaC),' ',len(rC),' ',\
              len(refl),
              len(incl),' ',len(self.energyCenter))

        N2=len(a)*len(LoM)*len(tauC)*len(thetaC)*len(rC)
        print("Number of simulations: ",N2)
        print("Events generated: ",N2*1000000)

        # make interpolation functions
        print('0 a ',a)
        print('1 LoM ',LoM)
        print('2 tauC ',tauC)
        print('3 tempC ',tempC)
        print('4 thetaC ',thetaC)
        print('5 rC ',rC)
        print('6 refl ',refl)
        print('7 incl ',incl)
        print('8 energy ',self.energyCenter)
# dataI=np.zeros((len(a),len(LoM),len(tempC),len(tauC),len(thetaC),len(rC),len(refl),len(incl),len(energyCenter)))
        self.modelI = RegularGridInterpolator((a,LoM,tempC,tauC,thetaC,rC,refl,incl,self.energyCenter), self.dataI,bounds_error=False)
        self.modelQ = RegularGridInterpolator((a,LoM,tempC,tauC,thetaC,rC,refl,incl,self.energyCenter), self.dataQ,bounds_error=False)
        self.modelU = RegularGridInterpolator((a,LoM,tempC,tauC,thetaC,rC,refl,incl,self.energyCenter), self.dataU,bounds_error=False)
        self.first  = True
        self.pars=[]
        self.numCall = 0
        
        self.a    = a
        self.Mdot = np.array([3.7051669608864184e+18,3.0983936747094113e+18,2.445638162425853e+18, \
                              1.7032611826597898e+18, 1.2510545631668196e+18,1.0294115141023624e+18, \
                              8.980141483098305e+17,7.35593547554075e+17,5.980603496793061e+17,\
                              5.297441102019783e+17,4.3573499282791e+17])
        self.Mdot /= 1e+18
        self.polOnlyFlag=True
                    
    def loadModel(self,pars,x1,x2):
        flag = not self.first
        if (flag or \
            (not np.array_equal(pars,self.pars)) or \
            (not np.array_equal(x1,self.x1)) or \
            (not np.array_equal(x2,self.x2))):
            self.numCall+=1

            if (self.numCall%100==0):
                print('Model call: ',self.numCall,' ',pars)

            self.first=False
            self.pars=pars
            self.x1=x1
            self.x2=x2
            x3=(x1+x2)/2.
            interpolPars=pars[0:8]

            # pNames = [0 "a",1 "M",2 "tempC",3 "tauC",4 "thetaC",5 "rC",
            # 6 "refl",7 "incl",8 "Mdot",9 "dist",10 "chi",11 "norm"]

            mDotSim=np.interp(pars[0],self.a,self.Mdot)

            # pars [1] M
            # pars [8] Mdot
            # pars [9] distance
            # pars[11] norm
            # L[L_Edd] over M ratio 
            myLoM  = ((pars[8]/mDotSim) / (pars[1]/10.)**2)**0.25
            # amplitude scales with 1/distance**2, Mdot, and norm
            ampl = (2./pars[9])**2 * (pars[8]/mDotSim) * pars[11]

#            print('myLoM ',myLoM,' ampl ',ampl)

            interpolPars[1]=myLoM

            # ampl = (2/dist)**2 * (M/10) * (Mdot/mDotSim[a]) * norm
            
            comPars  = combine(interpolPars,x3)
            chi      = pars[10]   
            xx1=np.array(x1)
            xx2=np.array(x2)
            delta=xx2-xx1            
            i=10**self.modelI(comPars)
            self.i = ampl*(delta*i)
            self.i = np.nan_to_num(self.i,copy=False,nan=1e-24)
            q = delta*self.modelQ(comPars)
            u = delta*self.modelU(comPars)
            q=np.nan_to_num(q,copy=False,nan=1e-24)
            u=np.nan_to_num(u,copy=False,nan=1e-24)
            self.q,self.u =scaleAndRotate(q,u,ampl,chi)
                        

    def myi(self, pars, x1, x2):
        """ Calculate Stokes I,
        in: pars: array of parameters,
        x1,x2: lists with left (x1) and right (x2) edges of each bin
        We can get the center of the energy from x3=(x1+x2)/2.
        out: list of I values"""
        self.loadModel(pars,x1,x2)
        return self.i
    
    def myq(self, pars, x1, x2):
        """ Calculate Stokes I, see myi for more information."""
        self.loadModel(pars,x1,x2)
        return self.q

    def myu(self, pars, x1, x2):
        """ Calculate Stokes U, see myi for more information."""
        self.loadModel(pars,x1,x2)
        return self.u
    
    def iquStat(self,data, model, staterror, syserror=None, weight=None, bkg=None): 

        length=len(data)
        l = int(length/3)
        if not length%3==0:
            print('The data set does not seem to include I, Q, and U information')
            return

        # (1) data
        di=data[0:l]
        dq=data[l:2*l]
        du=data[2*l:3*l]

        # (2) model
        mi=model[0:l]
        mq=model[l:2*l]
        mu=model[2*l:3*l]  
        print('data ',data)
        print('model ',model)
        return 0,0

    def polOnly(self,data, model, staterror, syserror=None, weight=None, bkg=None): 

        length=len(data)
        l = int(length/3)
        if not length%3==0:
            print('The data set does not seem to include I, Q, and U information')
            return
        if self.polOnlyFlag:
            print('Degrees of Freedom: ',2*l)
            self.polOnlyFlag=False

        # (1) data
        di=data[0:l]
        dq=data[l:2*l]
        du=data[2*l:3*l]
        # normalize
        ndq = div0(dq,di)
        ndu = div0(du,di)

        debugKerrc('data ',data)
        debugKerrc('di ',di)
        debugKerrc('dq ',dq)
        debugKerrc('du ',du)        
        debugKerrc('ndq ',ndq)
        debugKerrc('ndu ',ndu)        
        
        # (2) model
        mi=model[0:l]
        mq=model[l:2*l]
        mu=model[2*l:3*l]  
        # normalize
        nmq = div0(mq,mi)
        nmu = div0(mu,mi)
        
        debugKerrc('mi ',mi)
        debugKerrc('mq ',mq)
        debugKerrc('mu ',mu)        
        debugKerrc('nmq ',nmq)
        debugKerrc('nmu ',nmu)        
        
        # (3) set errors
        debugKerrc('si ',staterror[0:l])
        debugKerrc('sq ',staterror[l:2*l])
        debugKerrc('su ',staterror[2*l:3*l])        

        # calculate relative errors on I, Q, and U
        ei = div0(staterror[0:l],di)
        eq = div0(staterror[l:2*l],dq)
        eu = div0(staterror[2*l:3*l],du)

        debugKerrc('ei ',ei)
        debugKerrc('eq ',eq)
        debugKerrc('eu ',eu)        

        # add relative errors on I and Q (or U), and multiply with Q/I (or U/I) 
        # to get the error on Q/I (or U/I)
        eq=ndq*np.sqrt(ei**2+eq**2)
        eu=ndu*np.sqrt(ei**2+eu**2)

        debugKerrc('Sum eq ',eq)
        debugKerrc('Sum eu ',eu)        

        # check by how much Q/I and U/I deviate from  

        fq=div0(ndq-nmq,eq)
        fu=div0(ndu-nmu,eu)
        stat = (fq**2).sum()+(fu**2).sum()

        debugKerrc('res_q ',fq)
        debugKerrc('res_u ',fu)        
        debugKerrc('chi2 ',stat)        
        
        fvec = div0(data - model,staterror)
        return (stat, fvec)        

def tie_pars(model1,model2):
    model2.a       =model1.a
    model2.geom    =model1.geom
    model2.M       =model1.M
    model2.tempC   =model1.tempC
    model2.tauC    =model1.tauC
    model2.thetaC  =model1.thetaC
    model2.rC      =model1.rC
    model2.incl    =model1.incl
    model2.Mdot    =model1.Mdot
    model2.dist    =model1.dist
    model2.chi     =model1.chi

    model2.refl    =model1.refl
    model2.AFe     =model1.AFe
    model2.kTe     =model1.kTe
    model2.logDens =model1.logDens
    model2.eFit =model1.eFit
    model2.alpha =model1.alpha
    model2.l1 =model1.l1
    model2.l2 =model1.l2

#def tiec_pars(model1,model2):
#    model2.a      =model1.a
#    model2.M      =model1.M
#    model2.tempC  =model1.tempC
#    model2.tauC   =model1.tauC
#    model2.thetaC =model1.thetaC
#    model2.rC     =model1.rC
#    model2.refl   =model1.refl
#    model2.incl   =model1.incl
#    model2.Mdot   =model1.Mdot
#    model2.dist   =model1.dist
#    model2.chi    =model1.chi
#    model2.norm   =model1.norm
